game_behaviour

This project is entirely my own work. 
External library used is SFML a C++ Opengl Rendering Utility Library.

This project was a 2D physics simulator with a very small mini game.
I can't say the game is too enjoyable, but I felt like the physical behaviour and some of the other engine features were well executed.

To play.
W,A,S,D to move,
Space to jump.
Left shift to do a small teleportation.

Building.
Unfortunately I have not created a cmake for this project. 
In the IDE I established the build properties (Static link library order, post build commands, etc).