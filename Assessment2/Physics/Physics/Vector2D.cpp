#include "Vector2D.h"
#include <cmath>
#include <math.h>

//Constructors added for usability - but not strictly necessary.
Vector2D::Vector2D() : sf::Vector2<float>()
{}

Vector2D::Vector2D(float x, float y) : sf::Vector2<float>(x, y)
{}

Vector2D::Vector2D(sf::Vector2<float>& sf_version) : sf::Vector2<float>(sf_version.x, sf_version.y)
{}

Vector2D::Vector2D(const sf::Vector2<float>& const_sf_version) : sf::Vector2<float>(const_sf_version.x, const_sf_version.y)
{

}

Vector2D::Vector2D(sf::Vector2<int>& sf_int_version) : sf::Vector2<float>((float)sf_int_version.x, (float)sf_int_version.y)
{

}

float Vector2D::Length()
{
	return (float)std::sqrt(this->x * this->x + this->y* this->y);
}

Vector2D Vector2D::Normal()
{
	return Vector2D(this->x, this->y) / Vector2D::Length();
}

float Vector2D::Dot(Vector2D& other)
{
	return x * other.x + y * other.y;
}

float Vector2D::Dot(const Vector2D& other)
{
	return x * other.x + y * other.y;
}

//returns the vector squared
Vector2D Vector2D::Squared()
{
	return Vector2D(this->x * this->x, this->y * this->y);
}

Vector2D Vector2D::SquareRoot()
{
	return Vector2D(std::sqrt(this->x), std::sqrt(this->y));
}

Vector2D::operator sf::Vector2<float>()
{
	return sf::Vector2<float>(x,y);
}
