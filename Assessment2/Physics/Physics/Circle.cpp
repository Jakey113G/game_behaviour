#include "Circle.h"
#include "AABB.h"

Physics::Circle::Circle(Vector2D center, float radius) : m_position(center), m_radius(radius)
{
	this->type = CIRCLE_Collider;
}

Vector2D Physics::Circle::GetCenter()
{
	return m_position;
}

Vector2D Physics::Circle::GetSize()
{
	float half_extend = m_radius * 0.5f;
	return Vector2D(half_extend, half_extend); //returns maximum positive extends
}

void Physics::Circle::UpdatePosition(Vector2D& deltaPosition)
{
	m_position += deltaPosition;
}

void Physics::Circle::SetPosition(Vector2D& newPosition)
{
	m_position = newPosition;
}

Vector2D Physics::Circle::CalculateContactNormal(Vector2D contact)
{
	return Vector2D(contact - m_position).Normal();
}

Physics::CollisionPair Physics::Circle::HitDataBetweenShapes(Shape* shape)
{
	if (shape->GetColliderType() == CIRCLE_Collider)
	{
		return HitDataCircle(static_cast<Circle*>(shape));
	}
	else if (shape->GetColliderType() == AABB_Collider)
	{
		return HitDataAABB(static_cast<AABB*>(shape));
	}
	return CollisionPair(); //empty failure
}

Physics::CollisionPair Physics::Circle::HitDataCircle(Circle* circle_shape)
{
	CollisionPair hitdata;

	Vector2D seperation = circle_shape->GetCenter() - m_position; //displacement towards circle_shape
	hitdata.contact_normal = seperation.Normal(); //Get center is center (m_position) of circle in world space

	//return the position plus bounds for contact. Normal is used to work out which point relative to sphere.
	hitdata.contact_point = (hitdata.contact_normal * m_radius) + this->m_position;
	
	float axis_seperation_length = m_radius + circle_shape->GetSize().Length();
	float overlap_value = seperation.Length() - axis_seperation_length;
	hitdata.manifold_seperation = overlap_value * hitdata.contact_normal; //The overlap value in vector space by multiplying the distance by displacement direction.
	return hitdata;
}

Physics::CollisionPair Physics::Circle::HitDataAABB(AABB* aabb_shape)
{
	//Get closest sphere point towards
	CollisionPair hitdata;

	Vector2D closest_point = aabb_shape->ClosestPointAABB(this->GetCenter());
	hitdata.contact_point = closest_point;
	
	//SAT against closest AABB point
	Vector2D distance_to_point = this->GetCenter() - closest_point;
	hitdata.contact_normal = distance_to_point.Normal();

	//In this circle code I believe that contact point is calculated wrong so I shall just do a SAT test instead
	float depth_penetrating = Vector2D(hitdata.contact_point - this->GetCenter()).Length(); 
	depth_penetrating = std::abs(m_radius - depth_penetrating);
	hitdata.manifold_seperation = -1.0f * hitdata.contact_normal * depth_penetrating; //hack


	//TODO RESOLVE THIS DETECTION AND REMOVE THE LAZY CAST IN THE AABB CLASS FOR THIS FUNCTION.
	//Vector2D max_extends = this->GetSize() + (aabb_shape->GetSize() * 0.5f);
	//Vector2D distance = this->GetCenter() - aabb_shape->GetCenter();
	//float seperation = max_extends.Length() - distance.Length();
	//hitdata.manifold_seperation = seperation * hitdata.contact_normal;

	return hitdata;
}
