#pragma once
#include "Shape.h"
#include "Vector2D.h"
#include "CollisionPair.h"

namespace Physics
{
	class AABB;
	class Circle : public Shape
	{
	public:
		Circle(Vector2D center, float radius);

		virtual Vector2D GetCenter() override;
		virtual Vector2D GetSize() override;

		float GetRadius()
		{
			return m_radius;
		}

		virtual void UpdatePosition(Vector2D& deltaPosition) override;
		virtual void SetPosition(Vector2D& newPosition) override;

		virtual Vector2D CalculateContactNormal(Vector2D contact) override;
		virtual CollisionPair HitDataBetweenShapes(Shape* shape) override;

		CollisionPair HitDataCircle(Circle* shape);
		CollisionPair HitDataAABB(AABB*);

	private:
		Vector2D m_position;
		float m_radius;
	};
}