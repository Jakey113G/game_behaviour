#pragma once
#include "Vector2D.h"

namespace Physics
{
	class RigidBody2D;
	class CollisionPair
	{
	public:
		RigidBody2D* bodyA;
		RigidBody2D* bodyB;
		Vector2D contact_point;
		Vector2D contact_normal;
		Vector2D manifold_seperation;
	};
}