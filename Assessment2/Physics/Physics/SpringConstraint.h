#pragma once
#include "Constraint.h"

namespace Physics
{
	class SpringConstraint : public Constraint
	{
	public:
		SpringConstraint();
		SpringConstraint(RigidBody2D*, RigidBody2D*);

		virtual void ResolveConstraint(float) override;

		float Stiffness;
		float RestLength;
		float Dampen;
	};
}