#include "RigidBodySolver.h"
#include "PhysicsEventManager.h"
#include "CollisionPair.h"
#include "RigidBody2D.h"
#include "Vector2D.h"
#include "AABB.h"
#include "Circle.h"

#ifdef _DEBUG
#include <iostream>
#endif // _DEBUG

Physics::RigidBodySolver::RigidBodySolver(PhysicsEventManager* events, const Vector2D& world_gravity) : event_manager(events), gravity(world_gravity)
{

}

void Physics::RigidBodySolver::Resolve(CollisionPair& pair, float delta)
{
#ifdef _DEBUG
	if (dynamic_cast<Circle*>(pair.bodyA->GetColliderShape()))
	{
		std::cout << "circle collision" << std::endl;
	}
#endif // _DEBUG

	if (pair.bodyA->GetCollisionType() == BLOCKING_COLLISION && pair.bodyB->GetCollisionType() == BLOCKING_COLLISION)
	{	
		ResolvePenetration(pair, delta);	
		ElasticCollision(pair, delta);
		event_manager->GetEventState(pair.bodyA)->Hit_Event.AddToEvent(HitDataResult(pair.bodyB, pair.contact_normal));
		event_manager->GetEventState(pair.bodyB)->Hit_Event.AddToEvent(HitDataResult(pair.bodyA, -pair.contact_normal));
		
		//Apply friction between these hitting bodies
		//ApplyFrictionByVelocity(pair, delta);
	}
	//No collision - overlap event controlling
	else
	{
		if (pair.bodyA->GetCollisionType() == OVERLAP_COLLISION)
		{
			BodyPhysicsEvents* bodyAEvent = event_manager->GetEventState(pair.bodyA);
			if (!bodyAEvent->Overlapping_Event.AlreadyRegisteredInEvent(pair.bodyB))
			{
				bodyAEvent->Overlapping_Event.AddToEvent(pair.bodyB, -pair.contact_normal);
			}
		}
		if (pair.bodyB->GetCollisionType() == OVERLAP_COLLISION)
		{
			BodyPhysicsEvents* bodyBEvent = event_manager->GetEventState(pair.bodyB);
			if (!bodyBEvent->Overlapping_Event.AlreadyRegisteredInEvent(pair.bodyA))
			{
				bodyBEvent->Overlapping_Event.AddToEvent(pair.bodyA, pair.contact_normal);
			}
		}
	}
}

void Physics::RigidBodySolver::ElasticCollision(CollisionPair& pair, float delta)
{
	Vector2D contact_normal = pair.contact_normal / pair.contact_normal.Length();

	//average restitution of this collision - (Consider having magnitude of force to weight which restitution value has more effect)
	float restitution = (pair.bodyB->GetRestituition() + pair.bodyA->GetRestituition()) * 0.5f; 

	float invMassA = !pair.bodyA->IsDynamic() ? 0.0f : pair.bodyA->GetInvMass();
	float invMassB = !pair.bodyB->IsDynamic() ? 0.0f : pair.bodyB->GetInvMass();

	Vector2D delta_velocity = pair.bodyB->GetLinearVelocity() - pair.bodyA->GetLinearVelocity();
	float impulse_j = -(1 + restitution) * delta_velocity.Dot(contact_normal);
	impulse_j = impulse_j / (invMassB + invMassA);

	//if movement already taking us away from collision don't apply impulse. (COMMENTED OUT BECAUSE WITH OUT IT WE CANT PUSH OBJECTS - Should use momentum instead)
	//if (impulse_j == 0)
	//{
	//	return;
	//}

	//Average frictional force calculated between two bodies
	float body_frictional_value = (pair.bodyA->GetFrictionCoefficent() + pair.bodyB->GetFrictionCoefficent()) * 0.5f;

	//Apply impulse to body A and body B
	if (pair.bodyA->IsDynamic())
	{
		Vector2D impulse_velocity = contact_normal * (impulse_j / pair.bodyA->GetMass());
		Vector2D newLinearVelocity = pair.bodyA->GetLinearVelocity() - impulse_velocity;
		pair.bodyA->SetLinearVelocity(newLinearVelocity);
		ApplyFrictionToBody(*pair.bodyA, pair.contact_normal, Vector2D(impulse_velocity * -1.0f), body_frictional_value, delta);
	}
	if (pair.bodyB->IsDynamic())
	{
		Vector2D impulse_velocity = contact_normal * (impulse_j / pair.bodyB->GetMass());
		Vector2D newLinearVelocity = pair.bodyB->GetLinearVelocity() + impulse_velocity;
		pair.bodyB->SetLinearVelocity(newLinearVelocity);
		Vector2D contact_b = pair.contact_normal * -1.0f;
		ApplyFrictionToBody(*pair.bodyB, contact_b, impulse_velocity, body_frictional_value, delta);
	}
}

void Physics::RigidBodySolver::InElasticCollision(CollisionPair& pair)
{
	if (pair.bodyA->IsStatic())
	{
		pair.bodyA->SetLinearVelocity(Vector2D(0,0));
	}
	if (pair.bodyB->IsStatic())
	{
		pair.bodyB->SetLinearVelocity(Vector2D(0,0));
	}

	Vector2D contact_normal = pair.contact_normal / pair.contact_normal.Length();

	float massA = pair.bodyA->IsStatic() ? 0.0f : pair.bodyA->GetMass();
	float massB = pair.bodyB->IsStatic() ? 0.0f : pair.bodyB->GetMass();

	float inv_mass_sum = 1.0f / (pair.bodyA->GetMass() + pair.bodyB->GetMass());

	Vector2D velocity = ((pair.bodyA->GetLinearVelocity() * pair.bodyA->GetMass()) + (pair.bodyB->GetLinearVelocity() * pair.bodyB->GetMass())) * inv_mass_sum;

	//Update rigid bodies
	pair.bodyA->SetLinearVelocity(velocity);
	pair.bodyB->SetLinearVelocity(velocity);
}

//Should really be done in the impulse calculation to have a better application of friction
void Physics::RigidBodySolver::ApplyFrictionByVelocity(CollisionPair& pair, float delta)
{
	if (pair.bodyA->GetLinearVelocity() == Vector2D(0, 0))
	{
		//No friction when the velocity is zero (also prevents us creating some divide by 0 errors)
		return;
	}

	//if movement of bodies is in the same direction - don't apply any horizontal velocity changes.
	//if (pair.bodyA->GetLinearVelocity().Dot(pair.bodyB->GetLinearVelocity()) > 0)
	//{                           
	//	return;
	//}

	//DIRECTION OF Friction to apply for the bodies- b is opposite to A, A is opposite to B
	Vector2D friction_direction_b = pair.bodyA->GetLinearVelocity();
	Vector2D friction_direction_a = -friction_direction_b;

	float friction = 120.0f; //change to get value from averaging bodies
	if (pair.bodyA->IsDynamic())
	{
		friction_direction_a = friction_direction_a.Normal() * (friction * delta); //friction value

		if (pair.bodyA->GetLinearVelocity().Length() > 1)
		{
			pair.bodyA->AddLinearVelocity(friction_direction_a);
		}

		//Hack to ensure negligible velocity is reduced to 0 - should be replaced with sleep handling
		//Vector2D bodyA_velocity = pair.bodyA->GetLinearVelocity();
		//if (bodyA_velocity.x < 1.0f && bodyA_velocity.x > -1.0f && bodyA_velocity.y < 1.0f && bodyA_velocity.y > -1.0f)
		//{
		//	pair.bodyA->SetLinearVelocity(Vector2D(0, 0)); //dangerous - change this to turn the body into sleep
		//}
	}
	if (pair.bodyB->IsDynamic())
	{
		friction_direction_b = friction_direction_b.Normal() * (friction * delta); //friction value
		if (pair.bodyB->GetLinearVelocity().Length() > 1)
		{
			pair.bodyB->AddLinearVelocity(friction_direction_b);
		}

		//Hack to ensure negligible velocity is reduced to 0 - should be replaced with sleep handling
		//Vector2D bodyB_velocity = pair.bodyB->GetLinearVelocity();
		//if (bodyB_velocity.x < 1.0f && bodyB_velocity.x > -1.0f && bodyB_velocity.y < 1.0f && bodyB_velocity.y > -1.0f)
		//{
		//	pair.bodyB->SetLinearVelocity(Vector2D(0,0)); //dangerous - change this to turn the body into sleep
		//}
	}
}

void Physics::RigidBodySolver::ApplyFrictionToBody(RigidBody2D& body, Vector2D& surface_normal , Vector2D& change_in_velocity ,float friction_coefficent, float delta)
 {
#ifdef _DEBUG
	Vector2D norm = body.GetLinearVelocity().Normal();
	if ((norm.x > 0 && norm.x < 1) || (norm.x > -1 && norm.x < 0))
	{
		//std::cout << "at an angle" << std::endl;
	}
#endif // _DEBUG

	if (body.GetLinearVelocity() == Vector2D(0, 0))
	{
		return;
	}

	Vector2D movement_direction = body.GetLinearVelocity().Normal();
	//No friction if moving vertically. bouncing objects won't work - If time improve this so we handle vertical friction
	//for now added a slight hack so we apply vertical friction if vertical movement is minor in value
	if (surface_normal.Dot(movement_direction) > 0.05)
	{
		return;
	}

	//incorrect way of calculating force of body
	//calculate the body force from the velocity - needed to calculate which friction to apply.
	Vector2D linear_accelleration = body.GetLinearVelocity() / delta; //(Velocity = acceleraton * time) == linear accelleration = velocity / delta
	Vector2D force_of_body = body.GetMass() * linear_accelleration; //f = M*A

	//hack to calculate force treating velocity as the acceleration
	//Vector2D force_of_body = body.GetForce();
	//if (force_of_body.Length() == 0)
	//{
		//treat the velocity as the acceleration
	//	force_of_body = body.GetLinearVelocity() * body.GetMass();
	//}

	//correct way using the impulse change in velocity
	//accelleration = delta Velo / delta time
	//Vector2D accelleration = change_in_velocity / delta;
	//Vector2D force_of_body = accelleration * body.GetMass();

	//friction_coefficent = 1.0f - friction_coefficent;
	//friction_coefficent += 1.5f; //cheap hack to increase friction do it properly later and take value from body

	//calculate static friction coefficient to overcome before applying dynamic friction
	float static_force = surface_normal.Dot(gravity) * friction_coefficent; //Normal force of plane (will be gravity most cases) * friction coefficent

	//no friction
	if (static_force == 0)
	{
		return;
	}
	
	friction_coefficent *= 1000.0f; //multiple the frictional force to apply so it works for my physics scale

	//If the body force overcomes dynamic 
	if (force_of_body.Length() > abs(static_force)) ///DYNAMIC FRICTION - AS VELOCITY CHANGE
	{
		//apply friction to body as an impulse of dynamic friction force.
		Vector2D dynamic_friction_velocity = (friction_coefficent * 0.2f) * movement_direction; //<-- this is what needs changing
		body.AddLinearVelocity(-dynamic_friction_velocity * delta);
	}
	else //STATIC FRICTION CHANGE
	{
		if (body.GetLinearVelocity().Length() < 1.0f)
		{
			body.SetLinearVelocity(Vector2D(0, 0));
		}
		else
		{
			//apply static friction to bodies
			Vector2D dynamic_friction_velocity = (friction_coefficent * 1.0f) * movement_direction;  //<-- this is what needs changing
			body.AddLinearVelocity(-dynamic_friction_velocity * delta);
		}
	}

#ifdef _DEBUG
	Vector2D p = body.GetPosition();
	if (p.Length() > 3000)
	{
		std::cout << "body has been massively displaced" << std::endl;
	}
#endif // _DEBUG

}

void Physics::RigidBodySolver::ResolvePenetrationDynamicStatic(CollisionPair& pair, float delta)
{
	Vector2D depth_displacement = pair.manifold_seperation; //CalculateIntersectionDepth(*pair.bodyA->GetColliderShape(), *pair.bodyB->GetColliderShape());

	if (pair.bodyA->IsDynamic())
	{
		pair.bodyA->SetPosition(pair.bodyA->GetPosition() - depth_displacement);
	}
	if (pair.bodyB->IsDynamic())
	{
		pair.bodyB->SetPosition(pair.bodyB->GetPosition() + depth_displacement);
	}
}

void Physics::RigidBodySolver::ResolvePenetrationDynamicDynamic(CollisionPair& pair, float delta)
{
	//calculate penetration across the normal
	//Work out how to distribute the penetration resolve between dynamic bodies.
	//Work out velocity direction to calculate how the bodies intersected each other. Largest velocity towards collision takes more of the penetration offset.
	//bodies moving away from collision don't get their position modified.
	
	//NOTE: This can be improved on.

	//calculate velocities in the normal axis domain towards the collision
	float bodyA_velocity_along_normal_axis = pair.bodyA->GetLinearVelocity().Dot(Vector2D(-pair.contact_normal));
	float bodyB_velocity_along_normal_axis = pair.bodyB->GetLinearVelocity().Dot(pair.contact_normal);

	float sum_of_velos = abs(bodyA_velocity_along_normal_axis) + abs(bodyB_velocity_along_normal_axis);
	if (sum_of_velos == 0)
	{
		return;
	}

	//normalize velocity using sum of velocities. 
	//use value as magnitude of penetration resolution
	float normalized_normal_velocity_a = abs(bodyA_velocity_along_normal_axis)/ sum_of_velos;
	float normalized_normal_velocity_b = abs(bodyB_velocity_along_normal_axis)/ sum_of_velos;

	if (!pair.bodyA->IsStatic())
	{		
		Vector2D depth_displacement = pair.manifold_seperation;
		depth_displacement *= bodyB_velocity_along_normal_axis < 0 ? 1.0f : normalized_normal_velocity_a; //if body b is moving away from collision - A takes all the penetration displacement
		pair.bodyA->SetPosition(pair.bodyA->GetPosition() - depth_displacement);
	}
	if (!pair.bodyB->IsStatic())
	{
		Vector2D depth_displacement = pair.manifold_seperation;
		depth_displacement *= bodyA_velocity_along_normal_axis < 0 ? 1.0f : normalized_normal_velocity_b; //if body A is moving away from collision - B takes all the penetration displacement
		pair.bodyB->SetPosition(pair.bodyB->GetPosition() + depth_displacement);
	}
}

void Physics::RigidBodySolver::ResolvePenetration(CollisionPair& pair, float delta)
{
	//If one of bodies is static use static to dynamic penetration handling
	if (!pair.bodyA->IsDynamic() || !pair.bodyB->IsDynamic())
	{
		ResolvePenetrationDynamicStatic(pair, delta);
	}
	else
	{
		ResolvePenetrationDynamicDynamic(pair, delta);
	}
}

/*
//atm this just calculates our intersection depth using axis separation and direction
Vector2D Physics::RigidBodySolver::CalculateIntersectionDepth(Shape& shape1, Shape& shape2)
{
	//this method works by calculating the delta differences between our colliders based on extends and positions.
	//generating points to ensure we only effect if points lie in box
	Vector2D shape1_extends = static_cast<AABB&>(shape1).GetSize() * 0.5f;
	Vector2D shape2_extends = static_cast<AABB&>(shape2).GetSize() * 0.5f;

	float dx = shape2.GetCenter().x - shape1.GetCenter().x;
	float px = (shape1_extends.x + shape2_extends.x) - abs(dx);

	float dy = shape2.GetCenter().y - shape1.GetCenter().y;
	float py = (shape1_extends.y + shape2_extends.y) - abs(dy);

	Vector2D intersection_depth;
	if (px < py)
	{
		float sx = dx < 0 ? -1.0f : 1.0f;
		dx = px * sx;

		//intersection_depth.x = shape1.GetCenter().x + (shape1_extends.x * sx); //collision point x.
		intersection_depth.x = dx; //collision depth x
		intersection_depth.y = 0;
	}
	else
	{
		float sy = dy < 0 ? -1.0f : 1.0f;
		dy = py * sy;
		intersection_depth.x = 0;
		//intersection_depth.y = shape1.GetCenter().y + (shape1_extends.y * sy); //collision point y 
		intersection_depth.y = dy; //collision intersection depth y 
	}
	return intersection_depth;
}
*/