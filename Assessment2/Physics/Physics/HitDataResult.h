#pragma once
#include "Vector2D.h"

//Different from collision pairs - which held other data relevant to resolving the collision this struct only has the body and the normal
namespace Physics
{
	class RigidBody2D;

	class HitDataResult
	{
	public:
		HitDataResult(RigidBody2D* hit_body, Vector2D normal) : body(hit_body), contact_normal(normal)
		{}

		Physics::RigidBody2D* body;
		Vector2D contact_normal;

		bool operator==(const HitDataResult& body_hit_data) const
		{
			return (body == body_hit_data.body);
		}

		bool operator==(const Physics::RigidBody2D* body_compare) const
		{
			return (body == body_compare);
		}
	};
}