#include "PhysicsEvent.h"



Physics::PhysicsEvent::PhysicsEvent()
{

}

Physics::PhysicsEvent::~PhysicsEvent()
{

}

void Physics::PhysicsEvent::InvokeEvent()
{
	//Ensure delegates can only be called once
	if (event_triggered)
	{
		return;
	}
	event_triggered = true;

	for (Physics::HitDataResult& body_collision : bodies_in_event)
	{
		for (auto F : collection_of_delegate_function_calls)
		{
			//trigger function with body - dereference function pointer 
			(F)(body_collision);
		}
	}
}

void Physics::PhysicsEvent::Clear()
{
	bodies_in_event.clear();
	
	//refresh event trigger.
	event_triggered = false;
}
