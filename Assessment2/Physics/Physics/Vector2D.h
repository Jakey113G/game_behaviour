#pragma once
#include <SFML/System/Vector2.hpp>

//Rendering handled in Game uses the sfml vector2 classes. These classes are lacking in some functionality so I am extending them for my own usage.
//Extending off this allows backwards compatibility with my render library as this version of vector class can still work rendering.

//Not declaring this within Physics namespace as I feel it should be for anything in this entire solution (game, ai, physics)

//There is a problem you may have noticed. I am extending off a class that does not use a virtual destructor. If this class was used dynamically and a base destructor is called
//we may experience a memory leak. 
//1. This is a very tiny memory leak.
//2. Vector2D in my code has only ever been used statically - no base destructor calling.
//Potentially this won't be a leak, at worst case it can be a very tiny leak.

class Vector2D : public sf::Vector2<float>
{
public:
	//Constructors added for usability - but not strictly necessary.
	Vector2D();

	Vector2D(float x, float y);

	Vector2D(sf::Vector2<float>& sf_version);
	Vector2D(sf::Vector2<int>& sf_int_version);
	Vector2D(const sf::Vector2<float>& const_sf_version);

	//Conversion operator lovely thing that creates the type I want.
	operator sf::Vector2<float>();

	float Length();
	Vector2D Normal();
	float Dot(Vector2D& other);
	float Dot(const Vector2D& other);
	Vector2D Squared();
	Vector2D SquareRoot();
};