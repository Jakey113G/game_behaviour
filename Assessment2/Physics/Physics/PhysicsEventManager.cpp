#include "PhysicsEventManager.h"
#include "RigidBody2D.h"
#include <algorithm>

Physics::PhysicsEventManager::PhysicsEventManager()
{

}

Physics::PhysicsEventManager::~PhysicsEventManager()
{

}

/*
void Physics::PhysicsEventManager::Process(RigidBody2D* body)
{
	//Process all collisions and match which collisions should be in each event. Only do it if the body 
	BodyPhysicsEvents* events =	GetEventState(body);
	if (body->GetCollisionType() == BodyCollisionType::OVERLAP_COLLISION)
	{
		//if bodies in all detected collision is not in overlapping enter event list, add them
		for (RigidBody2D* body : events->all_detected_collisions)
		{
			if (!events->OverlappingEnter_Event.AlreadyRegisteredInEvent(body))
			{
				events->OverlappingEnter_Event.AddToEvent(body, Vector2D(0,0));
			}
		}

		//if bodies in the current overlap list not in the all detected list add them to the exit event
		for (RigidBody2D* body : events->Overlapping_Event.Bodies())
		{
			std::vector<RigidBody2D*>& all_collisions = events->all_detected_collisions;
			if (!(std::find(all_collisions.begin(), all_collisions.end(), body) != all_collisions.end()))
			{
				events->OverlappingExit_Event.AddToEvent(body, Vector2D(0,0));
			}
		}

		//set all bodies to overlap list
		events->Overlapping_Event.Bodies() = events->all_detected_collisions;

		events->Hit_Event.Clear(); //This object overlaps only no rigid collisions (until we add physics type layer filtering)
	}
	else if (body->GetCollisionType() == BLOCKING_COLLISION)
	{
		//if not in the all list remove them from the hit event

		//if already in the hit list remove them - they get added back after the collision resolve.

		//NO overlap events for a blocking collision event object
		events->Overlapping_Event.Clear();
		events->OverlappingEnter_Event.Clear();
		events->OverlappingExit_Event.Clear();
	}
}
*/

Physics::BodyPhysicsEvents* Physics::PhysicsEventManager::GetEventState(RigidBody2D* body_pointer)
{
	return &body_events[body_pointer];
}

void Physics::PhysicsEventManager::Invoke(RigidBody2D* body)
{
	body_events[body].InvokeEvents();
}