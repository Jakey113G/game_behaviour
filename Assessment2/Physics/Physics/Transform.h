#pragma once
#include <SFML/Graphics/Transformable.hpp>

//Similar to the Vector2D class. This is a derived transformable object.
//this is to allow the extension of functionality if I need it.

//typedef sf::Transformable Transform;


class Transform : public sf::Transformable
{
public:
	//Constructors added for usability - but not strictly necessary.
	Transform();
	Transform(sf::Transformable& sf_version);
};
