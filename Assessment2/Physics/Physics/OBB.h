#include "Shape.h"
#include "Vector2D.h"

namespace Physics
{
	class AABB;
	class OBB : public Shape
	{
	private:
		Vector2D center_point;
		Vector2D axis_rotation[2];
		Vector2D extends_from_center;
	
	public:
		OBB(Vector2D position, Vector2D extends);
		
		//Special constructor to make a OBB for a sweep test
		OBB(AABB& aabb_before, AABB& aabb_after);

		virtual Vector2D GetCenter() override;

		virtual void UpdatePosition(Vector2D& deltaPosition) override;
		virtual void SetPosition(Vector2D& newPosition) override;

		void UpdateRotation(float rotation);
		void SetAxis(Vector2D xaxis, Vector2D yaxis);
	};
}