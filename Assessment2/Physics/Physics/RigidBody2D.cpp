#include "RigidBody2D.h"
#include "AABB.h"

Physics::RigidBody2D::RigidBody2D() : linearVelocity(), force()
{
	motion_type = DYNAMIC_MOTION;
	collision_type = BLOCKING_COLLISION;
	SetMass(1.0f);
	SetFrictionCoefficent(1.0f);
}

Physics::RigidBody2D::RigidBody2D(BodyData data)
{
	//this->transform = static_cast<sf::Transformable*>(data.transform);
	this->position = data.position;
	this->force = data.force;
	this->angularVelocity = data.angularVelo;
	this->angularAccelleration = data.angularAccelleration;
	this->restituition = 0.0f;
	this->frictional_coefficent = data.frictionCoefficent;
	SetMass(data.mass);
	this->collision_type = data.collision_type;
	this->motion_type = data.motion_type;
	//if (data.kinematic) //everything starts awake unless specified directly
	//{
	//	this->sleeping = true;
	//}
}

Physics::RigidBody2D::RigidBody2D(Vector2D position)
{
	this->position =position;
	SetMass(10.0f);
	collision_type = BLOCKING_COLLISION;
	this->motion_type = BodyMotionType::DYNAMIC_MOTION;
	this->sleeping = false;
	SetFrictionCoefficent(1.0f);
}

//Physics::RigidBody2D::RigidBody2D(sf::Transformable* trans) : linearVelocity(), force()
//{
//	this->transform = trans;
//	SetMass(10);
//	this->kinematic = false;
//	this->sleeping = false;
//}

Physics::RigidBody2D::RigidBody2D(BodyData data, Shape* shape)
{
	//this->transform = static_cast<sf::Transformable*>(data.transform);
	this->position = data.position;
	this->force = data.force;
	this->angularVelocity = data.angularVelo;
	this->angularAccelleration = data.angularAccelleration;
	this->restituition = 0.0f;
	SetMass(data.mass);
	this->collision_type = data.collision_type;
	this->motion_type = data.motion_type;
	this->shape = shape;
	this->frictional_coefficent = data.frictionCoefficent;
	//if (data.kinematic) //everything starts awake unless specified directly
	//{
	//	this->sleeping = true;
	//}
}

Physics::RigidBody2D::~RigidBody2D()
{
	delete shape;
}

void Physics::RigidBody2D::SetPosition(Vector2D pos_vector)
{
	//Set the position of our body and adjust the position of our collider too.
	position = pos_vector;
	shape->SetPosition(pos_vector);
}

void Physics::RigidBody2D::AddPosition(Vector2D delta_position)
{
	//transform->setPosition(transform->getPosition() + delta_position); //used for transform objects only
	position += delta_position;
}

void Physics::RigidBody2D::AddLinearVelocity(Vector2D velocity)
{
	linearVelocity += velocity;
}

void Physics::RigidBody2D::SetLinearVelocity(Vector2D velocity)
{
	linearVelocity = velocity;
}

void Physics::RigidBody2D::SetForce(Vector2D force)
{
	this->force = force;
}

void Physics::RigidBody2D::AddForce(Vector2D additive_force)
{
	force += additive_force;
}

void Physics::RigidBody2D::SetAngularVelocity(float angular_velocity)
{
	this->angularVelocity = angular_velocity;
}

void Physics::RigidBody2D::SetAngularAccelleration(float angular_accelleration)
{
	this->angularAccelleration = angular_accelleration;
}

void Physics::RigidBody2D::SetMass(float newMass)
{
	mass = newMass;

	invMass = 1 / mass;
}

void Physics::RigidBody2D::SetInertia(float newInertia)
{
	inertia = newInertia;

	invInertia = 1 / newInertia;
}

void Physics::RigidBody2D::SetBodyMotionType(BodyMotionType state)
{
	motion_type = state;
}

bool Physics::RigidBody2D::Overlap(RigidBody2D& bodyB)
{
	//in future come up with a smarter method of calculating what type of intersection testing is required
	//The intention here is that the overlap function can dynamically choose (data-driven) the intersection test to perform.
	//Maybe use bit fields to check what kind of intersection to do.
	//For now we will trivially use an enum for handling it (better than messy dynamic casts)


	Shape& bodyAShape = *(this->shape);
	Shape& bodyBShape = *(bodyB.shape);

	return bodyAShape.Overlap(bodyBShape); //The shape them selves know how to resolve collisions the body just has it for accessibility
}

void Physics::RigidBody2D::AddImpulse(Vector2D impulse)
{
	this->linearVelocity += impulse * invMass;
}

void Physics::RigidBody2D::SetRestituition(float value)
{
	restituition = value;
}

void Physics::RigidBody2D::SetFrictionCoefficent(float value)
{
	frictional_coefficent = value;
}

void Physics::RigidBody2D::SetCollisionType(BodyCollisionType body_type)
{
	collision_type = body_type;
}
