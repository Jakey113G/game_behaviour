#pragma once
#include "Shape.h"
#include "Vector2D.h"
#include "SegmentLine.h"

namespace Physics
{
	class AABB : public Shape
	{
	public:
		AABB(Vector2D min, Vector2D max);
		AABB(float max_extends_x, float max_extends_y);
		const Vector2D& MIN();
		const Vector2D& MAX();

		//Physics::AABB ColliderInWorld(Vector2D transformation);

		virtual Vector2D GetCenter() override;

		virtual void UpdatePosition(Vector2D& deltaPosition) override;
		virtual void SetPosition(Vector2D& newPosition) override;

		virtual Vector2D GetSize() override;

		std::vector<Vector2D> GetPoints();
		Vector2D ClosestPointAABB(Vector2D point); //Used to replace my code in calculate nearest segment to point - taken from RealTime collision detection book section 5.1.3 
		float SqrDistancePointAABB(Vector2D point); //Taken from RealTime collision detection book section 5.1.3 

		//Calculate edge that of box that is nearest 
		SegmentLine ReturnNearestSegmentToPoint(Vector2D point);
		virtual Vector2D CalculateContactNormal(Vector2D contact) override;

		virtual CollisionPair HitDataBetweenShapes(Shape* shape) override;
		CollisionPair HitDataAABB(AABB* shape);
	private:
		Vector2D min_extend;
		Vector2D max_extend;
	public:
		bool PointInside(Vector2D point);
	};
}