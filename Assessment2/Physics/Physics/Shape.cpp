#include "Shape.h"
#include "Vector2D.h"

#include "AABB.h"
#include "Circle.h"

Physics::Shape::~Shape()
{
}

Vector2D Physics::Shape::GetCenter()
{
	return Vector2D();
}

Vector2D Physics::Shape::GetSize()
{
	return Vector2D();
}

void Physics::Shape::UpdatePosition(Vector2D& deltaPosition)
{
	
}

void Physics::Shape::SetPosition(Vector2D& newPosition)
{

}

Vector2D Physics::Shape::CalculateContactNormal(Vector2D contact)
{
	return Vector2D();
}

Physics::CollisionPair Physics::Shape::HitDataBetweenShapes(Shape* shape)
{	
	return CollisionPair();
}

bool Physics::Shape::AABBoverlapAABB(AABB* bodyA, AABB* bodyB)
{
	Physics::AABB* obja = bodyA;
	Physics::AABB* objb = bodyB;

	//More efficient early out. Escape if one of the axis fail to overlap 
	if (obja->MAX().x < objb->MIN().x || obja->MIN().x > objb->MAX().x)
		return false;
	if (obja->MAX().y < objb->MIN().y || obja->MIN().y > objb->MAX().y)
		return false;
	return true;
}

bool Physics::Shape::CircleOverlapAABB(Circle* circle_shape, AABB* aabb_shape)
{
	Vector2D circle_center = circle_shape->GetCenter();
	float circle_radius = circle_shape->GetRadius();

	float sqDist = aabb_shape->SqrDistancePointAABB(circle_center);

	return sqDist <= circle_radius * circle_radius;
}

bool Physics::Shape::CircleOverlapCircle(Circle* circleA, Circle* circleB)
{
	//calculate length of extends
	float extends = circleA->GetRadius() + circleB->GetRadius();
	
	//calculate displacement between shapes.
	Vector2D displacement = circleA->GetCenter() - circleB->GetCenter();
	
	return displacement.Length() < extends;
}

bool Physics::Shape::Overlap(Shape& bodyBShape)
{
	//todo turn into bit fields to improve which overlap to choose - 
	//or make it into an array of pointers to which overlap to return

	//AABB - AABB
	if (GetColliderType() == AABB_Collider && bodyBShape.GetColliderType() == AABB_Collider)
	{
		return AABBoverlapAABB(dynamic_cast<AABB*>(this), dynamic_cast<AABB*>(&bodyBShape));
	}
	//CIRCLE - CIRCLE
	else if (bodyBShape.GetColliderType() == CIRCLE_Collider && GetColliderType() == CIRCLE_Collider)
	{
		return CircleOverlapCircle(dynamic_cast<Circle*>(this), dynamic_cast<Circle*>(&bodyBShape));
	}
	//AABB - CIRCLE
	else if (GetColliderType() == CIRCLE_Collider && bodyBShape.GetColliderType() == AABB_Collider)
	{
		return CircleOverlapAABB(dynamic_cast<Circle*>(this), dynamic_cast<AABB*>(&bodyBShape));
	}
	//CIRCLE - AABB
	else if(GetColliderType() == AABB_Collider && bodyBShape.GetColliderType() == CIRCLE_Collider)
	{
		return CircleOverlapAABB(dynamic_cast<Circle*>(&bodyBShape), dynamic_cast<AABB*>(this));
	}
	return false;
}