#include "OBB.h"
#include "AABB.h"
#include <cmath>

Physics::OBB::OBB(AABB& aabb_before, AABB& aabb_after)
{
	Vector2D aabb_position_before = aabb_before.GetCenter();
	Vector2D aabb_position_after = aabb_after.GetCenter();
	
	//calculate center points of aabb find the midpoint of x and y.
	center_point = Vector2D((aabb_position_before + aabb_position_after) * 0.5f);
	
	//pythagoras - on x and y deltas. divided by 2. Half extends
	float deltaX = std::abs(aabb_after.MAX().x - aabb_before.MIN().x);
	float deltaY = std::abs(aabb_after.MAX().y - aabb_before.MIN().y);
	Vector2D hypotenus_vector = Vector2D(deltaX, 0).Squared() + Vector2D(0, deltaY).Squared();
	extends_from_center = hypotenus_vector.SquareRoot() * 0.5f;

	//calculate degree of rotation to apply to this obb
	axis_rotation[0] = hypotenus_vector.Normal(); //this vector is the x axis along the 
	axis_rotation[1] = Vector2D(hypotenus_vector.y, hypotenus_vector.x).Normal(); //flip the x and y to create an opposing direction vector
}

Physics::OBB::OBB(Vector2D position, Vector2D extends) : center_point(), extends_from_center(), axis_rotation()
{

}

Vector2D Physics::OBB::GetCenter()
{
	return center_point;
}

void Physics::OBB::UpdatePosition(Vector2D& deltaPosition)
{
	center_point += deltaPosition;
}

void Physics::OBB::SetPosition(Vector2D& newPosition)
{
	center_point = newPosition;
}

void Physics::OBB::UpdateRotation(float rotation)
{
	//add hanndling for constraining axis rotation between 0 - 360	
	
	//rotate the axis	
}

void Physics::OBB::SetAxis(Vector2D xaxis, Vector2D yaxis)
{
	axis_rotation[0] = xaxis;
	axis_rotation[1] = yaxis;
}
