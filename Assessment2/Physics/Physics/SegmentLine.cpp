#include "SegmentLine.h"
#include "Shape.h"
#include "AABB.h"

Physics::SegmentLine::SegmentLine(Vector2D origin, Vector2D end_point)
{
	m_origin = origin;
	m_end_point = end_point;
}

Physics::SegmentLine::SegmentLine()
{

}

Vector2D Physics::SegmentLine::Direction()
{
	return (m_end_point - m_origin); //should be a vector with 0 as origin.
}

Vector2D Physics::SegmentLine::BiTangentNormal()
{
	Vector2D normal = Direction().Normal();
	return Vector2D(normal.y, normal.x);
}

bool Physics::SegmentLine::IsIntersectsShape(Shape* collider)
{
	if (collider->GetColliderType() == AABB_Collider)
	{
		return RayIntersectAABB(static_cast<AABB*>(collider));
	}
	return false;
}

bool Physics::SegmentLine::RayIntersectAABB(AABB* aabb)
{
	//Cheat - divide the ray up into points and test each point.
	//if any point passes return true;
	//this isn't the standard way of doing it which should be based on planes of the object do we ever cross and then you work out based on distance 

	Vector2D ray_increments = Direction().Normal() * (Direction().Length() * 0.1f); // Ray direction * 1/10 of ray length

	for (int i = 0; i < 11; ++i) //process the 11th increment too
	{
		if (aabb->PointInside(m_origin + (ray_increments * (float)i)))
		{
			return true;
		}
	}
	return false;
}

