#include "SpringConstraint.h"
#include "RigidBody2D.h"

Physics::SpringConstraint::SpringConstraint()
{

}

Physics::SpringConstraint::SpringConstraint(RigidBody2D* a, RigidBody2D* b) : Constraint(a, b)
{

}

//Fairly certain gravity is preventing my damping from working fully
void Physics::SpringConstraint::ResolveConstraint(float deltatime)
{
	//We only resolve between two bodies if one body is missing there is nothing to resolve
	if (bodyA == nullptr || bodyB == nullptr)
		return;

	//Vector between the two masses attached to the spring
	Vector2D spring_vec = bodyB->GetPosition() - bodyA->GetPosition();

	//Distance between the two masses.
	float length = spring_vec.Length();

	float difference_of_extension = (length - RestLength);
	if (difference_of_extension != 0.0f)
	{
		float force_magnitude = -Stiffness * difference_of_extension;   // The magnitude of the spring force is calculated using the Hooke's law
		Vector2D normalized_spring_direction = spring_vec.Normal();

		//increase the magnitude for the world physics scale - here

		if (bodyA->IsDynamic())
		{
			//Apply the spring force to the two bodies joined by the spring
			//Note: The spring force has a direction which is given by the vector between BodyB and BodyA

			Vector2D linear_velocity = bodyA->GetLinearVelocity();
			Vector2D total_force_to_add = ((normalized_spring_direction * -1.0f) * force_magnitude) + (linear_velocity * -Dampen);
			bodyA->AddForce(total_force_to_add);
		}
		if (bodyB->IsDynamic())
		{
			Vector2D linear_velocity = bodyB->GetLinearVelocity();
			Vector2D total_force_to_add = (normalized_spring_direction * force_magnitude) + (linear_velocity * -Dampen);
			bodyB->AddForce(total_force_to_add);
		}
	}
}