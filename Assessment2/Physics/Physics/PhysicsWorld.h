#pragma once
#include <vector>
#include "Vector2D.h"
#include "RigidBodySolver.h"
#include "PhysicsEventManager.h"

namespace Physics
{
	class RigidBody2D;
	class SegmentLine;
	class Constraint;

	//The class that holds the representation of the game world in physics.
	//In theory the game only needs to interact through physics world for general purpose stuff
	//Special physics cases will rely on... more direct interference.
	class PhysicsWorld
	{
		friend class Game;
		typedef std::vector<RigidBody2D*> PhysicsObjects;
		typedef std::vector<Constraint*> PhysicsConstraints;
	public:
		PhysicsWorld();

		//////////////////////////////////////////////////////////////////////////
		//World handling

		const Vector2D&	GetGravity();
		void	SetGravity(Vector2D);

		//Physics Body collection management
		void	AddPhysicsEntity(RigidBody2D*);
		void	RemoveWorldEntity(RigidBody2D*);

		//Physics Constraint collection management
		void	AddPhysicsConstraint(Constraint*);
		void	RemovePhysicsConstraint(Constraint*);

		const PhysicsObjects& PhysicsEntities();
		void BindFunctionToBody(RigidBody2D* body_of_event, EPhysicsEvent physics_event, std::function<void(RigidBody2D*)>*);

		void Update(float frameTimeDuration);

		//public query functions to return all bodies that overlap the body argument.
		const std::vector<Physics::HitDataResult>& OverlappingRigidBodies(Physics::RigidBody2D* body);
		const std::vector<Physics::HitDataResult>& HitRigidBodies(Physics::RigidBody2D* queryBody);
		std::vector<Physics::RigidBody2D*> RayCastWorld(Physics::SegmentLine& ray);
		std::vector<Physics::RigidBody2D*> RayCastWorld(Vector2D origin, Vector2D end);

		std::vector<CollisionPair> ShapeCastWorld(Physics::Shape& shape, Vector2D start, Vector2D end);
		
		//Function for testing if a shape is penetrating an object. It returns required displacements to not be penetrating
		//Collision handling resolves this kind of thing by result. This exists for games to potentially query if a body they control would penetrate something.
		Vector2D PenetrationQuery(Physics::Shape&);

		//Internal functions
	private:
		void ProcessConstraints(float frameDelta);

		//Process all physics entities
		void UpdateAllPhysicEntities(float frameTimeDuration);

		//Process Collisions with the correct solvers.
		void HandleAllCollisionsConsecutively(float frameTime);

		//The highest level function for handling physics update for a body
		void UpdatePhysicsStep(Physics::RigidBody2D& body, float frameDelta);
		void UpdateBodyPhysicsProperties(Physics::RigidBody2D& body, float); //Update forces
		void UpdateBodyApplyPhysics(Physics::RigidBody2D& body, float); //Apply physics to the body positions

		bool PrimitiveSimpleBroadPhaseCheck(Physics::RigidBody2D* bodyA, Physics::RigidBody2D* bodyB);

		//internal Members
	private:
		RigidBodySolver rigidbody_solver;
		PhysicsEventManager event_manager;
		Vector2D gravity;

		PhysicsObjects physics_entities;
		PhysicsConstraints physics_constraints;
	};
}