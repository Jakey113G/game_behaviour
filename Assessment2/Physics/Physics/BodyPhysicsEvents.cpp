#include "BodyPhysicsEvents.h"



Physics::BodyPhysicsEvents::BodyPhysicsEvents()
{

}

Physics::BodyPhysicsEvents::~BodyPhysicsEvents()
{

}

void Physics::BodyPhysicsEvents::InvokeEvents()
{
	//if (OverlappingEnter_Event.Triggered())
	//{
	//	OverlappingEnter_Event.InvokeEvent();
	//}

	if (Overlapping_Event.Triggered())
	{
		Overlapping_Event.InvokeEvent();
	}

	//if (Overlapping_Event.Triggered())
	//{
	//	OverlappingExit_Event.InvokeEvent();
	//}

	if (Hit_Event.Triggered())
	{
		Hit_Event.InvokeEvent();
	}
}

void Physics::BodyPhysicsEvents::ClearEventOccurances()
{
	this->Overlapping_Event.Clear();
	this->Hit_Event.Clear();
}

void Physics::BodyPhysicsEvents::Overlap(Physics::HitDataResult& hit_data)
{
	Overlapping_Event.AddToEvent(hit_data);
}

void Physics::BodyPhysicsEvents::Hit(Physics::HitDataResult& hit_data)
{
	Hit_Event.AddToEvent(hit_data);
}

void Physics::BodyPhysicsEvents::ProcessEventsBegin()
{
	if (!m_processStarted)
	{
		all_detected_collisions.clear();
		this->ClearEventOccurances();
		m_processStarted = true;
	}
}

void Physics::BodyPhysicsEvents::ProcessEventsEnd()
{
	m_processStarted = false;
}

void Physics::BodyPhysicsEvents::AddToDetectedCollisions(RigidBody2D* body)
{
	//add body if not in detected collisions
	if (std::find(all_detected_collisions.begin(), all_detected_collisions.end(), body) == all_detected_collisions.end())
	{
		all_detected_collisions.push_back(body);
	}
}

