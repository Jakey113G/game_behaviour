#include "AABB.h"

#include "Circle.h" //temporary as the aabb circle stuff is currently in circle.h not here

//center based aabb
Physics::AABB::AABB(Vector2D min, Vector2D max)
{
	type = ColliderType::AABB_Collider;
}

Physics::AABB::AABB(float max_extends_x, float max_extends_y)
{
	type = ColliderType::AABB_Collider;

	//create extends with 0 center
	Vector2D extends(max_extends_x, max_extends_y);
	min_extend = extends * -0.5f;
	max_extend = extends * 0.5f;
}

const Vector2D& Physics::AABB::MIN()
{
	return this->min_extend;
}

const Vector2D& Physics::AABB::MAX()
{
	return this->max_extend;
}

Vector2D Physics::AABB::GetCenter()
{
	return (max_extend + min_extend) * 0.5f;
}

//Physics::AABB Physics::AABB::ColliderInWorld(Vector2D transformation)
//{
//	return AABB(transformation + this->min_extend, transformation + this->max_extend);
//}

void Physics::AABB::UpdatePosition(Vector2D& deltaPosition)
{
	min_extend += deltaPosition;
	max_extend += deltaPosition;
}

//Used to change the position of existing aabb - still center position based
void Physics::AABB::SetPosition(Vector2D& newPosition)
{
	Vector2D extend = max_extend - min_extend;
	min_extend = newPosition + (extend * -0.5f);
	max_extend = newPosition + (extend * 0.5f);
}

//return the delta 
Vector2D Physics::AABB::GetSize()
{
	float deltaX = std::abs(max_extend.x - min_extend.x);
	float deltaY = std::abs(max_extend.y - min_extend.y);
	return Vector2D(deltaX, deltaY);
}

//Return the corner vertexs starting from top left in clockwise rotation.
std::vector<Vector2D> Physics::AABB::GetPoints()
{
	std::vector<Vector2D> vertexs(4);
	vertexs[0] = { min_extend.x, max_extend.y };
	vertexs[1] = max_extend;
	vertexs[2] = { max_extend.x, min_extend.y };
	vertexs[3] = min_extend;
	return vertexs;
}

Vector2D Physics::AABB::ClosestPointAABB(Vector2D point)
{
	//calculate nearest vertex of box
	Vector2D v = point;

	//Process X
	if (point.x < min_extend.x)
	{
		v.x = min_extend.x;
	}
	if (point.x > max_extend.x)
	{
		v.x = max_extend.x;
	}

	//Process Y
	if (point.y < min_extend.y)
	{
		v.y = min_extend.y;
	}
	if (point.y > max_extend.y)
	{
		v.y = max_extend.y;
	}

	return v;
}

//Slightly adapted but original code is from 5.1.3 RealTimeCollision Detection book.
float Physics::AABB::SqrDistancePointAABB(Vector2D point)
{
	float sqr_dist = 0.0f;
	
	float v = point.x;
	if (v < min_extend.x)
	{
		sqr_dist += (min_extend.x - v) * (min_extend.x - v);
	}
	if (v > max_extend.x)
	{
		sqr_dist += (max_extend.x - v) * (max_extend.x - v);
	}

	v = point.y;
	if (v < min_extend.y)
	{
		sqr_dist += (min_extend.y - v) * (min_extend.y - v);
	}
	if (v > max_extend.y)
	{
		sqr_dist += (max_extend.y - v) * (max_extend.y - v);
	}

	return sqr_dist;
}

Vector2D Physics::AABB::CalculateContactNormal(Vector2D contact)
{
	SegmentLine segment = ReturnNearestSegmentToPoint(contact); //returns the clockwise vertex of the segment
	return segment.BiTangentNormal(); //normal of edge
}

Physics::CollisionPair Physics::AABB::HitDataBetweenShapes(Shape* shape)
{
	if (this->GetColliderType() == AABB_Collider && shape->GetColliderType() == AABB_Collider)
	{
		return HitDataAABB(static_cast<AABB*>(shape));
	}

	//EXTREMLY TENTATIVE
	if (Circle* c = dynamic_cast<Circle*>(shape))
	{
		CollisionPair p = c->HitDataAABB(this);
		p.contact_normal *= -1.0f;
		return p;
	}
	
	throw new std::exception("No handling for physics shape");
}

Physics::CollisionPair Physics::AABB::HitDataAABB(AABB* shape)
{
	//this method works by calculating the delta differences between our colliders based on extends and positions.
	//generating points to ensure we only effect if points lie in box
	Vector2D shape1_extends = this->GetSize() * 0.5f;
	Vector2D shape2_extends = shape->GetSize() * 0.5f;

	Vector2D delta = shape->GetCenter() - this->GetCenter();

	float& dx = delta.x;
	float px = (shape1_extends.x + shape2_extends.x) - abs(dx);

	float& dy = delta.y;
	float py = (shape1_extends.y + shape2_extends.y) - abs(dy);

	CollisionPair hit_data;
	if (px < py)
	{
		float sx = dx < 0 ? -1.0f : 1.0f;
		dx = px * sx;

		hit_data.contact_normal.x = sx;
		hit_data.contact_point.x = GetCenter().x + (shape1_extends.x * sx);
		hit_data.contact_point.y = shape->GetCenter().y;
		hit_data.manifold_seperation.x = dx;
	}
	else
	{
		float sy = dy < 0 ? -1.0f : 1.0f;
		dy = py * sy;

		hit_data.contact_normal.y = -sy;
		hit_data.contact_point.y = GetCenter().y + (shape1_extends.y * sy);
		hit_data.contact_point.x = shape->GetCenter().x;
		hit_data.manifold_seperation.y = dy;
	}
	return hit_data;
}

bool Physics::AABB::PointInside(Vector2D point)
{
	//Basically separating axis test using a point instead of side
	if (point.x < min_extend.x || point.x > max_extend.x)
		return false;
	if (point.y < min_extend.y || point.y > max_extend.y)
		return false;
	return true;
}

Physics::SegmentLine Physics::AABB::ReturnNearestSegmentToPoint(Vector2D point)
{
	//distance from center
	Vector2D distance_from_center = point - this->GetCenter();

	//calculate nearest vertex of box
	Vector2D nearest_vertex;
	//Left most side
	distance_from_center.x < 0 ? nearest_vertex.x = min_extend.x : nearest_vertex.x = max_extend.x;
	//Up side
	distance_from_center.y < 0 ? nearest_vertex.y = max_extend.y : nearest_vertex.y = min_extend.y;

	//evaluate the side from the nearest vertex
	//make assumption the thing furthest away from closest vertex is our intended axis
	Vector2D distance_to_vertex = Vector2D(point - nearest_vertex).Squared();
	
	//X axis is our intended segment 
	if (distance_to_vertex.x > distance_to_vertex.y)
	{
		//if right side - return bottom (clockwise movement of side). Otherise return top
		float correct_y_vertex_for_segment = distance_from_center.x > 0 ? min_extend.y : max_extend.y;
		float sign_of_entend = distance_from_center.x > 0 ? 1.0f : -1.0f;
		
		Vector2D end_vertex(nearest_vertex.x, correct_y_vertex_for_segment);
		Vector2D start_vertex(nearest_vertex.x, correct_y_vertex_for_segment  + (sign_of_entend * GetSize().y));
		return SegmentLine(start_vertex,end_vertex);
	}
	//Y axis is intended segment
	else
	{
		//if top side - return right (clockwise movement of side). Otherwise return left
		float correct_x_vertex_for_segment = distance_from_center.y < 0 ? max_extend.x : min_extend.x;
		float sign_of_entend = distance_from_center.y < 0 ? -1.0f : 1.0f;

		Vector2D end_vertex(correct_x_vertex_for_segment, nearest_vertex.y);
		Vector2D start_vertex(correct_x_vertex_for_segment + (sign_of_entend * GetSize().x), nearest_vertex.y);
		return SegmentLine(start_vertex, end_vertex);
	}
}