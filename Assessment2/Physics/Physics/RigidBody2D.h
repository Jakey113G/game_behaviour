#pragma once
#include "Vector2D.h"
#include "Transform.h"
#include "Shape.h"
#include <iostream>

namespace Physics
{
	class Shape;

	//Very Basic Collision filtering - Would be nice to create a bit field of collision objects in a collision layer system.
	enum BodyCollisionType
	{
		IGNORE_COLLISION,
		BLOCKING_COLLISION,
		OVERLAP_COLLISION
	};

	enum BodyMotionType
	{
		STATIC_MOTION,		//Object never moves no physics performed on the object
		KINEMATIC_MOTION,	//Object has all the behavior of a static body except can be move in the world
		DYNAMIC_MOTION		//Object has full physics applied to its behavior.
	};

	struct BodyData
	{
		BodyData()
		{
			angularVelo = 0;
			angularAccelleration = 0;
			mass = 1; //lets not devault it to 0
			motion_type = DYNAMIC_MOTION;
			collision_type = IGNORE_COLLISION;
			frictionCoefficent = 1.5f;
		}

		Vector2D position;
		Vector2D linearVelo;
		Vector2D force;
		float frictionCoefficent;
		float angularVelo;
		float angularAccelleration;
		float mass;
		BodyMotionType motion_type;
		BodyCollisionType collision_type;
	};

	class RigidBody2D
	{
		friend class RigidBodySolver; //Direct private access for BodySolver - this is for efficiency, allows direct access rather than having to go through function calls.

		//sf::Transformable* transform;				//Position of the body
		Vector2D position;
		float rotation;

		Vector2D linearVelocity;				    //linear velocity, for translational motion
		Vector2D force;								//body force

		float angularVelocity;						//angular velocity, for rotational motion - one dimensions
		float angularAccelleration;
		float mass;
		float invMass;
		float inertia;
		float invInertia;

		float restituition;
		float frictional_coefficent;

		//Intention is to collapse all these bool conditions down into a single int32 using bit masks.
		BodyMotionType motion_type;

		//bool is_static  //position and rotation are frozen
		//bool ignore_gravity;
		//bool lock_rotation;

		bool sleeping;
		BodyCollisionType collision_type;
		Shape* shape;

	public:
		//////////////////////////////////////////////////////////////////////////
		///Constructors
		//////////////////////////////////////////////////////////////////////////
		RigidBody2D();
		//RigidBody2D(sf::Transformable*);
		RigidBody2D(Vector2D position);
		RigidBody2D(BodyData data);
		RigidBody2D(BodyData data, Shape* shape);
		virtual ~RigidBody2D();

		//////////////////////////////////////////////////////////////////////////
		//Position
		//////////////////////////////////////////////////////////////////////////

		//Get a const safe center of mass position
		inline const Vector2D GetPosition()
		{
			return position;

			//TODO: Possible a little intensive - as Vector2D has to be constructed from the existing transform position vector type
			//return transform->getPosition();
		}

		void SetPosition(Vector2D vector);

		void AddPosition(Vector2D delta_position);

		//////////////////////////////////////////////////////////////////////////
		///LinearVelocity
		//////////////////////////////////////////////////////////////////////////

		//Get a linear velocity copy.
		inline Vector2D GetLinearVelocity()
		{
			return linearVelocity;
		}

		void AddLinearVelocity(Vector2D velocity);
		void SetLinearVelocity(Vector2D velocity);

		//////////////////////////////////////////////////////////////////////////
		///Force
		//////////////////////////////////////////////////////////////////////////

		//Get a const safe current force.
		inline const Vector2D GetForce()
		{
			return force;
		}

		void SetForce(Vector2D force);
		void AddForce(Vector2D additive_force);

		//////////////////////////////////////////////////////////////////////////
		///AngularVelocity
		//////////////////////////////////////////////////////////////////////////

		//Get angular velocity of rigid body
		inline float GetAngularVelocity()
		{
			return angularVelocity;
		}

		void SetAngularVelocity(float angular_velocity);

		//////////////////////////////////////////////////////////////////////////
		///AngularAccelleration
		//////////////////////////////////////////////////////////////////////////

		//Get angular accelleration value
		inline float GetAngularAccelleration()
		{
			return angularAccelleration;
		}

		void SetAngularAccelleration(float angular_accelleration);

		//////////////////////////////////////////////////////////////////////////
		///Mass
		//////////////////////////////////////////////////////////////////////////

		//Get float value of mass of rigid body
		inline float GetMass()
		{
			return mass;
		}

		//Set mass and recal invMass
		void SetMass(float newMass);

		//////////////////////////////////////////////////////////////////////////
		///InvMass
		//////////////////////////////////////////////////////////////////////////

		//Get inverted mass - cache value
		inline float GetInvMass()
		{
			return invMass;
		}

		//////////////////////////////////////////////////////////////////////////
		///Inertia
		//////////////////////////////////////////////////////////////////////////

		//Get value of inertia of rigid body
		inline float GetInertia()
		{
			return inertia;
		}

		//Set inertia and recalculate inverted inverted inertia
		void SetInertia(float newInertia);

		//////////////////////////////////////////////////////////////////////////
		///invInertia
		//////////////////////////////////////////////////////////////////////////
		inline float GetInvertedInertia()
		{
			return invInertia;
		}

		//////////////////////////////////////////////////////////////////////////
		///Kinematic (aka static non-moving entity)
		//////////////////////////////////////////////////////////////////////////

		inline bool IsStatic()
		{
			return motion_type == STATIC_MOTION;
		}

		inline bool IsDynamic()
		{
			return motion_type == DYNAMIC_MOTION;
		}

		inline bool IsKinematic()
		{
			return motion_type == KINEMATIC_MOTION;
		}

		void SetBodyMotionType(BodyMotionType state);

		//////////////////////////////////////////////////////////////////////////
		///Sleeping (avoid processing if sleeping needs awakening by collision with other body)
		//////////////////////////////////////////////////////////////////////////

		inline bool IsSleeping()
		{
			return sleeping;
		}

		inline void WakeUp()
		{
			sleeping = false;
		}

		inline void Sleep()
		{
			sleeping = true;
		}

		inline Shape* GetColliderShape()
		{
			return shape;
		}

		void SetShapeCollider(Shape* shape)
		{
			this->shape = shape;
		}

		inline float GetRestituition()
		{
			return restituition;
		}

		void SetRestituition(float value);

		inline float GetFrictionCoefficent()
		{
			return frictional_coefficent;
		}

		void SetFrictionCoefficent(float value);

		inline BodyCollisionType GetCollisionType()
		{
			return collision_type;
		}

		void SetCollisionType(BodyCollisionType);

		bool Overlap(RigidBody2D&);

		void AddImpulse(Vector2D);
	};
}