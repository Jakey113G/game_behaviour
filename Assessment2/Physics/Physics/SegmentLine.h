#pragma once
#include "Vector2D.h"

namespace Physics
{
	class Shape;
	class AABB;
	class SegmentLine
	{
	public:
		SegmentLine();
		SegmentLine(Vector2D, Vector2D);
		Vector2D Direction();
		Vector2D BiTangentNormal();

	private:
		Vector2D m_origin;
		Vector2D m_end_point;
	public:
		bool IsIntersectsShape(Shape* param1);
	private:
		bool RayIntersectAABB(AABB* aabb);
	};

	//typedef Segment Ray; //Mathmatically Similiar
};