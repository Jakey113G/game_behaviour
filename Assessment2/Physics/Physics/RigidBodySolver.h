#pragma once
#include "Vector2D.h"

namespace Physics
{
	class PhysicsEventManager;
	class CollisionPair;
	class RigidBody2D;
	class Shape;

	class RigidBodySolver
	{
	public:
		RigidBodySolver(PhysicsEventManager*, const Vector2D&);
		void Resolve(CollisionPair&, float);
	private:
		//Vector2D CalculateIntersectionDepth(Shape& shape1, Shape& shape2);

		void InElasticCollision(CollisionPair& pair);
		void ElasticCollision(CollisionPair& pair, float delta);
		void ApplyFrictionByVelocity(CollisionPair& pair, float delta);
		void ApplyFrictionToBody(RigidBody2D& pair, Vector2D& surface_normal, Vector2D& change_in_velocity, float friction_coefficent, float delta);

		void ResolvePenetration(CollisionPair& pair, float delta);
		void ResolvePenetrationDynamicStatic(CollisionPair& pair, float delta); //when bodies collide with a static
		void ResolvePenetrationDynamicDynamic(CollisionPair& pair, float delta); //when both bodies are moving and collide

		//the resolver reports has detailed knowledge on the interactions and events - so the solver has a pointer to enable communication.
		const Vector2D& gravity;
		PhysicsEventManager* event_manager; 
	};
}