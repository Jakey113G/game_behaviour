#pragma once
#include <functional>
#include <vector>
#include <algorithm>
#include "HitDataResult.h"

namespace Physics
{
	class RigidBody2D;

	//Template exists here for convenience of changing the delegating functions
	//template<class FunctionType, class... Args> 
	//removed due to complexity of storing different event functions. Maybe could have done polymorphic but could have got too complex & too long to make
	class PhysicsEvent
	{
	public:
		PhysicsEvent();
		~PhysicsEvent();

		//Return true if bodies are involved with this event
		inline bool Triggered()
		{
			return !bodies_in_event.empty();
		}

		void InvokeEvent();

		//So a outside system can actively poll for information
		const std::vector<Physics::HitDataResult>& EventResult()
		{
			return bodies_in_event;
		}

		inline void AddToEvent(Physics::RigidBody2D* body, Vector2D normal)
		{
			if (!AlreadyRegisteredInEvent(body))
			{
				Physics::HitDataResult hit_data(body, normal);
				bodies_in_event.push_back(hit_data);
			}
		}

		inline void AddToEvent(Physics::HitDataResult& hit_data)
		{
			if (!AlreadyRegisteredInEvent(hit_data.body))
			{
				bodies_in_event.push_back(hit_data);
			}
		}
		
		inline bool AlreadyRegisteredInEvent(Physics::RigidBody2D* body)
		{
			if (std::find(bodies_in_event.begin(), bodies_in_event.end(), body) != bodies_in_event.end())
			{
				return true;
			}
			return false;
		}

		//Remove bodies in event.
		void Clear();

		//Dangerous - but the correct safe design pattern has not been put in place yet
		inline std::vector<HitDataResult>& Bodies()
		{
			return bodies_in_event;
		}

	private:
		std::vector<HitDataResult> bodies_in_event;

		//collection of delegate functions to be invoked on the event
		std::vector<std::function<void(HitDataResult&)>> collection_of_delegate_function_calls;
		bool event_triggered;
	};
}