#pragma once
#include <unordered_map>
#include <functional>
#include "BodyPhysicsEvents.h"

//managing class that holds possession of events and allows mapping/delegating to external functions if implemented
//this class can be polled to see the state of physics - such as is hitting something or overlapping something.

//The way we map the physics event is by an unordered_map of rigid body pointers.
//map is more memory efficient but unordered is better for look up. 

namespace Physics
{
	class RigidBody2D;
	class PhysicsEventManager
	{
		friend class PhysicsWorld; //Only creat-able in specific relationship

	public:
		//Return the physics event state of a rigid body. The event state can be used to query current physics events such as overlaps
		BodyPhysicsEvents* GetEventState(RigidBody2D*);

		//void EmptyEvents(RigidBody2D*);
		void Invoke(RigidBody2D*);
	private:
		PhysicsEventManager();
		~PhysicsEventManager();

		//Decided to remove the templating as it become very complex to handle
		//template< class F, class... Args>
		void AddCallBackEvent(RigidBody2D* body_of_event, EPhysicsEvent physics_event, std::function<void(RigidBody2D*)>*)
		{
			BodyPhysicsEvents* body_event = GetEventState(body_of_event);
			if (body_event)
			{
				switch (physics_event)
				{
				case OVERLAP_ENTER_EVENT:
					return;
				case OVERLAP_EVENT:
					return;
				case OVERLAP_EXIT_EVENT:
					return;
				case HIT_EVENT:
					return;
				default:
					return;
				}
			}
		}

	private:
		std::unordered_map<RigidBody2D*, BodyPhysicsEvents> body_events;
	public:
		//void Process(RigidBody2D*);
	};
}