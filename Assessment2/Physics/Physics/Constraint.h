#pragma once

namespace Physics
{
	class RigidBody2D;

	//A constraint is something that only exists between physics bodies.
	class Constraint
	{
	public:
		Constraint();
		Constraint(RigidBody2D*, RigidBody2D*);
		virtual ~Constraint();

		void SetConstraintBodies(RigidBody2D*, RigidBody2D*);
		virtual void ResolveConstraint(float) = 0;
	protected:
		RigidBody2D* bodyA;
		RigidBody2D* bodyB;
	};
}