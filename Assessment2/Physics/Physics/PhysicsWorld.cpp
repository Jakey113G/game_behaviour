#include "PhysicsWorld.h"
#include "RigidBody2D.h"
#include "Constraint.h"
#include "CollisionPair.h"
#include "SegmentLine.h"

#include "PhysicsEvent.h"
#include "AABB.h"
#include "Circle.h"

#define SUBSTEP_ITERATIONS 10
#define SUBSTEP_MULTIPLIER 0.1f

#define GRAVITY_STRENGTH 200.0f

//Gravity has been tuned to a value that just feels better for game play - changing the world scale would fix this better so I can use a value of 9.81 to refer to larger movements
Physics::PhysicsWorld::PhysicsWorld() : gravity(0, GRAVITY_STRENGTH), rigidbody_solver(&event_manager, gravity)
{

}

const Vector2D& Physics::PhysicsWorld::GetGravity()
{
	return gravity;
}

void Physics::PhysicsWorld::SetGravity(Vector2D value)
{
	gravity = value;
}

void Physics::PhysicsWorld::AddPhysicsEntity(RigidBody2D* entity)
{
	physics_entities.push_back(entity);
	event_manager.body_events[entity]; //will default initialize BodyPhysicEvent class mapped to this rigid body
}

void Physics::PhysicsWorld::RemoveWorldEntity(RigidBody2D* destroyedObject)
{
	//Remove the pointer from vector - the game entity class manages its lifetime we just remove it from a physics look up container 
	auto it = std::find(physics_entities.begin(), physics_entities.end(), destroyedObject);
	if (it != physics_entities.end())
	{
		physics_entities.erase(it);
	}
	event_manager.body_events.erase(destroyedObject);
}

void Physics::PhysicsWorld::AddPhysicsConstraint(Constraint* c)
{
	physics_constraints.push_back(c);
}

void Physics::PhysicsWorld::RemovePhysicsConstraint(Constraint* c)
{
	auto it = std::find(physics_constraints.begin(), physics_constraints.end(), c);
	if (it != physics_constraints.end())
	{
		physics_constraints.erase(it);
	}
}

const Physics::PhysicsWorld::PhysicsObjects& Physics::PhysicsWorld::PhysicsEntities()
{
	return physics_entities;
}


void Physics::PhysicsWorld::BindFunctionToBody(RigidBody2D* body_of_event, EPhysicsEvent physics_event, std::function<void(RigidBody2D*)>* func)
{
	event_manager.AddCallBackEvent(body_of_event, physics_event, func);
}

void Physics::PhysicsWorld::UpdateBodyPhysicsProperties(Physics::RigidBody2D& body, float frameDelta)
{
	Vector2D acceleration = body.GetForce() * body.GetInvMass();

	//add acceleration over time (delta) to the velocity.
	Vector2D newVelcocity = body.GetLinearVelocity() + (GetGravity() + acceleration) * frameDelta * SUBSTEP_MULTIPLIER;
	
	//cap terminal velocity handling here
	if (newVelcocity.Length() > 1500.0f)
	{
		newVelcocity = newVelcocity.Normal() * 1500.0f;
	}

	body.SetLinearVelocity(newVelcocity); //set the velocity to oldVelocity + acceleration * delta time (* sub step multiplier; if we are doing the velocity 10 times only do a tenth of the velocity).
}

void Physics::PhysicsWorld::UpdateBodyApplyPhysics(Physics::RigidBody2D& body, float frameDelta)
{
	//calculate new position for this frame
	Vector2D deltaPosition = body.GetLinearVelocity() * frameDelta * SUBSTEP_MULTIPLIER;
	body.AddPosition(deltaPosition);

	//Update collider
	body.GetColliderShape()->UpdatePosition(deltaPosition);
}

//This is a very basic simple broad phase collision test. Multiple the shape size by 2 and test if the distance is within the broad phase distances.
//The fact it has to calculate the broad phase each time is bad for performance. Future revision would have the broad phase already calculated.
bool Physics::PhysicsWorld::PrimitiveSimpleBroadPhaseCheck(Physics::RigidBody2D* bodyA, Physics::RigidBody2D* bodyB)
{
	//calculate object distance 
	float distance = Vector2D(bodyA->GetPosition() - bodyB->GetPosition()).Length();

	//calculate broad phase separation distance
	float broad_collision_distance = Vector2D(bodyA->GetColliderShape()->GetSize() * 2.0f).Length() + Vector2D(bodyB->GetColliderShape()->GetSize() * 2.0f).Length();

	//if object distance exceed exceeds extend size - reject the collision
	return distance < broad_collision_distance;
}

void Physics::PhysicsWorld::UpdatePhysicsStep(Physics::RigidBody2D& body, float frameDelta)
{
	//Update the velocity, acceleration in the simulation
	UpdateBodyPhysicsProperties(body, frameDelta);

	//Update the position of the rigid body from the body physics
	UpdateBodyApplyPhysics(body, frameDelta);
}

void Physics::PhysicsWorld::UpdateAllPhysicEntities(float frameTimeDuration)
{
	//Update all physics entities doesn't take a modified physics step.
	//but we need to divide the values we apply in these steps which is why we use a substep multiplier (changing the time delta could cause a slightly different effect when processed)
	ProcessConstraints(frameTimeDuration);

	//Update physics
	for (RigidBody2D* body : physics_entities)
	{
		if (body->IsDynamic() && !body->IsSleeping()) //Not static(dynamic moving) and not sleeping
		{
			UpdatePhysicsStep(*body, frameTimeDuration);
		}
	}
}

void Physics::PhysicsWorld::HandleAllCollisionsConsecutively(float frameTime)
{
	//TODO At some point add SPATIAL PARTIONING
	std::vector<CollisionPair> pairs;

	//detect collisions
	for (RigidBody2D*& bodyA : physics_entities)
	{
		//No events yet
		event_manager.GetEventState(bodyA)->ProcessEventsBegin();

		//If body ignore collisions skip the test.
		//-Something that ignore collisions won't need a collision check
		//-Statics don't move - no point testing there movement collision
		//-Sleeping won't have new collisions unless collided with as sleeping means they are not receiving physics steps.
		if (bodyA->GetCollisionType() != IGNORE_COLLISION && !bodyA->IsStatic() && !bodyA->IsSleeping())
		{
			for (RigidBody2D* bodyB : physics_entities)
			{
				if (bodyA == bodyB)
				{
					continue;
				}

				//Collision with bodyB is ignored no need to test collision.  
				if (bodyB->GetCollisionType() == IGNORE_COLLISION)
				{
					continue;
				}

				//A very simple broad phase collision Cull.
				//Removed because generating the broad phase is actually potentially more costly than the performance saved within my game.
				//Would be useful to use if I had a lot more bodies or changed my system so that the broad phase does not require regenerating every test.
				//if (!PrimitiveSimpleBroadPhaseCheck(bodyA, bodyB))
				//{
				//	continue;
				//}
				
				//Check for collision
				if (bodyA->Overlap(*bodyB))
				{
					//check their is not already a collision pair
					bool exists = false;
					for (CollisionPair& p : pairs)
					{
						if (p.bodyB == bodyA && p.bodyA == bodyB)
						{
							exists = true;
							break;
						}
					}
					if (exists) { continue; }
					
					CollisionPair pair = bodyA->GetColliderShape()->HitDataBetweenShapes(bodyB->GetColliderShape());
#ifdef _DEBUG
					//std::cout << "NORMAL: " << pair.contact_normal.x << " " << pair.contact_normal.y << std::endl;
#endif // _DEBUG
					pair.bodyA = bodyA;
					pair.bodyB = bodyB;
					pairs.push_back(pair);

					//Add body b to known collisions with body A and vice versa
					event_manager.GetEventState(bodyA)->AddToDetectedCollisions(bodyB);
					event_manager.GetEventState(bodyB)->AddToDetectedCollisions(bodyA);
				}
			}
			//Updates what is in the event_begin overlap, event_exit overlap and hit event
			//event_manager.Process();
		}
	}

	//resolve narrow collisions
	for (CollisionPair& pair : pairs)
	{
		rigidbody_solver.Resolve(pair, frameTime);
	}
}

void Physics::PhysicsWorld::ProcessConstraints(float frameDelta)
{
	for (Constraint* constraint : physics_constraints)
	{
		constraint->ResolveConstraint(frameDelta);
	}
}

void Physics::PhysicsWorld::Update(float frameTimeDuration)
{
	for (int i = 0; i < SUBSTEP_ITERATIONS; ++i)
	{
		float step_time = frameTimeDuration * SUBSTEP_MULTIPLIER;

		UpdateAllPhysicEntities(frameTimeDuration);

		//sub-step resolve - look into fixing this so impulse does not reply multiple times.
		HandleAllCollisionsConsecutively(step_time);

		/*
		//Apply Drag - if not colliding
		for (RigidBody2D* body : physics_entities)
		{
			if (body->IsKinematic()){continue;}

			if (!event_manager.GetEventState(body)->Hit_Event.Triggered())
			{
				//reduce velo by 0.5% each frame - fake slowing of wind
				Vector2D reduce = body->GetLinearVelocity() * 0.995f;
				body->SetLinearVelocity(reduce);
			}
		}
		*/
	}
	

	//Trigger delegates and clears the active collision events lists for the bodies in our physics environment
	for (RigidBody2D* body : this->physics_entities)
	{
		event_manager.GetEventState(body)->InvokeEvents();
		event_manager.GetEventState(body)->ProcessEventsEnd();
	}

	//zero the body force's after it has been processed.
	for (RigidBody2D* body : physics_entities)
	{
		body->SetForce(Vector2D(0, 0));
	}
}

const std::vector<Physics::HitDataResult>& Physics::PhysicsWorld::OverlappingRigidBodies(Physics::RigidBody2D* queryBody)
{
	BodyPhysicsEvents* body_events = event_manager.GetEventState(queryBody);
	if (body_events)
	{
		return body_events->Overlapping_Event.EventResult();
	}

	throw new std::exception("QueryBody has no collision event class - shouldn't happen as physics bodies in this system should have a BodyEvent object");
}

const std::vector<Physics::HitDataResult>& Physics::PhysicsWorld::HitRigidBodies(Physics::RigidBody2D* queryBody)
{
	BodyPhysicsEvents* body_events = event_manager.GetEventState(queryBody);
	if (body_events)
	{
		return body_events->Hit_Event.EventResult();
	}

	throw new std::exception("QueryBody has no collision event class - shouldn't happen as physics bodies in this system should have a BodyEvent object");
}

std::vector<Physics::RigidBody2D*> Physics::PhysicsWorld::RayCastWorld(Physics::SegmentLine& ray)
{
	std::vector<Physics::RigidBody2D*> contacts;
	for (RigidBody2D* body : this->physics_entities)
	{
		if (body->GetCollisionType() == BLOCKING_COLLISION)
		{
			if (ray.IsIntersectsShape(body->GetColliderShape()))
			{
				contacts.push_back(body);
			}
		}
	}

	return contacts;
}

std::vector<Physics::RigidBody2D*> Physics::PhysicsWorld::RayCastWorld(Vector2D origin, Vector2D end)
{
	Physics::SegmentLine ray(origin, end);
	return RayCastWorld(ray);
}

std::vector<Physics::CollisionPair> Physics::PhysicsWorld::ShapeCastWorld(Physics::Shape& shape, Vector2D start, Vector2D end)
{
	shape.SetPosition(start);

	Vector2D direction_of_cast = end - start;
	direction_of_cast = direction_of_cast.Normal();

	Vector2D size_to_displace = shape.GetSize().Length() * direction_of_cast;

	float length_of_shape_cast = Vector2D(start - end).Length();
	Vector2D distance_travelled;
	std::vector<CollisionPair> collisions_from_cast;

	//Keep iterating the shape by its extends in the direction of the travel.
	while (distance_travelled.Length() < length_of_shape_cast) //calculate length of shape towards end and if the distance is less 
	{
		for (RigidBody2D* other_objects : physics_entities)
		{
			//don't generate multiple contacts with same body
			auto found_pair = (std::find_if(collisions_from_cast.begin(), collisions_from_cast.end(), [other_objects](const CollisionPair& pair)-> bool {return pair.bodyB == other_objects; }));
			if(found_pair != collisions_from_cast.end())
			{
				continue;
			}

			if (Shape* other = other_objects->GetColliderShape())
			{
				if (shape.Overlap(*other))
				{
					CollisionPair p = shape.HitDataBetweenShapes(other);
					p.bodyB = other_objects;
					collisions_from_cast.push_back(std::move(p));
				}
			}
		}
		distance_travelled += size_to_displace;
		shape.UpdatePosition(size_to_displace);
	}
	return collisions_from_cast;
}

//Collision handling resolves this kind of thing by default. This exists for games to potentially query if a body they control would penetrate something.
Vector2D Physics::PhysicsWorld::PenetrationQuery(Physics::Shape& shape)
{
	Vector2D penetration_resolve(0, 0);
	for (Physics::RigidBody2D* body : physics_entities)
	{
		if (body->GetCollisionType() == BLOCKING_COLLISION)
		{
			if (body->GetColliderShape()->Overlap(shape))
			{
				penetration_resolve += body->GetColliderShape()->HitDataBetweenShapes(&shape).manifold_seperation;
				return penetration_resolve;
			}
		}
	}
	return penetration_resolve;
}