#pragma once
#include <vector>
#include "CollisionPair.h"

class Vector2D;

namespace Physics
{
	enum ColliderType
	{
		None = 0,
		AABB_Collider = 1,
		CIRCLE_Collider = 2,
		OBB_Collider = 3,
		COMPLEX_Collider = 4
	};
	class AABB;
	class Circle;
	class Shape
	{
	public:
		virtual ~Shape();
		const ColliderType GetColliderType()
		{
			return type;
		}

		//Non pure virtual because lazy - refactor later
		//virtual void ColliderInWorld(Vector2D transformation);

	protected:
		ColliderType type;
	public:
		virtual Vector2D GetCenter();
		virtual Vector2D GetSize();

		virtual void UpdatePosition(Vector2D& deltaPosition);
		virtual void SetPosition(Vector2D& newPosition);

		virtual Vector2D CalculateContactNormal(Vector2D contact);

		virtual CollisionPair HitDataBetweenShapes(Shape* shape);

		static bool AABBoverlapAABB(AABB* bodyA, AABB* bodyB);
		static bool CircleOverlapAABB(Circle* body_circle, AABB* body_box);
		static bool CircleOverlapCircle(Circle* circleA, Circle* circleB);
		
		bool Overlap(Shape& bodyBShape);
	};
};