#include "Constraint.h"
#include "RigidBody2D.h"

Physics::Constraint::Constraint()
{

}

Physics::Constraint::~Constraint()
{

}

Physics::Constraint::Constraint(RigidBody2D* body_1, RigidBody2D* body_2) : bodyA(body_1), bodyB(body_2)
{

}

void Physics::Constraint::SetConstraintBodies(RigidBody2D* physicsbody_1, RigidBody2D* physicsbody_2)
{
	bodyA = physicsbody_1;
	bodyB = physicsbody_2;
}
