#pragma once
#include "PhysicsEvent.h"
#include <vector>

namespace Physics
{
	enum EPhysicsEvent
	{
		NOEVENT,
		OVERLAP_ENTER_EVENT,
		OVERLAP_EVENT,
		OVERLAP_EXIT_EVENT,
		HIT_EVENT
	};


	class RigidBody2D;
	//container of physics events. This container should be associated with a rigid body.
	//If the events are ever polled it will fire off the bound functions each time with the event related bodies
	class BodyPhysicsEvents
	{
		friend class PhysicsWorld;
		friend class PhysicsEventManager;
	public:
		BodyPhysicsEvents();
		~BodyPhysicsEvents();

		void InvokeEvents();
		void ClearEventOccurances();

		void AddToDetectedCollisions(RigidBody2D*);
		void Overlap(HitDataResult&);
		void Hit(HitDataResult&);

		void ProcessEventsBegin();
		void ProcessEventsEnd();

	public:
		//TODO: Change it to return not just a collection of bodies for the event but also event information such as the HIT Info.
		//preferable as a struct of { body , hit info }

		//Physics events have been created that will hold a collection of pointers to the rigid body for the events.
		PhysicsEvent OverlappingEnter_Event;
		PhysicsEvent Overlapping_Event;
		PhysicsEvent OverlappingExit_Event;

		PhysicsEvent Hit_Event;

		std::vector<RigidBody2D*> all_detected_collisions;
	private:
		bool m_processStarted;
	};
}