#include "Waypoint.h"
#include "EdgeRelation.h"
#include <algorithm>

//This static counter is always incremented when creating new Waypoint's, ensuring a unique ID.
static int instance_counter = 0;

AI::Waypoint::Waypoint() : unique_identifier(instance_counter++)
{
	acceptable_range = 35.0f;
}

AI::Waypoint::Waypoint(Vector2D location) : position(location), unique_identifier(instance_counter++)
{
	acceptable_range = 35.0f;
}

AI::Waypoint::Waypoint(float x, float y) : position(x,y), unique_identifier(instance_counter++)
{
	acceptable_range = 35.0f;
}

AI::Waypoint::Waypoint(const Waypoint& copy_from) : position(copy_from.position), unique_identifier(copy_from.unique_identifier), acceptable_range(copy_from.acceptable_range)
{

}

const Vector2D AI::Waypoint::GetLocation()
{
	return position;
}

std::vector<AI::EdgeRelation>& AI::Waypoint::GetEdgeRoutes()
{
	return routes_from_node;
}

void AI::Waypoint::CreateEdgeToWaypoint(Waypoint& wp)
{
	float val = DistanceTo(&wp);
	Hueristic cost_to_node((int)val);
	AI::EdgeRelation edge_relation(this, &wp, cost_to_node);
	routes_from_node.push_back(edge_relation);
}

float AI::Waypoint::DistanceTo(Waypoint* point)
{
	return Vector2D(point->GetLocation() - position).Length();
}

bool AI::Waypoint::InAcceptableRangeOfWaypoint(Vector2D& ai_position, float value)
{
	return (Vector2D(position - ai_position).Length() < value);
}

int AI::Waypoint::ID()
{
	return unique_identifier;
}

void AI::Waypoint::RemoveEdgeToNode(Waypoint& way)
{
	auto it = std::find_if(routes_from_node.begin(), routes_from_node.end(), [way](AI::EdgeRelation& route)->bool { return route.GetDestination() == &way; });
	if (it != routes_from_node.end())
	{
		routes_from_node.erase(it);
	}
}

void AI::Waypoint::SetEntrySize(float value)
{
	acceptable_range = value;
}

float AI::Waypoint::GetEntrySize()
{
	return acceptable_range;
}

bool AI::Waypoint::HasEdge(AI::Waypoint* point)
{
	auto it = std::find_if(routes_from_node.begin(), routes_from_node.end(), [point](EdgeRelation& edge)->bool{return edge.GetDestination()->ID() == point->ID(); });
	if (it != routes_from_node.end())
	{
		return true;
	}
	return false;
}

//bool AI::Waypoint::IsInaccesible()
//{
//	for(EdgeRelation& edge : routes_from_node)
//	{
//		if (edge.GetState() == EnumEdgeRelationState::PASSABLE)
//		{
//			return false;
//		}
//	}
//	return true;
//}
