#include "AIWorld.h"
#include "BaseAgent.h"
#include "WorldRepresentation.h"
#include "Operator.h"
#include "WaypointWorld.h"

#include <Physics/PhysicsWorld.h>

//typedef AI::WayPointWorld RepresentWorld;
//typedef AI::Waypoint	Node;

AI::AIWorld::AIWorld(WorldRepresentation*)
{

}

AI::AIWorld::AIWorld()
{

}

AI::AIWorld::~AIWorld()
{
	delete world;

	//World operators should be owned by game entities in actual game.
	world_operators.clear();
}

void AI::AIWorld::AddAgent(BaseAgent* agent)
{
	agents.push_back(agent);
}

void AI::AIWorld::RemoveAgent(BaseAgent* agent)
{
	//Remove the pointer from vector - the game entity class manages its lifetime we just remove it from the ai_world update 
	auto it = std::find(agents.begin(), agents.end(), agent);
	if (it != agents.end())
	{
		agents.erase(it);
	}
}

//AI::AStarPath AI::AIWorld::RouteToGoal(Waypoint* origin, Waypoint* goal)
//{

//}

void AI::AIWorld::Update(float delta)
{
	//Code to update the Operator if it needs to update (after thinking about it, I decided I might not need this)
	//for (AI::Operator* operator_in_ai_world : world_operators)
	//{
	//	operator_in_ai_world->Update(delta);
	//}

	for (BaseAgent* agent : agents)
	{
		agent->UpdateAgent(this, delta);
	}
}

void AI::AIWorld::SetRepresentation(AI::WayPointWorld* ai_representation)
{
	world = ai_representation;
}

AI::WayPointWorld* AI::AIWorld::GetRepresentation()
{
	return world;
}

void AI::AIWorld::AddOperatorToWorld(Operator* op)
{
	world_operators.push_back(op);
}

void AI::AIWorld::RemoveOperatorFromAIWorld(Operator* op)
{
	auto it = std::find(world_operators.begin(), world_operators.end(), op);
	if (it != world_operators.end())
		world_operators.erase(it);
}

std::vector<AI::BaseAgent*>& AI::AIWorld::GetAgents()
{
	return agents;
}

AI::Waypoint* AI::AIWorld::GetWaypointClosestToPoint(Vector2D point)
{
	AI::Waypoint* min = nullptr;
	float smallest_seperation = 99999;

	for (AI::Waypoint& wp : world->world_waypoints)
	{
		Vector2D location_of_waypoint = wp.GetLocation();
		Vector2D displacement_to_node(location_of_waypoint - point);

		if (displacement_to_node.Length() < smallest_seperation)
		{
			min = &wp;
			smallest_seperation = displacement_to_node.Length();
		}
	}
	return min;
}

std::vector<AI::Waypoint*> AI::AIWorld::WaypointsInRange(Waypoint* target_waypoint, float range)
{
	std::vector<AI::Waypoint*> points;
	Vector2D location_of_target = target_waypoint->GetLocation();

	for (AI::Waypoint& waypoint : world->world_waypoints)
	{
		if (*target_waypoint == waypoint)
		{
			continue;
		}

		if (Vector2D(waypoint.GetLocation() - location_of_target).Length() < range)
		{
			points.push_back(&waypoint);
		}
	}
	return points;
}

AI::AStarPath AI::AIWorld::GetPathRoute(Waypoint* a, Waypoint* b)
{
	AStar_Search search_nodes; //decide if should make static
	search_nodes.Setup(a, b);
	return search_nodes.PerformFullSearch();
}

std::vector<AI::Operator*>& AI::AIWorld::GetOperators()
{
	return world_operators;
}


void AI::AIWorld::GenerateRoutesByNodeDistance(Waypoint& world_waypoint, float distance)
{
	for (AI::Waypoint* point : WaypointsInRange(&world_waypoint, distance))
	{
		if (world_waypoint.HasEdge(point))
		{
			continue;
		}

		//access the physics to evaluate if the edge should be made

		//I know we are told to keep physics ai and game seperate but I feel like too much will be going into the game unless I do this.
		if (physics_world->RayCastWorld(world_waypoint.GetLocation(), point->GetLocation()).empty())
		{
			//Just makes edges if in range. My dynamic edge updating will disable the route if it is in-crossable.
			world_waypoint.CreateEdgeToWaypoint(*point);
			point->CreateEdgeToWaypoint(world_waypoint);
		}
	}
}

//This overload exists for the case where we are spawning the node at a location where a character would exist and we need to allow that collision
//This is the moment to have really needed to put in the collision filtering in my physics query utilties.
void AI::AIWorld::GenerateRoutesByNodeDistanceIgnoreCollisions(Waypoint& world_waypoint, float distance)
{
	for (AI::Waypoint* point : WaypointsInRange(&world_waypoint, distance))
	{
		if (world_waypoint.HasEdge(point))
		{
			continue;
		}

		//access the physics to evaluate if the edge should be made

		//Just makes edges if in range. My dynamic edge updating will disable the route if it is in-crossable.
		world_waypoint.CreateEdgeToWaypoint(*point);
		point->CreateEdgeToWaypoint(world_waypoint);		
	}
}

void AI::AIWorld::SetPhysicsQuery(Physics::PhysicsWorld* physical_world)
{
	physics_world = physical_world;
}
