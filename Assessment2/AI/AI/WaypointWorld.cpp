#include "WaypointWorld.h"
#include <algorithm>

AI::WayPointWorld::WayPointWorld()
{

}

AI::WayPointWorld::~WayPointWorld()
{

}

void AI::WayPointWorld::AddWayPoint(Waypoint& w)
{
	world_waypoints.push_back(std::move(w));
}

AI::Waypoint& AI::WayPointWorld::CreateWayPoint(Vector2D location)
{
	world_waypoints.push_back(Waypoint(location));
	return world_waypoints.back();
}

void AI::WayPointWorld::JoinWaypoints(Waypoint& wayPointA, Waypoint& wayPointB)
{
	wayPointA.CreateEdgeToWaypoint(wayPointB);
	wayPointB.CreateEdgeToWaypoint(wayPointA);
}

void AI::WayPointWorld::JoinWaypoints(int a, int b)
{	
	AI::Waypoint& wayA = GetWayPointByID(a);
	AI::Waypoint& wayB = GetWayPointByID(b);

	JoinWaypoints(wayA, wayB);
}

void AI::WayPointWorld::RemoveEdge(Waypoint& wayPointA, Waypoint& wayPointB)
{
	wayPointA.RemoveEdgeToNode(wayPointA);
	wayPointB.RemoveEdgeToNode(wayPointB);
}

AI::Waypoint& AI::WayPointWorld::GetWayPointByID(int id)
{
	for (AI::Waypoint& waypoint : world_waypoints)
	{
		if (waypoint.ID() == id)
		{
			return waypoint;
		}
	}

	throw new std::exception("No waypoint with the provided ID exists");
}

void AI::WayPointWorld::RemoveWayPoint(AI::Waypoint& waypoint)
{	
	auto it = std::find(world_waypoints.begin(), world_waypoints.end(), waypoint);
	if (it != world_waypoints.end())
	{
		world_waypoints.erase(it);
	}
}

void AI::WayPointWorld::RemoveWayPoint(Vector2D location)
{
	auto it = std::find_if(world_waypoints.begin(), world_waypoints.end(), [location](AI::Waypoint& point)->bool {return point.GetLocation() == location; });
	if (it != world_waypoints.end())
	{
		world_waypoints.erase(it);
	}
}
