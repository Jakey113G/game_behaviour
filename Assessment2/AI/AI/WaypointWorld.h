#pragma once
#include "WorldRepresentation.h"
#include "Waypoint.h"
#include <vector>

namespace AI
{
	class WayPointWorld : public WorldRepresentation
	{
		friend class AIWorld;
	public:
		WayPointWorld();;
		virtual ~WayPointWorld();;

		//virtual void CalculateWayPoints() = 0;

		//Waypoint* ReturnClosestWaypointTo(Vector2D point);
		//AStarPath GetPathToWayPoint(Waypoint&);

		void AddWayPoint(Waypoint&);
		AI::Waypoint& CreateWayPoint(Vector2D location);

		void JoinWaypoints(Waypoint&, Waypoint&);
		void JoinWaypoints(int, int);
		void RemoveEdge(Waypoint& wayPointA, Waypoint& wayPointB);

		AI::Waypoint& GetWayPointByID(int id);
		void RemoveWayPoint(Waypoint&);
		void RemoveWayPoint(Vector2D location);

		std::vector<Waypoint> world_waypoints; //the node map for ai to use when navigating routes
	};
}