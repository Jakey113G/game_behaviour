#pragma once

namespace AI
{
	class Hueristic
	{
	public:
		Hueristic() {};
		Hueristic(int value)
		{
			cached_hueristic = value;
		}

		virtual ~Hueristic() {};

		virtual void CalculateHueristicValue() {};

		inline int Value()
		{
			return cached_hueristic;
		}

		void SetHeristicValue(int value)
		{
			cached_hueristic = value;
		}

	protected:
		int cached_hueristic;
	};
}