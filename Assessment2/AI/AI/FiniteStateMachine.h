#pragma once
#include <map>
#include <string>
#include "State.h"

namespace AI
{
	//Base abstract class for finite state machines.
	//It doesn't make sense how to derive from it at the moment as you can't really enforce different enums as the param type.
	//Maybe this is the correct way: http://stackoverflow.com/questions/8357240/how-to-automatically-convert-strongly-typed-enum-into-int
	
	//Potential (Un-neat) solutions:
	//1)Decide to just handle things as pointers and do it all through casting.
	//2)Use maps with strings as the key but I feel maps is too overkill in my opinion.
	//3)Template it all 

	//For now I will ignore the pure virtual design.

	class FiniteStateMachine
	{
	public:
		FiniteStateMachine() {};
		virtual ~FiniteStateMachine() {};
		//virtual void Update(const int event_value, State& existing_state) = 0;
		
		//void AddState(std::string, State);
		//std::map<std::string, State> machine_states;
	};
}