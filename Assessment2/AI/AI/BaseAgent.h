#pragma once
#include "AStarPath.h"

namespace AI
{
	class AIWorld;
	class BaseAgent
	{
	public:
		BaseAgent();
		virtual ~BaseAgent();

		//Detects events in the ai_world
		virtual void UpdateAgent(AIWorld*,float) = 0;

		virtual bool EvaluateIfPossibleToReachPointFromNode() = 0;

		virtual bool PathForAIStillValid(AStarPath&) = 0;

		inline AStarPath& GetCalculatedPath()
		{
			return path_to_goal;
		}

	protected:
		AStarPath path_to_goal;
	};
}