#pragma once
#include "Hueristic.h"
#include <Physics/Vector2D.h>

namespace AI
{
	//Abstract class to interface with AI world. Anything that is special and not related to navigation is interfaced with this
	class Operator
	{
	public:
		virtual ~Operator() {};
		virtual Vector2D GetLocation() = 0;
		Hueristic value_of_this_operator;
	};
}