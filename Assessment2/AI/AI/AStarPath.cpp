#include "AStarPath.h"
#include "Waypoint.h"

typedef AI::Waypoint NodeType;

std::vector<NodeType*>& AI::AStarPath::GetRoute()
{
	return route_order;
}

int AI::AStarPath::IndexOfNode(NodeType* node)
{
	for (int i = 0; i < route_order.size(); ++i)
	{
		if (route_order[i] == node)
		{
			return i;
		}
	}
	return 0; //couldn't find node.
}

void AI::AStarPath::AddEdge(EdgeRelation& edge)
{
	route_order.push_back(edge.GetDestination());
	route_cost = route_cost + edge.GetEdgeModifiedHueristicValue();

	BlackListNode(*edge.GetDestination());
}

NodeType* AI::AStarPath::Advance()
{
	if (current_node != route_order.back())
	{
		for ( int i = 0; i < route_order.size(); ++i)
		{
			if (route_order[i] == current_node)
			{
				current_node = route_order[i + 1];
				return current_node;
			}
		}
	}

	return current_node;
}

bool AI::AStarPath::StillAdvancable()
{
	return current_node != route_order.back();
}

std::vector<NodeType*>& AI::AStarPath::GetPreviousNodes()
{
	return previously_visited_nodes;
}

NodeType* AI::AStarPath::GetLastNodeInPath()
{
	return route_order.back();
}

NodeType* AI::AStarPath::GetCurrentNode()
{
	return current_node;
}

NodeType* AI::AStarPath::GetNextNodeInRoute()
{
	int index = IndexOfNode(current_node);
	if (index + 1 >= route_order.size())
	{
		return route_order[index];
	}

	return route_order[index + 1];
}

int AI::AStarPath::RouteCost()
{
	return route_cost;
}

bool AI::AStarPath::IsBlackListed(NodeType& node)
{
	for (NodeType* black_node : previously_visited_nodes)
	{
		if (&node == black_node)
		{
			return true;
		}
	}
	return false;
}

//Potential routes from the end of the path
int AI::AStarPath::PotentialRoutes()
{
	int count = 0;

	for (AI::EdgeRelation& edges : GetLastNodeInPath()->GetEdgeRoutes())
	{
		if (!IsBlackListed(*edges.GetDestination()))
		{
			count++;
		}
	}
	return count;
}

void AI::AStarPath::BlackListNode(NodeType& node)
{
	previously_visited_nodes.push_back(&node);
}

void AI::AStarPath::NewRoute(NodeType& origin)
{
	route_cost = 0;

	route_order.clear();
	previously_visited_nodes.clear();

	route_order.push_back(&origin);
	previously_visited_nodes.push_back(&origin);

	current_node = &origin;
}

AI::AStarPath::AStarPath()
{

}

AI::AStarPath::AStarPath(NodeType& origin_node)
{
	route_order.push_back(&origin_node);
	BlackListNode(origin_node);
	current_node = &origin_node;
	route_cost = 0;
}

AI::AStarPath::AStarPath(const AStarPath& spawn_tree)
{
	route_order = spawn_tree.route_order;

	previously_visited_nodes = spawn_tree.previously_visited_nodes;

	route_cost = spawn_tree.route_cost;
	current_node = spawn_tree.current_node;
}