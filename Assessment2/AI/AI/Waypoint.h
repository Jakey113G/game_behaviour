#pragma once
#include <vector>
#include "EdgeRelation.h"
#include <Physics/Vector2D.h> //This isn't actually physics dependent. TODO Move physics else where

namespace AI
{
	enum class WayPointType
	{
		LOCATION_POINT,
		SMOOTHING_POINT,
		OBJECTIVE_POINT
	};

	class Waypoint
	{
	public:
		Waypoint();
		Waypoint(Vector2D location);
		Waypoint(float x, float y);
		Waypoint(const Waypoint& copy_from);

		const Vector2D GetLocation();
		std::vector<EdgeRelation>& GetEdgeRoutes();

		void CreateEdgeToWaypoint(Waypoint&);
		float DistanceTo(Waypoint* point);

		bool InAcceptableRangeOfWaypoint(Vector2D& position, float);

		inline EdgeRelation* GetEdgeToWayPoint(AI::Waypoint& point)
		{
			for (EdgeRelation& edge : routes_from_node)
			{
				if (edge.GetDestination() == &point) // the address of node type matches that of point - same object
				{
					return &edge;
				}
			}
			return nullptr;
		}

		int ID();

		void RemoveEdgeToNode(Waypoint&);

		void SetEntrySize(float value);
		float GetEntrySize();

		bool operator==(const Waypoint& query_point) const
		{
			return query_point.unique_identifier == this->unique_identifier;
		}

	private:
		int unique_identifier;
		float acceptable_range;
		Vector2D position;
		std::vector<EdgeRelation> routes_from_node;
	public:
		bool HasEdge(AI::Waypoint* point);
		bool IsInaccesible();
	};
}
