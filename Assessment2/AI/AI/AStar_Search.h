#pragma once
#include "AStarPath.h"
#include "Waypoint.h"
#include <vector>

namespace AI
{
	class AStar_Search
	{
		class Waypoint;
	public:

		AStar_Search();
		AStar_Search(AI::Waypoint* initial_node, AI::Waypoint* completion_condition_node);

		void Setup(AI::Waypoint* start, AI::Waypoint* end);

		AStarPath PerformFullSearch();


		//The initial set up required for the search
		void StartSearch();

		void AdvanceSearchStep();

		void EvaluateAndChangeToLowestCostingPath();

		float GetHeuristicCost(AI::Waypoint* point);

		void AdvanceCurrentPath();

		void CreateNewPath(EdgeRelation& edge);

		bool  EvaluatePathToGoal();

	private:
		AI::Waypoint* starting_point;
		AI::Waypoint* destination_point;

		std::vector<AStarPath*> potential_paths; //made into heap allocation for now
		AStarPath* current_path;
		bool searching_for_goal;
	};
}