#pragma once
#include "AStar_Search.h"
#include <vector>

namespace Physics { class PhysicsWorld; }

namespace AI
{
	class BaseAgent;
	class Waypoint;
	class WorldRepresentation;
	class WayPointWorld;
	class Operator;

	//typedef AI::WayPointWorld WorldW;
	//typedef AI::Waypoint	Node;

	//Detected events in the world representation that gets forwarded to the ai agents.
	//Update ai agents in the world.
	class AIWorld
	{
	public:
		AIWorld();
		AIWorld(WorldRepresentation*);
		virtual ~AIWorld();

		void AddAgent(BaseAgent* agent);
		void RemoveAgent(BaseAgent* agent);

		//WayPointPath AStar_CalculateRouteToGoal(Waypoint& origin, Waypoint& goal);
		//AStarPath RouteToGoal(Waypoint* origin, Waypoint* goal);

		//Advance the AI logic within 
		void Update(float delta);

		void SetRepresentation(AI::WayPointWorld* ai_representation);
		AI::WayPointWorld* GetRepresentation();
		
		void AddOperatorToWorld(Operator*);
		void RemoveOperatorFromAIWorld(Operator*);

		std::vector<AI::BaseAgent*>& GetAgents();

		Waypoint* GetWaypointClosestToPoint(Vector2D point);
		std::vector<Waypoint*> WaypointsInRange(Waypoint* waypoint, float range);

		AStarPath GetPathRoute(Waypoint* a, Waypoint* b);
		
		std::vector<AI::Operator*>& GetOperators();
		void GenerateRoutesByNodeDistance(Waypoint& world_waypoint, float distance);
		void GenerateRoutesByNodeDistanceIgnoreCollisions(Waypoint& world_waypoint, float distance);
		void SetPhysicsQuery(Physics::PhysicsWorld* physical_world);
	private:
		WayPointWorld* world;
		Physics::PhysicsWorld* physics_world;

		std::vector<AI::BaseAgent*> agents; //This are our AI agents to update and manage in the AI world
		
		std::vector<AI::Operator*> world_operators;
	};
};