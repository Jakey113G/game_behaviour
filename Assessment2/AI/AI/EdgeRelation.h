#pragma once
#include "Hueristic.h"

namespace AI
{
	enum class EnumEdgeRelationState
	{
		PASSABLE,
		BLOCKED
	};

	class Waypoint;
	typedef Waypoint Node;
	class EdgeRelation
	{
	public:
		EdgeRelation();
		EdgeRelation(Node* a, Node* b);
		EdgeRelation(Node* a, Node* b, Hueristic hueristic);
	
		Node* GetOrigin();
		Node* GetDestination();

		void SetHueristic(Hueristic hue);
		Hueristic& GetHueristic();

		
		//this class takes a standard hueristic object and applies modifying values to the returned result
		inline  int GetEdgeModifiedHueristicValue()
		{
			if (state == EnumEdgeRelationState::BLOCKED)
			{
				return m_Value.Value() * 10;
			}
			return m_Value.Value();
		}

		void SetState(EnumEdgeRelationState);
		EnumEdgeRelationState GetState();
	private:
		Node* m_origin;
		Node* m_destination;
		Hueristic m_Value;
		EnumEdgeRelationState state;
	};
}