#pragma once
#include <vector>

//Templated so it can be used for grid cells or for waypoint nodes.
//template <typedef NodeType>

namespace AI
{
	class Waypoint;
	class EdgeRelation;
	class AStarPath
	{
		typedef AI::Waypoint NodeType; //Typedef in Header and CPP - just to enable easier refactoring in future.
		friend class AStar_Search;
	public:
		AStarPath();
		AStarPath(NodeType& origin_node);
		
		//Copy old route to new route
		AStarPath(const AStarPath& spawn_tree);

		void NewRoute(NodeType& origin);

		void BlackListNode(NodeType& node);
		bool IsBlackListed(NodeType& node);

		int PotentialRoutes();

		int RouteCost();
		NodeType* GetLastNodeInPath();
		NodeType* GetCurrentNode();
		NodeType* GetNextNodeInRoute();

		std::vector<NodeType*>& GetPreviousNodes();
		std::vector<NodeType*>& GetRoute();
		int IndexOfNode(NodeType* node);
		void AddEdge(AI::EdgeRelation& edge);
		NodeType* Advance();
		bool StillAdvancable();
	private:
		int route_cost;
		std::vector<NodeType*> route_order;
		std::vector<NodeType*> previously_visited_nodes;
		NodeType* current_node;
	};
}