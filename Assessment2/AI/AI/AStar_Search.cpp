#include "AStar_Search.h"
#include "EdgeRelation.h"
#include <algorithm>
#include <limits>

AI::AStar_Search::AStar_Search(AI::Waypoint* initial_node, AI::Waypoint* completion_condition_node)
{
	Setup(initial_node, completion_condition_node);
}

AI::AStar_Search::AStar_Search()
{

}

void AI::AStar_Search::Setup(AI::Waypoint* start, AI::Waypoint* end)
{
	if (!potential_paths.empty())
	{
		for (AStarPath* tree : potential_paths)
		{
			delete tree;
		}

		potential_paths.clear();
	}

	starting_point = start;
	destination_point = end;
	delete current_path;
	searching_for_goal = true;
}

AI::AStarPath AI::AStar_Search::PerformFullSearch()
{
	StartSearch();

	if (starting_point == destination_point)
	{
		current_path = new AStarPath(*starting_point);
		return *current_path;
	}

	while(searching_for_goal)
	{
		AdvanceSearchStep();
		EvaluatePathToGoal();
	}

	//Should be the best path to the destination
	return *current_path;
}

void AI::AStar_Search::StartSearch()
{
	//Create basic routes from start location 
	for (AI::EdgeRelation& edge : starting_point->GetEdgeRoutes())
	{
		AStarPath* new_route = new AStarPath(*starting_point);
 		new_route->AddEdge(edge);
		potential_paths.push_back(new_route);
	}

	if (potential_paths.size() == 0)
	{
		searching_for_goal = false;
		current_path = new AStarPath(*starting_point);
	}
}

void AI::AStar_Search::AdvanceSearchStep()
{
	EvaluateAndChangeToLowestCostingPath();
	AdvanceCurrentPath();
}

void AI::AStar_Search::EvaluateAndChangeToLowestCostingPath()
{
	//delete the old current_path - it is not in the potential paths at this point.
	delete current_path;

	float minimum_cost = std::numeric_limits<float>::max();
	for (AStarPath* route : potential_paths)
	{
		float heuristic_cost = GetHeuristicCost(route->GetLastNodeInPath());

		if (route->RouteCost() + heuristic_cost < minimum_cost)
		{
			minimum_cost = route->RouteCost() + heuristic_cost;
			current_path = route;
		}
	}
}

float AI::AStar_Search::GetHeuristicCost(AI::Waypoint* point)
{
	return destination_point->DistanceTo(point); //Change so the evaluation can be configured so it isn't done by just distance
}

void AI::AStar_Search::AdvanceCurrentPath()
{
	//Create multiple potential routes and discard the obsolete path 
	for (EdgeRelation& edge : current_path->GetLastNodeInPath()->GetEdgeRoutes())
	{
		if (current_path->IsBlackListed(*edge.GetDestination()))
		{
			continue;
		}

		//No longer remove edges. Only modify their heuristic
		//if (edge.GetState() == EnumEdgeRelationState::BLOCKED)
		//{
		//	continue;
		//}

		CreateNewPath(edge); //Should change so it advances existing path rather than creates instances of new paths
	}

	//Remove current path from potential paths vector
	potential_paths.erase(std::remove(potential_paths.begin(), potential_paths.end(), current_path), potential_paths.end());
}

void AI::AStar_Search::CreateNewPath(AI::EdgeRelation& edge)
{
	//Safety ensure that we only attempt to build a new node onto our path from an edge that is accessible to the head of the search tree.
	//check all edges, if a match is found create new path and return
	for (AI::EdgeRelation& edge_in_node : current_path->GetLastNodeInPath()->GetEdgeRoutes())
	{
		if (&edge == &edge_in_node)
		{
			AStarPath* newRoute = new AStarPath(*current_path);
			newRoute->AddEdge(edge);
			potential_paths.push_back(newRoute);
			return;
		}
	}
}

//change the searching for goal variable to decide if we should give up on getting to goal or not
bool AI::AStar_Search::EvaluatePathToGoal()
{	
	if (current_path->GetLastNodeInPath() == destination_point) //current path should be valid but not in a potential path list at this point.
	{
		searching_for_goal = false;
	}
	else if(potential_paths.size() == 0) //if previous check fails to break out of search. Check we have potential routes before continuing
	{
		searching_for_goal = false;
	}

	return searching_for_goal;
}
