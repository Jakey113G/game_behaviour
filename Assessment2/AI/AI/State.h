#pragma once

namespace AI
{
	class State
	{
	public:
		virtual void Update(float delta);
		virtual void Action();
	};
}