#include "EdgeRelation.h"
#include "Waypoint.h"

AI::EdgeRelation::EdgeRelation()
{

}

AI::EdgeRelation::EdgeRelation(Node* a, Node* b, Hueristic hueristic) : m_origin(a), m_destination(b), m_Value(hueristic)
{

}

AI::EdgeRelation::EdgeRelation(Node* a, Node* b) : m_origin(a), m_destination(b)
{

}

AI::Node* AI::EdgeRelation::GetOrigin()
{
	return m_origin;
}

AI::Node* AI::EdgeRelation::GetDestination()
{
	return m_destination;
}

void AI::EdgeRelation::SetHueristic(Hueristic hue)
{

}

AI::Hueristic& AI::EdgeRelation::GetHueristic()
{
	return m_Value;
}

void AI::EdgeRelation::SetState(EnumEdgeRelationState newState)
{
	state = newState;
}

AI::EnumEdgeRelationState AI::EdgeRelation::GetState()
{
	return state;
}
