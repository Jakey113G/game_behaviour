#pragma once
#include "BaseComponent.h"
#include <Physics/RigidBody2D.h>

//Wrapper to the physics stuff of my game entity.
class PhysicsComponent : public BaseComponent
{
public:
	PhysicsComponent();
	PhysicsComponent(Physics::RigidBody2D*);
	virtual ~PhysicsComponent();

	virtual void Begin(float delta) override;
	virtual void Update(float delta) override;

	Physics::RigidBody2D& GetRigidBody();

private:
	Physics::RigidBody2D* body;
};