#pragma once
#include "GameEntity.h"

class TriggerWall : public GameEntity
{
public:
	TriggerWall(float x, float y);
	
	void ToggleWall();

	bool active;

	virtual void BeginPlay() override;

};
