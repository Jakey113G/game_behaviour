#pragma once
#include "Character.h"

class Enemy : public Character
{
public:
	virtual ~Enemy();

	virtual void Update(float delta_time) override;

};