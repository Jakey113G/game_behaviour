#pragma once
#include <SFML/Graphics/Transformable.hpp>
#include "BaseComponent.h"

//This class is the main class for manipulating the game entity in the world
class Vector2D;

class TransformComponent : public BaseComponent
{
public:
	TransformComponent();

	inline const sf::Vector2f& GetLocation()
	{
		return transform.getPosition();
	}

	inline const sf::Transformable& GetTransformable()
	{
		return transform;
	}

	inline const sf::Transform& GetTransform() const
	{
		return transform.getTransform();
	}

	virtual void Begin(float delta) override;


	virtual void Update(float delta) override;

private:
	//This component owns a sf::transformable - rather than inherits a transform
	sf::Transformable transform;
public:
	void setPosition(Vector2D& newPosition);
	void setPosition(const Vector2D& position);
};