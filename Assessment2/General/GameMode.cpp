#include "GameMode.h"
#include "Game.h"

#include "SFML/Graphics/Text.hpp"
#include "AssetLibrary.h"

GameMode::GameMode(Game* game)
{
	game_in_play = game;
	game_complete = false;
}

GameMode::~GameMode()
{

}

//the update function for the gamemode that uses the game to evaluate its action
void GameMode::GameModeUpdate(float delta)
{
	if (GameOver())
	{
		return;
	}

	if (WinConditionCheck())
	{
		OnWin();
	}
	else if (LoseConditionCheck())
	{
		OnLose();
	}
}

//condition of the game mode
bool GameMode::GameOver()
{
	return game_complete;
}

//Invoke game mode into a state
void GameMode::SetGameWon()
{
	OnWin();
}

void GameMode::SetGameLost()
{
	OnLose();
}

void GameMode::SetWinConditionScore(int score_val)
{
	win_condition_score = score_val;
}

int GameMode::GetWinConditionScore()
{
	return win_condition_score;
}

void GameMode::IncrementScore()
{
	score++;
}

//////////////////////////////////////////////////////////////////////////
//Self checking conditions that can be used by the game mode to detect a game state
bool GameMode::WinConditionCheck()
{
	return  score == win_condition_score;  // if using a score system use this
}

bool GameMode::LoseConditionCheck()
{
	return game_complete; //The lose condition is directly changed by the world
}

void GameMode::OnLose()
{
	game_complete = true;

	sf::Font& font = AssetLibrary::GetInstance()->GetFont(FontKeyEnum::AARDV);

	sf::Text* game_message = game_in_play->GetCameraManager()->GetUI().CreateText();
	game_message->setFont(font);
	game_message->setOutlineColor(sf::Color::Black);
	game_message->setOutlineThickness(3.0f);
	game_message->setFillColor(sf::Color::Red);
	game_message->setString("Game Over!");
	game_message->setPosition(-850, -150);
	game_message->setCharacterSize(300);
}

void GameMode::OnWin()
{
	game_complete = true;

	sf::Font& font = AssetLibrary::GetInstance()->GetFont(FontKeyEnum::AARDV);

	sf::Text* game_message = game_in_play->GetCameraManager()->GetUI().CreateText();
	game_message->setFont(font);
	game_message->setOutlineColor(sf::Color::Black);
	game_message->setOutlineThickness(3.0f);
	game_message->setFillColor(sf::Color::Green);
	game_message->setString("You Win!");
	game_message->setPosition(-800, -150);
	game_message->setCharacterSize(300);
}
