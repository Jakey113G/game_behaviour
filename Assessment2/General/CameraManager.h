#pragma once
#include <Physics/Vector2D.h>
#include <SFML/Graphics/View.hpp>
#include "UI.h"

//TODO: CHANGE INTO A SINGLETON

namespace sf
{
	class RenderWindow;
}

class UI;
class GameEntity;

class CameraManager
{
	friend class Game;
public:
	CameraManager(sf::RenderWindow& win);

	void Update(float deltaFrame);
	void SetTarget(GameEntity*);
	
	sf::View& GetView();
	Vector2D GetPosition();
	UI& GetUI();
private:
	UI user_interface;
	sf::RenderWindow& window;
	GameEntity* target;

	sf::View view;
	float camera_move_boundary;
	void MoveWithInput(float deltaFrame);
public:
	void DrawView(sf::RenderWindow& window);
};