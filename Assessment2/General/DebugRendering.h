#pragma once
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <vector>

#include <Physics/RigidBody2D.h>
#include <Physics/AABB.h>
#include <Physics/Circle.h>

#include <AI/WaypointWorld.h>
#include <AI/Waypoint.h>
#include <AI/EdgeRelation.h>
#include <AI/BaseAgent.h>


//Purpose of this class is to render things for debugging such as my physics objects
class DebugRender
{
public:
	DebugRender()
	{}

	DebugRender(sf::RenderWindow& renderwindow) : window(&renderwindow)
	{}
	
	void DrawAllPhysics(const std::vector<Physics::RigidBody2D*>& bodies)
	{
		for (Physics::RigidBody2D* body : bodies)
		{
			DrawRigidBody(*body);
		}
		
		for (sf::VertexArray* debug_draw : other_debug_drawings)
		{
			window->draw(*debug_draw);
		}
	}

	void DrawAIRepresentation(AI::WayPointWorld* world)
	{
		for (AI::Waypoint& point : world->world_waypoints)
		{
			DrawPoint(point.GetLocation(), point.GetEntrySize(), sf::Color::Green);
			for (AI::EdgeRelation& edge : point.GetEdgeRoutes())
			{
				if (edge.GetState() == AI::EnumEdgeRelationState::PASSABLE) //Blocked routes are still use-able but more costly we hide them in debug view.
				{
					sf::VertexArray v = DrawLine(edge.GetOrigin()->GetLocation(), edge.GetDestination()->GetLocation(), sf::Color::Green);
					window->draw(v);
				}
			}
		}
	}

	void DrawAllAgentPaths(std::vector<AI::BaseAgent*>& agents)
	{
		for (AI::BaseAgent* agent : agents)
		{
			DrawCurrentAITree(agent->GetCalculatedPath(), sf::Color::Blue);
		}
	}

	void DrawCurrentAITree(AI::AStarPath& path, sf::Color color = sf::Color::Green)
	{
		for (AI::Waypoint* point : path.GetRoute())
		{
			DrawPoint(point->GetLocation(), point->GetEntrySize(), color);
			for (AI::EdgeRelation& edge : point->GetEdgeRoutes())
			{
				sf::VertexArray v = DrawLine(edge.GetOrigin()->GetLocation(), edge.GetDestination()->GetLocation(), color);
				window->draw(v);			
			}
		}
	}

	void DrawRigidBody(Physics::RigidBody2D& body)
	{
		//draw collider
		DrawCollider(body.GetColliderShape());

		//draw forces
		DrawForces(body);
	}

	void DrawCollider(Physics::Shape* shape)
	{
		switch (shape->GetColliderType())
		{
			case Physics::ColliderType::AABB_Collider:
			{	
				std::vector<Vector2D> points = static_cast<Physics::AABB*>(shape)->GetPoints();
				window->draw(DrawBox(points));
				break;
			}
			case  Physics::ColliderType::CIRCLE_Collider:
			{
				Physics::Circle* circle = static_cast<Physics::Circle*>(shape);
				DrawCircle(circle->GetCenter(), circle->GetRadius());
				return;
			}
			default:
			{	//complex case
				return;
			}
		}
	}
	
	void DrawForces(Physics::RigidBody2D& body)
	{
		window->draw(DrawVector(body.GetPosition(), body.GetForce()));
		window->draw(DrawVector(body.GetPosition(), body.GetLinearVelocity()));
	}

	void SetWindow(sf::RenderWindow& renderWindow)
	{
		window = &renderWindow;
	}

	//A little hack-y but clears anything that could be there one frame that shouldn't next frame
	void Refresh()
	{
		//iterate through the other debug drawings and make sure they get deleted
		for (std::vector<sf::VertexArray*>::iterator it = other_debug_drawings.begin(); it != other_debug_drawings.end(); ++it)
		{
			delete (*it);
		}
		other_debug_drawings.clear();
	}


protected:
	sf::VertexArray DrawBox(std::vector<Vector2D> vert_array)
	{
		sf::VertexArray vertex_array;
		for (sf::Vector2<float> sfml_vector : vert_array)
		{
			sf::Vertex vertex(sfml_vector, sf::Color::Red);
			vertex_array.append(vertex);
		}
		vertex_array.append(sf::Vertex(vert_array.at(0), sf::Color::Red));


		vertex_array.setPrimitiveType(sf::LineStrip);
		return vertex_array;
	}

	sf::VertexArray DrawVector(const Vector2D& origin, const Vector2D& end)
	{
		sf::VertexArray vertex_array;
		vertex_array.append(sf::Vertex(origin, sf::Color::Yellow));
		vertex_array.append(sf::Vertex(origin + end, sf::Color::Red));
		vertex_array.setPrimitiveType(sf::Lines);
		return vertex_array;
	}

	sf::VertexArray DrawLine(const Vector2D& origin, const Vector2D& end, sf::Color color = sf::Color::Green)
	{
		sf::VertexArray vertex_array;
		vertex_array.append(sf::Vertex(origin, color));
		vertex_array.append(sf::Vertex(end, color));
		vertex_array.setPrimitiveType(sf::Lines);
		return vertex_array;
	}

	void DrawPoint(Vector2D point, float target_radius , sf::Color color = sf::Color::Green)
	{
		target_radius *= 0.1f; //we only render the sphere at a fracton of its real size

		sf::CircleShape point_shape;
		point_shape.setRadius(target_radius);
		point_shape.setOrigin(Vector2D(target_radius, target_radius));
		point_shape.setPosition(point);
		point_shape.setOutlineColor(sf::Color::Black);
		point_shape.setOutlineThickness(2);
		color.a = 100;
		point_shape.setFillColor(color);
		window->draw(point_shape);
	}

	void DrawCircle(Vector2D point, float radius)
	{
		sf::CircleShape point_shape;
		point_shape.setRadius(radius);
		point_shape.setPosition(point);
		point_shape.setOutlineThickness(2);
		point_shape.setOutlineColor(sf::Color::Red);
		window->draw(point_shape);
	}

public:
	std::vector<sf::VertexArray*> other_debug_drawings;

private:
	sf::RenderWindow* window;
};

