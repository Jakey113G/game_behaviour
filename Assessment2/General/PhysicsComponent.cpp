#include "PhysicsComponent.h"
#include <Physics/PhysicsWorld.h>
#include "World.h"
#include <Physics/Shape.h>

PhysicsComponent::PhysicsComponent() : body(new Physics::RigidBody2D())
{

}

PhysicsComponent::PhysicsComponent(Physics::RigidBody2D* newBody): body(newBody)
{

}

PhysicsComponent::~PhysicsComponent()
{
	delete body;
}

void PhysicsComponent::Begin(float delta)
{
}

void PhysicsComponent::Update(float delta)
{
	//changes to the physics managed by this component is done here.

}

Physics::RigidBody2D& PhysicsComponent::GetRigidBody()
{
	return *body;
}