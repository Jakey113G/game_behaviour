#pragma once
#include <vector>

class CameraManager;

//forward declares
namespace sf
{
	class RenderWindow;
	class RenderStates;
	class Drawable;
	class Text;
}

class UI
{
public:
	UI(CameraManager* cam);

	sf::Text* CreateText();
	void AddText(sf::Drawable* drawable);
	void RemoveDraw(sf::Drawable* drawable);

	void DrawUI(sf::RenderWindow& window, const sf::RenderStates&);

public:
	std::vector<sf::Drawable*> ui_elements;
	CameraManager* camera_manager;

	//sf::Transform render_screen_offset;
};