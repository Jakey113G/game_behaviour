#include "FlyingEnemy.h"
#include <AI/AIWorld.h>

#include "AssetLibrary.h"
#include "EntityFactory.h"

#include "World.h"
#include "GameOperator.h"
#include "Player.h"
#include "TransformComponent.h"
#include "PhysicsComponent.h"
#include "RenderComponent.h"
#include <Physics/HitDataResult.h>

FlyingEnemyStateMachine FlyingEnemy::state_machine_for_ai;

FlyingEnemy::FlyingEnemy() : BaseAgent(), Enemy()
{
	transform_component = new TransformComponent();
	physics_component = new PhysicsComponent();

	sf::Sprite sprite = Factory::GetInstance()->MakeSprite(AssetLibrary::GetInstance()->GetTexture(TextureKeyEnum::Red_Bird), 120, 70);
	sprite.setOrigin(120 * 0.5f, 70 * 0.5f);
	this->render_component = new RenderComponent(sprite);

	Physics::RigidBody2D& body = physics_component->GetRigidBody();

	body.SetShapeCollider(new Physics::AABB(110.0f, 60.0f));
	body.SetCollisionType(Physics::BLOCKING_COLLISION);
	ai_decision_update_rate.SetTimer(0.5f);
	sight_range = 550.0f;
}

bool FlyingEnemy::EvaluateIfPossibleToReachPointFromNode()
{
	throw std::logic_error("The method or operation is not implemented.");
}

bool FlyingEnemy::PathForAIStillValid(AI::AStarPath&)
{
	throw std::logic_error("The method or operation is not implemented.");
}

void FlyingEnemy::BeginPlay()
{
	GameEntity::BeginPlay();
}

//detect events - this controls the logic the ai will choose to obey
//called from ai_world update.
void FlyingEnemy::UpdateAgent(AI::AIWorld* ai_world, float delta)
{
	ai_decision_update_rate.Update(delta);
	if (!ai_decision_update_rate.HasTimeElapsed())
	{
		return;
	}
	ai_decision_update_rate.Restart();

	//detect from ai world what nodes are around - evaluate what to do with state-machine as guidance
	//process attack condition
	bool enemy_sighted = false;
	Vector2D player_location;
	for (AI::Operator* e : GetWorld()->AIWorld().GetOperators())
	{
		if (GameOperator* game_operator = dynamic_cast<GameOperator*>(e))
		{
			if (Vector2D(game_operator->GetLocation() - this->transform_component->GetLocation()).Length() < sight_range)
			{
				//test no obstacle should have two collisions including both of ourselves
				if (GetWorld()->PhysicsWorld().RayCastWorld(this->transform_component->GetLocation(), game_operator->GetLocation()).size() == 2)
				{
					enemy_sighted = true;
					player_location = game_operator->GetLocation();
				}
			}
		}
	}
	
	if (enemy_sighted)
	{
		state_machine_for_ai.UpdateState(EnumFlyingEvents::EnemyInRange, state);
	}
	else
	{
		state_machine_for_ai.UpdateState(EnumFlyingEvents::EnemyNotInRange, state);
	}


	//process route found going to player.
	path_to_goal = ai_world->GetPathRoute(ai_world->GetWaypointClosestToPoint(GetTransformComponent()->GetLocation()), ai_world->GetWaypointClosestToPoint(player_location));
	if (path_to_goal.StillAdvancable())
	{
		state_machine_for_ai.UpdateState(EnumFlyingEvents::RouteDetected, state);
	}
	else
	{
		state_machine_for_ai.UpdateState(EnumFlyingEvents::NoRoute, state);
	}
}

//What the ai should actually be doing - called from game entity update.
void FlyingEnemy::Update(float delta)
{
	for (const Physics::HitDataResult& hit_data : GetWorld()->PhysicsWorld().HitRigidBodies(&physics_component->GetRigidBody()))
	{
		if (Player* p = dynamic_cast<Player*>(GetWorld()->FindGameEntityInWorld(hit_data.body)))
		{
#ifdef _DEBUG
			std::cout << "--KILLING PLAYER--" << std::endl;
#endif // _DEBUG
			
			p->Destroy();
			return;
		}
	}

	if (state == EnumFlyingState::Seek)
	{
		SeekAction(delta);
	}
	else if (state == EnumFlyingState::Attack)
	{
		AttackAction(delta);
	}
	else if (state == EnumFlyingState::Hover)
	{
		HoverAction(delta);
	}
}

void FlyingEnemy::HoverAction(float delta)
{
	Vector2D hover_velocity(0, -200);
	GetPhysicsComponent()->GetRigidBody().SetLinearVelocity(hover_velocity);
}

void FlyingEnemy::AttackAction(float delta)
{
	if (GetWorld()->AIWorld().GetOperators().empty())
	{
		//if no enemy lets just fly
		HoverAction(delta);
		return;
	}


	Vector2D displacement = GetWorld()->AIWorld().GetOperators().front()->GetLocation() - this->GetTransformComponent()->GetLocation();

	Vector2D velocity = displacement.Normal() * 200.0f;

	this->GetPhysicsComponent()->GetRigidBody().SetLinearVelocity(velocity);
}

void FlyingEnemy::SeekAction(float delta)
{
	//detect what to defend against and try to dodge it
	//follow path
	Vector2D direction(path_to_goal.GetCurrentNode()->GetLocation() - this->GetTransformComponent()->GetLocation());
	direction = direction.Normal();
	GetPhysicsComponent()->GetRigidBody().SetLinearVelocity(direction * 230.0f);

	if (path_to_goal.GetCurrentNode()->InAcceptableRangeOfWaypoint(Vector2D(GetTransformComponent()->GetLocation()), 75.0f))
	{
		path_to_goal.Advance();
	}
}

void FlyingEnemy::InitComponents()
{
	Enemy::InitComponents();

	//Register this in the ai world as an ai operator.
	m_world->AIWorld().AddAgent(this);
}

void FlyingEnemyStateMachine::UpdateState(EnumFlyingEvents event_value, EnumFlyingState& existing_state)
{	
	if (event_value == EnumFlyingEvents::NoRoute)
	{
		existing_state = EnumFlyingState::Hover;
	}
	
	if (event_value == EnumFlyingEvents::RouteDetected)
	{
		if (existing_state == EnumFlyingState::Hover)
		{
			existing_state = EnumFlyingState::Seek;
		}
	}

	if(event_value == EnumFlyingEvents::EnemyInRange)
	{
		existing_state = EnumFlyingState::Attack;
	}

	if (event_value == EnumFlyingEvents::EnemyNotInRange)
	{
		if (existing_state == EnumFlyingState::Attack)
		{
			existing_state = EnumFlyingState::Seek;
		}
	}
}
