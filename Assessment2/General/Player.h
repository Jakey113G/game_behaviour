#pragma once
#include "Character.h"

namespace sf
{
	class Sprite;
}

namespace Physics
{
	class RigidBody2D;
}

class GameOperator;
class Player : public Character
{
public:
	Player();
	Player(TransformComponent*, RenderComponent*, PhysicsComponent*);
	virtual ~Player();
	//Player(sf::Sprite& texture, Physics::RigidBody2D& physics);

	
	virtual void Update(float delta_time) override;

	virtual void BeginPlay() override;

	float speed;

	virtual void InitComponents() override;

private:
	GameOperator* ai_operator;
	void InputPolling(float delta_time);
	void TeleportShift(Vector2D param1);
	void ResetTeleport();
	bool can_telport;
};

