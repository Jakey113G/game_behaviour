#pragma once
#include "GameEntity.h"

class Coin : public GameEntity
{
public:
	Coin();
	~Coin();

	virtual void BeginPlay() override;


	virtual void InitComponents() override;


	virtual void Update(float delta_time) override;

};