#pragma once
#include <AI/BaseAgent.h>
#include <AI/Waypoint.h>
#include "Enemy.h"

#include "SyncroTimer.h"
#include <AI/FiniteStateMachine.h>

namespace AI { class AIWorld; }

//AI Events
//////////////////////////////////////////////////////////////////////////
enum class EnumPlatformCrawlingvents
{
	EnemySighted,
	LostEnemy,
	FarFromGuard,
	AtGuard
};

enum class EnumPlatformCrawlingState
{
	Patrol,
	ReturnToPatrol,
	Attack
};

//AI Statemachine
//////////////////////////////////////////////////////////////////////////
class PlatformCrawlingStateMachine : public AI::FiniteStateMachine
{
public:
	virtual void UpdateState(EnumPlatformCrawlingvents event_value, EnumPlatformCrawlingState& existing_state);

};

//Simple enemy who patrols between two set way point using path finding
//if an enemy is sighted they chase and attack the enemy.
//On losing the enemy or they stray too far from home
//they return to their home platform to patrol.
class PlatformCrawlingEntity : public AI::BaseAgent, public Enemy
{
public:

	PlatformCrawlingEntity();

	virtual void UpdateAgent(AI::AIWorld*, float) override;

	virtual void Update(float delta_time) override;

	virtual void BeginPlay() override;

	virtual void InitComponents() override;

	virtual void ProcessMovementPatrolling(float);

	void SetPointToProtect(Vector2D location);

	virtual bool EvaluateIfPossibleToReachPointFromNode() override;

	virtual bool PathForAIStillValid(AI::AStarPath&) override;

	void SetGuardLocation(Vector2D location);

	//bool CheckBehaviour(float delta);
	void CheckAIPath();

	void CalculatePathOfAttack(float delta);
	void CalculatePathOfPatrol(float delta);

private:
	static PlatformCrawlingStateMachine state_machine;
	EnumPlatformCrawlingState state;

	//used initially to gain performance
	SyncroTimer	ai_decision_update_rate; //Used for managing the updating our ai agent. --UpdateAgent
	//SyncroTimer ai_behaviour_check_rate; //used for evaluating the ai behaviour. --Update

	AI::Waypoint* point_to_protect;
	AI::Waypoint* point_to_attack;
	
	Vector2D patrol_direction;
	Vector2D player_direction_from_point;

	//Helpful for handling movement - (in-case path gets updated and we lose the most important data)
	AI::Waypoint* next_waypoint;
 	AI::Waypoint* current_waypoint;
	AI::Waypoint* previously_visited_waypoint;

	//AI::AStarPath ai_current_path;
	bool need_to_recalculate_path;

	void ProcessMovementAlongPath(float delta_time);
	float speed;
};