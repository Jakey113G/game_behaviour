#include "ButtonTrigger.h"

#include "World.h"
#include "TransformComponent.h"
#include "RenderComponent.h"
#include "PhysicsComponent.h"
#include "AsyncTimer.h"

#include "AssetLibrary.h"
#include "EntityFactory.h"

ButtonTrigger::ButtonTrigger()
{
	transform_component = new TransformComponent();
	physics_component = new PhysicsComponent();

	sf::Sprite sprite = Factory::GetInstance()->MakeSprite(AssetLibrary::GetInstance()->GetTexture(GreenButton), 70, 70);
	sprite.setOrigin(70 * 0.5f, 70 * 0.65f);
	sprite.scale(Vector2D(1.5,1.5));
	render_component = new RenderComponent(sprite);

	Physics::RigidBody2D& body = physics_component->GetRigidBody();
	body.SetShapeCollider(new Physics::AABB(70.0f, 70.0f));
	body.SetCollisionType(Physics::BodyCollisionType::BLOCKING_COLLISION);
	body.SetBodyMotionType(Physics::BodyMotionType::STATIC_MOTION);

	active = true;
	reenable_button_timer.SetTimer(3.0f);
}

void ButtonTrigger::BeginPlay()
{
	//create button
}

void ButtonTrigger::InitComponents()
{
	GameEntity::InitComponents();
}

void ButtonTrigger::Update(float delta_time)
{
	if (active)
	{
		//on collision disable button for so long
		const std::vector<Physics::HitDataResult>& collisions = GetWorld()->PhysicsWorld().HitRigidBodies(&this->GetPhysicsComponent()->GetRigidBody());
		auto it = std::find_if(collisions.begin(), collisions.end(), [](const Physics::HitDataResult& hit_data)-> bool { return hit_data.contact_normal.y == 1.0f; });
		if (it != collisions.end())
		{
			//toggle button
			ToggleButton();
		}
	}
	else
	{
		//the button is off we keep ticking
		reenable_button_timer.Update(delta_time);
		if (reenable_button_timer.HasTimeElapsed())
		{
			ToggleButton();
			reenable_button_timer.Restart();
		}
	}
}

void ButtonTrigger::AddFunctionToTrigger(std::function<void()> func)
{
	button_function_trigger.push_back(func);
}

void ButtonTrigger::ToggleButton()
{
	if (active)
	{
		this->GetPhysicsComponent()->GetRigidBody().SetCollisionType(Physics::IGNORE_COLLISION);
		GetWorld()->RemoveFromDraw(render_component);
		for (auto F : button_function_trigger)
		{
			F();
		}

		//AsyncTimer(3000, true, &ButtonTrigger::ToggleButton, this); //Async could be dangerous modifying the contents of a container. Changed to use Syncro timer.
	}
	else
	{
		this->GetPhysicsComponent()->GetRigidBody().SetCollisionType(Physics::BLOCKING_COLLISION);
		GetWorld()->AddToDraw(render_component);
	}

	active = !active;
}
