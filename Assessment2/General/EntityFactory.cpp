#include "EntityFactory.h"
#include "TransformComponent.h"
#include "RenderComponent.h"
#include "PhysicsComponent.h"
#include "Player.h"

sf::Sprite Factory::MakeSprite(const sf::Texture& texture, int size_x, int size_y)
{
	return sf::Sprite(texture, sf::IntRect(0, 0, size_x, size_y));
}

Player* Factory::MakePlayer(const sf::Texture& texture, float x, float y)
{
	sf::Sprite sprite(texture, sf::IntRect(0, 0, (int)x, (int)y));
	sprite.setOrigin(x * 0.5f, y * 0.5f);

	TransformComponent* trans = new TransformComponent();
	RenderComponent* render = new RenderComponent(sprite);

	Physics::BodyData bodydata;
	bodydata.position = trans->GetLocation();
	bodydata.mass = 10.f;
	bodydata.motion_type = Physics::BodyMotionType::DYNAMIC_MOTION;
	bodydata.frictionCoefficent = 4.0f;
	bodydata.collision_type = Physics::BodyCollisionType::BLOCKING_COLLISION;
	PhysicsComponent* phys = new PhysicsComponent(new Physics::RigidBody2D(bodydata, new Physics::AABB(x, y)));

	Player* p = new Player(trans, render, phys);
	return p;
}

Factory* Factory::GetInstance()
{
	if (!s_factory)
	{
		s_factory = new Factory();
	}
	return s_factory;
}

Factory::Factory()
{

}

Factory* Factory::s_factory;
