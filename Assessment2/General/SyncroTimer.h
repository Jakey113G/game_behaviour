#pragma once

//class a synchronous timer for triggering bound functions
//different from AsyncTimer all this class does is keeps tracks of delta times and invokes the function assigned to it
//very basic in nature

class SyncroTimer
{
public:
	SyncroTimer() 
	{
		reset_timer_on_completion = false;
	}

	void SetTimer(float seconds_time)
	{
		end_time_delta = seconds_time;
	}

	inline bool HasTimeElapsed()
	{
		bool condition = time_passed_delta >= end_time_delta;
		if (reset_timer_on_completion && condition)
		{
			Restart();
		}
		return condition;
	}

	inline void Update(float time_increment)
	{
		time_passed_delta += time_increment;
		
		//if(HasTimeElapsed())
		//{
			//invoke functions
			//if this class could stored delegate functions
			//--delegate functions here--
			
		//}
	}

	void SetRestartOnElapse(bool value)
	{
		reset_timer_on_completion = value;
	}

	void Restart()
	{

		time_passed_delta = 0;
	}

private:
	float time_passed_delta;
	float end_time_delta;
	bool reset_timer_on_completion;
};