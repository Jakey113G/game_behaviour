#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Transform.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <Physics/PhysicsWorld.h>
#include <AI/AIWorld.h>
#include <vector>

class GameMode;
class GameEntity;
class CameraManager;

namespace Physics { class RigidBody2D; class Shape; class CollisionPair; };
namespace AI { class WayPointWorld; };

#define PIXEL_UNITS_TO_METRE 32.0f

//Handles the world and level management
class World
{
	friend class Game;
public:
	World(Game*);

	void Load();

	void BeginPlay();
	void update(float frameTimeDuration);
	void PostPhysicsUpdate(float frame_delta);

	void AddToDestroyQueue(GameEntity* entity);
	void AddToSpawnQueue(GameEntity* entity);
	void RemoveGameEntity(GameEntity*);
	void SpawnGameEntity(GameEntity* entity);
	void ProcessLifetimeLists();

	void ProccessSpawnList();
	void ProcessDestroyList();

	inline void DrawWorld(sf::RenderWindow& window)
	{
		for (sf::Drawable* entitiy : world_drawing)
		{
			window.draw(*entitiy, world_state);
		}
	}

	inline Physics::PhysicsWorld& PhysicsWorld()
	{
		return physical_world;
	}

	inline AI::AIWorld& AIWorld()
	{
		return ai_world;
	}

	constexpr float PhysicalUnits() const
	{
		return PIXEL_UNITS_TO_METRE;
	}

	GameEntity* FindGameEntityInWorld(Physics::RigidBody2D* body);
	const std::vector<GameEntity*>& GetGameEntitiesInWorld();
	Vector2D GetWorldLocation(GameEntity*);

	void SpawnStaticEnvironment(); //todo
	void SpawnDynamicEnvironment(); //todo
	void CalculateStaticWorldRepresentation();
	void UpdateWorldRepresentation(); //todo

	sf::RenderWindow& GetWindow();

	bool IsInWorld(GameEntity* entity);

	//Physics and Ai have their own interface utility's - draw also should have interface for managing the drawing
	void RemoveFromDraw(sf::Drawable* render_component);
	void AddToDraw(sf::Drawable* render_component);

private:
	//different implementations I was experimenting with
	std::vector<AI::Waypoint> CreateSmoothingOfPoints(Physics::Shape&, Physics::CollisionPair& hit_point, AI::Waypoint& start_point, AI::Waypoint& end);
	std::vector<AI::Waypoint> CreateSmoothedPoint(Physics::CollisionPair& contact, AI::Waypoint& start_point, AI::Waypoint& end_point);

private:
	Game& m_game;
	std::vector<GameEntity*> world_entities;
	std::vector<sf::Drawable*> world_drawing;

	Physics::PhysicsWorld physical_world;
	AI::AIWorld ai_world;

	AI::WayPointWorld* ai_representation;

	sf::RenderStates world_state;
	float world_time_dilation;

	std::vector<GameEntity*> m_destroyedList;
	std::vector<GameEntity*> m_spawnList;
	void Register(GameEntity* entity);
public:
	Game& GetGame();
	GameMode& GetGameMode();
	CameraManager* GetCamera();
};