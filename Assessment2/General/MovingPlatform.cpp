#include "MovingPlatform.h"
#include "TransformComponent.h"
#include "PhysicsComponent.h"

#include "World.h"
#include <Physics/HitDataResult.h>
#include <Physics/RigidBody2D.h>

MovingPlatform::MovingPlatform(TransformComponent* transform, RenderComponent* draw, PhysicsComponent* body) : GameEntity(transform, draw, body)
{
	timer_for_motion.SetTimer(1.0f);
}

void MovingPlatform::BeginPlay()
{
	this->GetPhysicsComponent()->GetRigidBody().SetBodyMotionType(Physics::BodyMotionType::KINEMATIC_MOTION);
	//restart_position = this->GetTransformComponent()->GetLocation();

	movement_velocity = Vector2D(0, 250); //a default movement
}

void MovingPlatform::Update(float delta_time)
{
	Vector2D old_position = GetTransformComponent()->GetLocation();
	Vector2D new_position = old_position;
	new_position += movement_velocity * delta_time;
	
	SetPosition(new_position);
	MoveBodiesInContact(new_position - old_position);

	timer_for_motion.Update(delta_time);
	if (timer_for_motion.HasTimeElapsed())
	{
		//small bug here where the new position far exceeds max displacement causing a continuous back and forth flipping of the velocity as it becomes stuck
		//flipping velocity back and force.
		movement_velocity *= -1.0f;
		timer_for_motion.Restart();
	}
}

void MovingPlatform::MoveBodiesInContact(Vector2D delta_position)
{
	//Little hack so anything in contact so they move with the platform 
	for (const Physics::HitDataResult& body_contact : GetWorld()->PhysicsWorld().HitRigidBodies(&GetPhysicsComponent()->GetRigidBody()))
	{
		if (body_contact.body->GetColliderShape() != nullptr)
		{
			if (!body_contact.body->IsStatic()) //This way I can move move only objects I deem move-able. Kinematic or dynamic
			{
				if (body_contact.contact_normal.y == 1) //only objects on top can move with it
				{
					body_contact.body->SetPosition(body_contact.body->GetPosition() + delta_position);
				}
			}
		}
	}
}

void MovingPlatform::SetMovement(Vector2D displacement_to_use)
{
	movement_velocity = displacement_to_use;
}

void MovingPlatform::SetMotionTimer(float seconds)
{
	timer_for_motion.SetTimer(seconds);
}

//void MovingPlatform::SetMaxDisplacement(float value)
//{
//	max_displacement = value;
//}
