#pragma once

class Game;

//Base classes for game modes
class GameMode
{
public:
	GameMode(Game*);
	virtual ~GameMode();

	virtual void GameModeUpdate(float delta);
	virtual bool GameOver();

	//Api that the actual game can use to win or lose the game which the game mode won't detect directly
	virtual void SetGameWon();
	virtual void SetGameLost();

	void SetWinConditionScore(int param1);
	int GetWinConditionScore();
	void IncrementScore();
private:
	virtual bool WinConditionCheck();
	virtual bool LoseConditionCheck();

	virtual void OnLose();
	virtual void OnWin();

private:
	Game* game_in_play;

	int score;
	int win_condition_score;
	bool game_complete;
};