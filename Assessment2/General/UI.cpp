#include "UI.h"
#include "AssetLibrary.h"
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/Drawable.hpp>

UI::UI(CameraManager* cam)
{
	camera_manager = cam;

	/*
	sf::Font& font = AssetLibrary::GetInstance()->GetFont(FontKeyEnum::AARDV);
	sf::Text* time_per_frame = new sf::Text();
	time_per_frame->setString("Starting...");
	time_per_frame->setFont(font);
	time_per_frame->setCharacterSize(300);
	time_per_frame->setFillColor(sf::Color::Red);
	time_per_frame->setPosition(0, 0);
	ui_elements.push_back(time_per_frame);
	*/
}

sf::Text* UI::CreateText()
{
	sf::Text* text = new sf::Text();
	ui_elements.push_back(text);
	return text;
}

void UI::AddText(sf::Drawable* drawable)
{
	ui_elements.push_back(drawable);
}

void UI::RemoveDraw(sf::Drawable* drawable)
{
	auto it = std::find(ui_elements.begin(), ui_elements.end(), drawable);
	if (it != ui_elements.end())
	{
		ui_elements.erase(it);
	}
}

void UI::DrawUI(sf::RenderWindow& window, const sf::RenderStates& screen_transform)
{
	for (sf::Drawable* drawing : ui_elements)
	{
		window.draw(*drawing, screen_transform);
	}
}