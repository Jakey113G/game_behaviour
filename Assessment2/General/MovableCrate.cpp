#include "MovableCrate.h"

#include "TransformComponent.h"
#include "RenderComponent.h"
#include "PhysicsComponent.h"

#include <Physics/AABB.h>
#include "AssetLibrary.h"
#include "EntityFactory.h"

MovableCrate::MovableCrate()
{
	transform_component = new TransformComponent();
	physics_component = new PhysicsComponent();

	sf::Sprite sprite = Factory::GetInstance()->MakeSprite(AssetLibrary::GetInstance()->GetTexture(TextureKeyEnum::PlatformIndustrial_057), 70, 70);
	sprite.setOrigin(70 * 0.5f, 70 * 0.5f);
	render_component = new RenderComponent(sprite);

	Physics::RigidBody2D& body = physics_component->GetRigidBody();
	body.SetShapeCollider(new Physics::AABB(70.0f, 70.0f));
	body.SetCollisionType(Physics::BodyCollisionType::BLOCKING_COLLISION);
	body.SetBodyMotionType(Physics::BodyMotionType::DYNAMIC_MOTION);
}

void MovableCrate::BeginPlay()
{
	GameEntity::BeginPlay();
}

void MovableCrate::InitComponents()
{
	GameEntity::InitComponents();
}
