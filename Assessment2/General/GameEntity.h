#pragma once

class Vector2D;
class World;
class TransformComponent;
class RenderComponent;
class PhysicsComponent;

//Forward declare sf classes
namespace sf
{
	class Transform;
	class Transformable;
	class RenderTarget;
	class Drawable;
}

//Forward declare the physics classes
namespace Physics
{
	class Animator;
	class RigidBody2D;
}

//After much review I decided to go with a composition based approach for handling my GameObjects. 
//If you are interested to know more about this decision read chapter 15.2 of "Game Engine Architecture (2nd Edition)" by Jason Gregory 
class GameEntity
{
public:
	GameEntity();
	//GameEntity(sf::Transformable* transform, sf::Drawable* draw, Physics::RigidBody2D* body);
	GameEntity(TransformComponent* transform, RenderComponent* draw, PhysicsComponent* body);
	virtual ~GameEntity();

	void SetWorldOwnership(World* world);

	World* GetWorld();


	//////////////////////////////////////////////////////////////////////////

	TransformComponent* GetTransformComponent();
	RenderComponent* GetRenderComponent();
	PhysicsComponent* GetPhysicsComponent();

	//////////////////////////////////////////////////////////////////////////

	//First called before regular update cycle
	virtual void BeginPlay();
	virtual void InitComponents();

	virtual void Update(float delta_time);
	virtual void PostPhysicsUpdate(float delta_time);

	virtual void SetPosition(Vector2D& newPosition);
	void Destroy();

protected:
	//virtual void draw(sf::RenderTarget& target, const sf::Transform& transform) const;

protected:
	World* m_world; //The world that this entity belongs to.

	TransformComponent* transform_component;
	RenderComponent* render_component;
	PhysicsComponent* physics_component;

	//Physics::Animator*		m_animator;
};