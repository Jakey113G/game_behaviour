#include "SpringPad.h"
#include "TransformComponent.h"
#include "PhysicsComponent.h"
#include "RenderComponent.h"
#include "AssetLibrary.h"
#include "EntityFactory.h"
#include "World.h"
#include <Physics/SpringConstraint.h>

SpringPad::SpringPad(float spring_restlength)
{
	transform_component = new TransformComponent();
	physics_component = new PhysicsComponent();

	sf::Sprite sprite = Factory::GetInstance()->MakeSprite(AssetLibrary::GetInstance()->GetTexture(TextureKeyEnum::PlatformIndustrial_048), 600, 70);
	sprite.setOrigin(600 * 0.5f, 70 * 0.5f);
	render_component = new RenderComponent(sprite);

	Physics::RigidBody2D& body = physics_component->GetRigidBody();
	body.SetShapeCollider(new Physics::AABB(600.0f, 50.0f));
	body.SetBodyMotionType(Physics::DYNAMIC_MOTION);
	body.SetCollisionType(Physics::BLOCKING_COLLISION);

	static_spring_base = new Physics::RigidBody2D();
	static_spring_base->SetBodyMotionType(Physics::STATIC_MOTION);
	static_spring_base->SetCollisionType(Physics::IGNORE_COLLISION);
	static_spring_base->SetShapeCollider(new Physics::AABB(200.0f, 50.0f));
	spring_constraint = new Physics::SpringConstraint(static_spring_base, &body);
	spring_constraint->Stiffness = 50.0f;
	spring_constraint->Dampen = 0.8f;
	spring_constraint->RestLength = spring_restlength;
}

SpringPad::~SpringPad()
{
	GetWorld()->PhysicsWorld().RemovePhysicsConstraint(spring_constraint);
	delete spring_constraint;

	GetWorld()->PhysicsWorld().RemoveWorldEntity(static_spring_base);
	delete static_spring_base;
}

void SpringPad::BeginPlay()
{

}

void SpringPad::Update(float delta_time)
{
	GameEntity::Update(delta_time);

	//lock horizontal axis velocity and position value - really should be turned into physics constraints
	Physics::RigidBody2D& body = GetPhysicsComponent()->GetRigidBody();
	Vector2D position_of_body = body.GetPosition();
	Vector2D velocity_of_springpad = body.GetLinearVelocity();

	velocity_of_springpad.x = 0;
	position_of_body.x = static_spring_base->GetPosition().x;
	body.SetPosition(position_of_body);
	body.SetLinearVelocity(velocity_of_springpad);
}

void SpringPad::InitComponents()
{
	GameEntity::InitComponents();

	GetWorld()->PhysicsWorld().AddPhysicsEntity(static_spring_base);
	GetWorld()->PhysicsWorld().AddPhysicsConstraint(spring_constraint);
}

void SpringPad::SetPosition(Vector2D& newPosition)
{
	GameEntity::SetPosition(newPosition);

	//Additional spring handling to shift the base of the spring
	Vector2D base_spring_position = GetTransformComponent()->GetLocation();
	base_spring_position.y -= spring_constraint->RestLength;
	static_spring_base->SetPosition(base_spring_position);
}

void SpringPad::SetSpringStiffness(float value)
{
	spring_constraint->Stiffness = value;
}
