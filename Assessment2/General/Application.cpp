#include "Application.h"
#include "AssetLibrary.h"
#include "ErrorLog.h"
#include "Utility.h"
#include "World.h"

Application::Application() : window(sf::VideoMode(1280, 720), "Assessment2"), game(window)
{
	window.setFramerateLimit(120);
}

void Application::InstantiateOtherSystems()
{
	//Instantiate singletons
	AssetLibrary::GetInstance();
	ErrorLog::GetInstance();

}

void Application::LoadResource()
{
	sf::Font font_AARDV;
	if(!font_AARDV.loadFromFile("assets/fonts/AARDV.TTF"))
	{
		ErrorLog::Write("Unable to load font: AARDV");
	}
	
	sf::Texture platformIndustrial_048;
	if (!platformIndustrial_048.loadFromFile("assets/platformIndustrial_048.png"))
	{
		ErrorLog::Write("Unable to load Texture: platformIndustrial_048.png");
	}
	platformIndustrial_048.setRepeated(true);


	sf::Texture texture_platformIndustrial_057;
	if (!texture_platformIndustrial_057.loadFromFile("assets/platformIndustrial_057.png"))
	{
		ErrorLog::Write("Unable to load Texture: platformIndustrial_057.png");
	}
	texture_platformIndustrial_057.setSmooth(true);
	texture_platformIndustrial_057.setRepeated(true);
	
	sf::Texture texture_platformIndustrial_003;
	if (!texture_platformIndustrial_003.loadFromFile("assets/platformIndustrial_003.png"))
	{
		ErrorLog::Write("Unable to load Texture: platformIndustrial_003.png");
	}
	texture_platformIndustrial_003.setSmooth(true);
	texture_platformIndustrial_003.setRepeated(true);

	sf::Texture BluePortal;
	if (!BluePortal.loadFromFile("assets/portal.png"))
	{
		ErrorLog::Write("Unable to load Texture: blue_portal.png");
	}
	BluePortal.setSmooth(true);
	
	sf::Texture alienBlue_suit;
	if (!alienBlue_suit.loadFromFile("assets/alienBlue_suit.png"))
	{
		ErrorLog::Write("Unable to load Texture: alienBlue_suit.png");
	}
	alienBlue_suit.setSmooth(true);
	
	sf::Texture barnacle;
	if (!barnacle.loadFromFile("assets/barnacle.png"))
	{
		ErrorLog::Write("Unable to load Texture: barnacle.png");
	}
	barnacle.setSmooth(true);

	sf::Texture red_bird;
	if (!red_bird.loadFromFile("assets/red_bird.png"))
	{
		ErrorLog::Write("Unable to load Texture: red_bird.png");
	}
	red_bird.setSmooth(true);

	sf::Texture spike_horizontal;
	if (!spike_horizontal.loadFromFile("assets/platformIndustrial_052_left.png"))
	{
		ErrorLog::Write("Unable to load Texture: platformIndustrial_052_left.png");
	}
	spike_horizontal.setRepeated(true);

	sf::Texture spike_vertical;
	if (!spike_vertical.loadFromFile("assets/platformIndustrial_038_top.png"))
	{
		ErrorLog::Write("Unable to load Texture: platformIndustrial_038_top.png");
	}
	spike_vertical.setRepeated(true);

	sf::Texture green_button;
	if (!green_button.loadFromFile("assets/green_button.png"))
	{
		ErrorLog::Write("Unable to load Texture: green_button.png");
	}
	green_button.setSmooth(true);

	sf::Texture green_wall_block;
	if (!green_wall_block.loadFromFile("assets/green_wall_tile.png"))
	{
		ErrorLog::Write("Unable to load Texture: green_wall_tile.png");
	}
	green_wall_block.setRepeated(true);

	sf::Texture gold_coin;
	if (!gold_coin.loadFromFile("assets/coinGold.png"))
	{
		ErrorLog::Write("Unable to load Texture: coinGold.png");
	}
	gold_coin.setSmooth(true);

	AssetLibrary::GetInstance()->AddFont(FontKeyEnum::AARDV, font_AARDV);
	AssetLibrary::GetInstance()->AddTexture(TextureKeyEnum::PlatformIndustrial_048, platformIndustrial_048);
	AssetLibrary::GetInstance()->AddTexture(TextureKeyEnum::PlatformIndustrial_057, texture_platformIndustrial_057);
	AssetLibrary::GetInstance()->AddTexture(TextureKeyEnum::PlatformIndustrial_003, texture_platformIndustrial_003);
	AssetLibrary::GetInstance()->AddTexture(TextureKeyEnum::BluePortal, BluePortal);
	AssetLibrary::GetInstance()->AddTexture(TextureKeyEnum::AlienBlue_Suit, alienBlue_suit);
	AssetLibrary::GetInstance()->AddTexture(TextureKeyEnum::Barnacle, barnacle);
	AssetLibrary::GetInstance()->AddTexture(TextureKeyEnum::Red_Bird, red_bird);

	AssetLibrary::GetInstance()->AddTexture(TextureKeyEnum::VerticalSpike, spike_vertical);
	AssetLibrary::GetInstance()->AddTexture(TextureKeyEnum::HorizontalSpike, spike_horizontal);

	AssetLibrary::GetInstance()->AddTexture(TextureKeyEnum::GreenButton, green_button);
	AssetLibrary::GetInstance()->AddTexture(TextureKeyEnum::GreenWallBlock, green_wall_block);

	AssetLibrary::GetInstance()->AddTexture(TextureKeyEnum::GoldCoin, gold_coin);
}

void Application::StartRunTime()
{
	DefaultFrameRate();
}

int Application::RunTime()
{
	sf::Font& font = AssetLibrary::GetInstance()->GetFont(FontKeyEnum::AARDV);

#ifdef _DEBUG
	sf::Text time_per_frame;
	time_per_frame.setString("Starting...");
	time_per_frame.setFont(font);
	time_per_frame.setCharacterSize(30);
	time_per_frame.setPosition(-1580, -900);
	game.GetCameraManager()->GetUI().AddText(&time_per_frame);
#endif // _DEBUG

	game.BeginPlay();

	while (window.isOpen())
	{
		std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();

		//process events & input
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
			//else
			//Todo: hook up my input event dispatcher rather than use polling in update handling
			//	InputDispatch->ProcessInput(event);
		}

		//update logic
		game.update(m_deltaFrameFime.count());

		//render
		window.clear(sf::Color(0, 153, 255, 255));
		game.Render();
		window.display();
		
		std::chrono::duration<float> frameTimeDuration = std::chrono::high_resolution_clock::now() - start;
		m_deltaFrameFime = std::chrono::duration_cast<std::chrono::milliseconds>(frameTimeDuration);

		#ifdef _DEBUG
		//When debugging the game if a break point is triggered, the frame time can become unreasonably large. 
		//This restores it to a more appropriate value following a debug break point.
		if (m_deltaFrameFime.count() > 1.0f)
		{
			DefaultFrameRate();
		}
		#endif

#ifdef _DEBUG
		time_per_frame.setString("Time per Frame: " + Utility::ConvertFloatToString(m_deltaFrameFime.count()));
#endif // _DEBUG
	}

	return 0;
}

float Application::GetFrameRate()
{
	//possible formulas
	//sum of all frames / (time delta now - start);
	//or
	//quantity of frames from last second / second_ticks

	//	static unsigned int frame_count;

	return 0;
}

void Application::DefaultFrameRate()
{
	std::chrono::duration<float> overridingDeltaTime(1.0f / 30.0f);
	m_deltaFrameFime = std::chrono::duration_cast<std::chrono::milliseconds>(overridingDeltaTime);
}
