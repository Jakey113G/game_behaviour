#include "SpikeStrip.h"
#include "World.h"

#include "AssetLibrary.h"
#include "EntityFactory.h"
#include "RenderComponent.h"
#include "PhysicsComponent.h"

#include <Physics/HitDataResult.h>
#include <Physics/RigidBody2D.h>

SpikeStrip::SpikeStrip(SpikeDirection sd, float length_spikes)
{
	transform_component = new TransformComponent();
	render_component = new RenderComponent();

	physics_component = new PhysicsComponent();
	Physics::RigidBody2D& body = physics_component->GetRigidBody();

	sf::Sprite sprite;

	//Based on the enum create the sprite and apply the correct offsetting for position with bounding box.
	//May feel a bit hacky but it works. 35 is extends of sprite in other axis that isn't elongated for the spikestrip direction
	switch (sd)
	{
	case UP:
	{
		sf::Texture& t = AssetLibrary::GetInstance()->GetTexture(TextureKeyEnum::VerticalSpike);
		sprite = Factory::GetInstance()->MakeSprite(t, (int)length_spikes, 35);
		sprite.setOrigin(length_spikes * 0.5f, 17.5f);
		body.SetShapeCollider(new Physics::AABB(length_spikes, 35.0f));
		break;
	}
	case DOWN:
	{
		sf::Texture& t = AssetLibrary::GetInstance()->GetTexture(TextureKeyEnum::VerticalSpike);
		sprite = Factory::GetInstance()->MakeSprite(t, (int)length_spikes, 35);
		sprite.setOrigin(length_spikes * 0.5f, 17.5f);
		sprite.setScale(1, -1);
		body.SetShapeCollider(new Physics::AABB(length_spikes, 35.0f));
		break;
	}
	case LEFT:
	{
		sf::Texture& t = AssetLibrary::GetInstance()->GetTexture(TextureKeyEnum::HorizontalSpike);
		sprite = Factory::GetInstance()->MakeSprite(t, 35, (int)length_spikes);
		sprite.setOrigin(17.5f, length_spikes * 0.5f);
		body.SetShapeCollider(new Physics::AABB(35.0f, length_spikes));
		break;
	}
	case RIGHT:
	{
		sf::Texture& t = AssetLibrary::GetInstance()->GetTexture(TextureKeyEnum::HorizontalSpike);
		sprite = Factory::GetInstance()->MakeSprite(t, 35, (int)length_spikes);
		sprite.setOrigin(17.5f, length_spikes * 0.5f);
		sprite.setScale(-1.0f, 1);
		body.SetShapeCollider(new Physics::AABB(35.0f, length_spikes));
		break;
	}
	default:
		return;
	}
	render_component = new RenderComponent(sprite);

	body.SetCollisionType(Physics::BLOCKING_COLLISION);
	body.SetBodyMotionType(Physics::BodyMotionType::KINEMATIC_MOTION);
}

void SpikeStrip::Update(float delta_time)
{

}

void SpikeStrip::PostPhysicsUpdate(float delta_time)
{
	GameEntity::PostPhysicsUpdate(delta_time);

	for (const Physics::HitDataResult& hit_results : GetWorld()->PhysicsWorld().HitRigidBodies(&GetPhysicsComponent()->GetRigidBody()))
	{
		if (hit_results.body->IsDynamic())
		{
			GameEntity* entity = GetWorld()->FindGameEntityInWorld(hit_results.body);
			if (entity)
			{
				entity->Destroy();
			}
		}
	}
}
