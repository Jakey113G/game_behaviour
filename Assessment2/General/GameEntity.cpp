#include "GameEntity.h"

#include <SFML/Graphics/Transform.hpp>
#include <SFML/Graphics/Drawable.hpp>

#include "TransformComponent.h"
#include "PhysicsComponent.h"
#include "RenderComponent.h"

#include <Physics/RigidBody2D.h>
#include <Physics/Vector2D.h>
#include "World.h"

GameEntity::GameEntity() //Default init all pointers to nullptr
{

}

GameEntity::GameEntity(TransformComponent* transform, RenderComponent* draw, PhysicsComponent* body) : transform_component(transform), render_component(draw), physics_component(body)
{

}

//Provide functionality to set member components
//GameEntity::GameEntity(sf::Transformable* transform, sf::Drawable* draw, Physics::RigidBody2D* body ) : 
//	m_transform(transform), m_drawable(draw), m_rigidbody(body)
//{

//}

GameEntity::~GameEntity()
{
	//delete components

	delete transform_component;
	delete render_component;
	delete physics_component;
}

void GameEntity::SetWorldOwnership(World* world)
{
	m_world = world;
}

World* GameEntity::GetWorld()
{
	return m_world;
}

TransformComponent* GameEntity::GetTransformComponent()
{
	return transform_component;
}

RenderComponent* GameEntity::GetRenderComponent()
{
	return render_component;
}

PhysicsComponent* GameEntity::GetPhysicsComponent()
{
	return physics_component;
}

void GameEntity::BeginPlay()
{

}

void GameEntity::Update(float delta_time)
{
	transform_component->Update(delta_time); //Update transform - (honestly there is unlikely for their to be anything happening in the transform)
	
	physics_component->Update(delta_time); //update the physics for this entity.

	render_component->Update(delta_time); //Updates transforms that are used in rendering
}

void GameEntity::PostPhysicsUpdate(float delta_time)
{
	if (physics_component)
	{
		transform_component->setPosition(physics_component->GetRigidBody().GetPosition());
	}
}

//Update the position of entity components
void GameEntity::SetPosition(Vector2D& newPosition)
{
	transform_component->setPosition(newPosition);
	if(physics_component)
		physics_component->GetRigidBody().SetPosition(newPosition);
}

void GameEntity::Destroy()
{
	m_world->AddToDestroyQueue(this);
}

void GameEntity::InitComponents()
{
	if (transform_component)
	{
		transform_component->SetOwner(this);
	}
	if (physics_component)
	{
		physics_component->SetOwner(this);
	}
	if (render_component)
	{
		render_component->SetOwner(this);
	}
}

//remove
/*
void GameEntity::draw(sf::RenderTarget& target, const sf::Transform& transform) const
{
	sf::Vector2f position_to_screen_transform = m_transform->getPosition();
	position_to_screen_transform *= 32.0f;

	const_cast<sf::Transform&>(transform).translate(position_to_screen_transform);
	target.draw(*m_drawable, transform);
}
*/