#pragma once
#include <sstream>

//purely utility functions - 
class Utility
{
public:
	static std::string ConvertFloatToString(float number)
	{
		std::ostringstream buff;
		buff << number;
		return buff.str();
	}
};