#include "CameraManager.h"
#include "GameEntity.h"
#include <Physics/Vector2D.h>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Window/Keyboard.hpp>

#include "TransformComponent.h"

CameraManager::CameraManager(sf::RenderWindow& win) : window(win), view(Vector2D(0,300), Vector2D(3200,1800)), user_interface(this)
{
	//change zoom here
	window.setView(view);
	camera_move_boundary = 100.0f;
}

void CameraManager::Update(float deltaFrame)
{
	if (target)
	{
		Vector2D target_position = target->GetTransformComponent()->GetLocation();

		//replaced camera logic to always be centered on player
		view.setCenter(target_position);

		/*
		Vector2D offset_camera_move = Vector2D();
		Vector2D extend_max = view.getCenter() + view.getSize() * 0.5f;
		Vector2D extend_min = view.getCenter() - view.getSize() * 0.5f;
		extend_max.x -= camera_move_boundary;
		extend_max.y -= camera_move_boundary;
		extend_min.x += camera_move_boundary;
		extend_min.y += camera_move_boundary;

		//offset_camera_move.x = 100.0f; //test
		//more than x boundary
		if (target_position.x > extend_max.x)
		{
			offset_camera_move.x = 90.0f;
		}
		else if (target_position.x < extend_min.x)
		{
			offset_camera_move.x = -90.0f;
		}

		if (target_position.y > extend_max.y)
		{
			offset_camera_move.y = 90.0f;
		}
		else if (target_position.y < extend_min.y)
		{
			offset_camera_move.y = -90.0f;
		}
		*/
		//view.move(offset_camera_move * deltaFrame);
		window.setView(view); //reapply the view to the window
	}
	else
	{
		MoveWithInput(deltaFrame);
	}
}

void CameraManager::SetTarget(GameEntity* focus)
{
	target = focus;
}

sf::View& CameraManager::GetView()
{
	return view;
}

Vector2D CameraManager::GetPosition()
{
	//Should be screen center
	return view.getCenter();
}

UI& CameraManager::GetUI()
{
	return user_interface;
}

//Mostly for debug if no camera target added default to this
void CameraManager::MoveWithInput(float deltaFrame)
{
	Vector2D current_position = view.getCenter();
	float speed = 500.0f;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		current_position += Vector2D(-1, 0) * speed * deltaFrame;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		current_position += Vector2D(1, 0) * speed * deltaFrame;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		current_position += Vector2D(0, -1) * speed * deltaFrame;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		current_position += Vector2D(0, 1) * speed * deltaFrame;
	}
	view.setCenter(current_position);
	window.setView(view);
}

void CameraManager::DrawView(sf::RenderWindow& window)
{
	sf::RenderStates ui_transfrom;
	ui_transfrom.transform.translate(view.getCenter());
	user_interface.DrawUI(window, ui_transfrom);
}
