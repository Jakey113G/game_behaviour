#include "Application.h"

int main(int argc, char* argv[]) 
{
	Application* app = new Application();
	app->InstantiateOtherSystems();
	app->LoadResource();
	app->StartRunTime();
	app->RunTime();

	return 0;
}