#include "ErrorLog.h"
#include <string>

ErrorLog* ErrorLog::s_ErrorLog;

ErrorLog::ErrorLog()
{
	error_stream.open("ErrorLog.txt", std::fstream::out);
}

void ErrorLog::WriteStringToStream(std::string& s)
{
	error_stream << s << std::endl;
}

//Is short hand for calling instance and then function
void ErrorLog::Write(std::string s)
{
	GetInstance()->WriteStringToStream(s);
}

ErrorLog* ErrorLog::GetInstance()
{
	if (s_ErrorLog == nullptr)
	{
		s_ErrorLog = new ErrorLog();
	}
	return s_ErrorLog;
}
