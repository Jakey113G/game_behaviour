#include "Portal.h"
#include "World.h"

#include "AssetLibrary.h"
#include "EntityFactory.h"

#include "TransformComponent.h"
#include "PhysicsComponent.h"
#include "RenderComponent.h"

#include <Physics/AABB.h>
#include <Physics/RigidBody2D.h>

#include <algorithm>

Portal::Portal()
{
	transform_component = new TransformComponent();
	physics_component = new PhysicsComponent();

	sf::Sprite sprite = Factory::GetInstance()->MakeSprite(AssetLibrary::GetInstance()->GetTexture(BluePortal), 90, 90);
	sprite.setOrigin(90 * 0.5f, 90 * 0.5f);
	render_component = new RenderComponent(sprite);
	Physics::RigidBody2D& body = physics_component->GetRigidBody();

	body.SetShapeCollider(new Physics::AABB(85.0f, 85.0f));
	body.SetCollisionType(Physics::BodyCollisionType::OVERLAP_COLLISION);
	body.SetBodyMotionType(Physics::BodyMotionType::STATIC_MOTION);
}

Portal::~Portal()
{

}

void Portal::OnOverlapEnter(Physics::RigidBody2D* body)
{
	//DONT MOVE Non-dynamic bodies
	if (!body->IsDynamic())
	{
		return;
	}

	//Overlap depth - difference in positions
	if (Vector2D(body->GetPosition() - physics_component->GetRigidBody().GetPosition()).Length() < 25.0f)
	{
		GameEntity* world_object = m_world->FindGameEntityInWorld(body);
		if (world_object)
		{
			world_object->SetPosition(Vector2D(0, 0));
		}
	}
}

void Portal::BeginPlay()
{
	if (teleport_location == Vector2D(0, 0)) //unset teleport location
	{
		if (tethered_portal)
		{
			teleport_location = Vector2D(tethered_portal->GetTransformComponent()->GetLocation());
		}
	}

	deletion_timer.SetTimer(5); //5 seconds life.

	//Event system - replaced with polling (update function) for now 	
	//this->m_world->PhysicsWorld().BindFunctionToBody(&physics_component->GetRigidBody(), Physics::EPhysicsEvent::OVERLAP_ENTER_EVENT)
}

void Portal::Update(float delta_time)
{
	deletion_timer.Update(delta_time);
	if (deletion_timer.HasTimeElapsed())
	{
		this->Destroy();
		return;
	}


	const std::vector<Physics::HitDataResult>& overlapping_bodies = m_world->PhysicsWorld().OverlappingRigidBodies(&physics_component->GetRigidBody());
	for (const Physics::HitDataResult& body_over : overlapping_bodies)
	{
		//DONT teleport Non-dynamic bodies
		if (!body_over.body->IsDynamic())
		{
			continue;
		}

		GameEntity* entity_of_overlapping_body = GetWorld()->FindGameEntityInWorld(body_over.body);
		if (entity_of_overlapping_body != nullptr)
		{
			//if body doesn't already exist in the other teleport list
			if (!tethered_portal->EntityTeleported(entity_of_overlapping_body))
			{
				Teleport_Entity(entity_of_overlapping_body);
			}
		}
	}

	//clean up function should only be done by world really - if time create a world update delgate system so functions can be called by world.
	//or something that owns both portals and handles the updates

	//process teleporter list for any overlaps of the two teleport portals
	CleanUpTeleportList();
}


void Portal::Link(Portal* portal)
{
	tethered_portal = portal;
}

std::vector<GameEntity*>& Portal::TeleportedBodies()
{
	return entities_teleported;
}

bool Portal::EntityTeleported(GameEntity* entity)
{
	for (GameEntity* teleported : entities_teleported)
	{
		if (entity == teleported)
			return true;
	}

	for (GameEntity* teleported : tethered_portal->TeleportedBodies())
	{
		if (entity == teleported)
			return true;
	}

	return false;
}

void Portal::RemoveFromTeleportList(GameEntity* entity)
{
	auto it = std::find(entities_teleported.begin(), entities_teleported.end(), entity);
	if (it != entities_teleported.end())
	{
		entities_teleported.erase(it);
	}
}

void Portal::Teleport_Entity(GameEntity* entity)
{
	//Overlap depth - difference in positions
	Vector2D displacement(entity->GetTransformComponent()->GetLocation() - GetTransformComponent()->GetLocation());
	if (displacement.Length() < 50.0f)
	{
		entity->SetPosition(teleport_location);
		//std::cout << "Teleport to: " << std::endl;

		AddToRecentTeleportList(entity);
	}
}

//std::vector<GameEntity*> Portal::entities_teleported;

void Portal::CleanUpTeleportList()
{
	const std::vector<Physics::HitDataResult>& teleporterA = m_world->PhysicsWorld().OverlappingRigidBodies(&physics_component->GetRigidBody());
	const std::vector<Physics::HitDataResult>& teleporterB = m_world->PhysicsWorld().OverlappingRigidBodies(&tethered_portal->GetPhysicsComponent()->GetRigidBody());

	std::vector<GameEntity*> entities_to_remove;
	for (GameEntity* entity : entities_teleported)
	{
		if (!GetWorld()->IsInWorld(entity))
		{
			entities_to_remove.push_back(entity);
			continue;
		}

		bool entity_still_in_portal = false;
		Physics::RigidBody2D* entity_physics_body = &entity->GetPhysicsComponent()->GetRigidBody();
		
		//Check portal A for the teleported entity
		//predicate I made to use with a std::find_if, used to circumvent const issue initially ->  [entity_physics_body](Physics::HitDataResult i)->bool {return i == entity_physics_body; }
		if (std::find(teleporterA.begin(), teleporterA.end(), entity_physics_body) != teleporterA.end())
		{
			entity_still_in_portal = true;
		}

		//Check portal B for the teleported entity
		if (std::find(teleporterB.begin(), teleporterB.end(), entity_physics_body) != teleporterB.end())
		{
			entity_still_in_portal = true;
		}
		
		if (entity_still_in_portal == false)
		{
			entities_to_remove.push_back(entity);
			continue;
		}
	}

	for (GameEntity* entity : entities_to_remove)
	{
		RemoveFromTeleportList(entity);
	}
}

void Portal::AddToRecentTeleportList(GameEntity* entity)
{
	entities_teleported.push_back(entity);
}

