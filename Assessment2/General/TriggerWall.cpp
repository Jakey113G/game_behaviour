#include "TriggerWall.h"

#include "TransformComponent.h"
#include "PhysicsComponent.h"
#include "RenderComponent.h"

#include "World.h"

#include "AssetLibrary.h"
#include "EntityFactory.h"


//TODO: Fix the scaling on the image so it appears correctly in game

TriggerWall::TriggerWall(float x, float y)
{
	transform_component = new TransformComponent();
	physics_component = new PhysicsComponent();

	sf::Sprite sprite = Factory::GetInstance()->MakeSprite(AssetLibrary::GetInstance()->GetTexture(TextureKeyEnum::GreenWallBlock), (int)x, (int)y);
	sprite.setOrigin(x * 0.5f, y * 0.5f);
	render_component = new RenderComponent(sprite);

	Physics::RigidBody2D& body = physics_component->GetRigidBody();
	body.SetShapeCollider(new Physics::AABB(x, y));
	body.SetCollisionType(Physics::BodyCollisionType::IGNORE_COLLISION);
	body.SetBodyMotionType(Physics::BodyMotionType::STATIC_MOTION);

	active = true;
}

void TriggerWall::ToggleWall()
{
	if (active)
	{
		this->GetPhysicsComponent()->GetRigidBody().SetCollisionType(Physics::IGNORE_COLLISION);
		GetWorld()->RemoveFromDraw(render_component);		
	}
	else
	{
		this->GetPhysicsComponent()->GetRigidBody().SetCollisionType(Physics::BLOCKING_COLLISION);
		GetWorld()->AddToDraw(render_component);
	}

	active = !active;
}

void TriggerWall::BeginPlay()
{
	GameEntity::BeginPlay();
	GetPhysicsComponent()->GetRigidBody().SetCollisionType(Physics::BodyCollisionType::BLOCKING_COLLISION); //this is done like this due to a way point placed into the world
}
