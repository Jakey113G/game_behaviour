#pragma once
#include <AI/Operator.h>
#include <Physics/Vector2D.h>

class GameEntity;

//Need some way of converting my game objects into AI objects. The solution was this.
//Physics is difference because my objects have to have a physics object.
class GameOperator : public AI::Operator
{
public:
	GameOperator(GameEntity& obj);
	GameOperator(GameEntity& obj, int value);

	void SetValue(int value);

	virtual Vector2D GetLocation() override;
	GameEntity* WorldEntity();

private:
	GameEntity& entity;
};