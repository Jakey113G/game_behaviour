#pragma once
#include "GameEntity.h"

//abstract base class (non-constructible except by derived classes) that holds some generic information to be implemented by all components. 
class BaseComponent
{
public:
	virtual ~BaseComponent();

	inline GameEntity* GetOwner()
	{
		return owner;
	}

	void SetOwner(GameEntity*);

	virtual void Begin(float delta) = 0;
	virtual void Update(float delta) = 0;
	
protected:
	BaseComponent();
	
	BaseComponent(GameEntity* entity);

	GameEntity* owner;
};