#pragma once
#include "Enemy.h"
#include <AI/State.h>
#include <AI/AIWorld.h>
#include <AI/BaseAgent.h>
#include <AI/FiniteStateMachine.h>
#include "SyncroTimer.h"

//AI Events
//////////////////////////////////////////////////////////////////////////
enum class EnumFlyingEvents
{
	NoRoute,
	RouteDetected,
	EnemyInRange,
	EnemyNotInRange
};

enum class EnumFlyingState
{
	Hover,
	Seek,
	Attack
};

//AI States
//////////////////////////////////////////////////////////////////////////
//No easy way to expose game entity controlling logic to the states
/*
class Patrol : public AI::State
{
	virtual void Update(float delta) override;
	virtual void Action() override;
};

class Attack : public AI::State
{
	virtual void Update(float delta) override;
	virtual void Action() override;
};
*/

//AI Statemachine
//////////////////////////////////////////////////////////////////////////
class FlyingEnemyStateMachine : public AI::FiniteStateMachine
{
public:
	virtual void UpdateState(EnumFlyingEvents event_value, EnumFlyingState& existing_state);

};


//Actual AI Enemy
//////////////////////////////////////////////////////////////////////////
class AIWorld;
class GameOperator;
class FlyingEnemy : public AI::BaseAgent, public Enemy
{
public:
	FlyingEnemy();


	virtual bool EvaluateIfPossibleToReachPointFromNode() override;

	virtual void UpdateAgent(AI::AIWorld* ai_world, float delta) override;

	virtual bool PathForAIStillValid(AI::AStarPath&) override;
	
	virtual void BeginPlay() override;

private:
	static FlyingEnemyStateMachine state_machine_for_ai;
	EnumFlyingState state;
	SyncroTimer	ai_decision_update_rate;

	float sight_range;

	virtual void Update(float delta) override;

	void HoverAction(float delta);
	void AttackAction(float delta);
	void SeekAction(float delta);

	virtual void InitComponents() override;
};