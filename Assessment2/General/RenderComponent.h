#pragma once

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transform.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include "BaseComponent.h"
#include "TransformComponent.h"

namespace sf
{
	class RenderTarget;
	class Transform;
}

//class that is used to handle our rendering
class RenderComponent : public BaseComponent, public sf::Drawable
{
public:
	RenderComponent();
	RenderComponent(sf::Sprite);
	virtual ~RenderComponent();

	virtual void Begin(float delta) override;
	virtual void Update(float delta) override;

protected:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	
	inline void onDraw(sf::RenderTarget& target, sf::Transform& transform) const
	{
		target.draw(sprite, transform);
	}

	inline const sf::Transform& GetTransform() const
	{
		return owner->GetTransformComponent()->GetTransform();
	}

protected:
	inline sf::Transform ScaleTransformPosition(sf::Transform transform, float x, float y)
	{
		//translate to the new position, subtract the original position. To be world positioned
		float positionX = transform.getMatrix()[12];
		float positionY = transform.getMatrix()[13];
		float translatex = positionX * x;
		float translatey = positionY * y;
		return transform.translate(translatex - positionX, translatey - positionY);
	}

private:
	//sf::Transform& transform_ref;
	sf::Transformable local_transform;
	sf::Sprite sprite;
};