#pragma once
#include "BaseComponent.h"

//The purpose of this component is to handle animation of our game objects.
//currently bare and not needed
class AnimationComponent : public BaseComponent
{
public:
	AnimationComponent();

	virtual void Begin(float delta) override;
	virtual void Update(float delta) override;

};