#pragma once
#include <vector>
#include <string.h>
#include <fstream>

//dump-data to a log 
class DebugLog
{
public:
	DebugLog();
	
	void PushToLog(std::string s);

	std::string path;
	std::vector<std::string> lines;
};