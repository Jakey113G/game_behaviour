#pragma once

struct FrameRate
{
	static int frames = 0;
	static double starttime = 0;
	static float fps = 0.0f;

public:
	void First(double timepassed)
	{
		frames = 0;
		starttime = timepassed;
	}

	inline UpdateFrameTracker()
	{
		frames++;
	}

	inline void CalculateFrameRate(float& current_time)
	{
		if (current_time - starttime > 0.25 && frames > 10)
		{
			fps = (double)frames / (current_time - starttime);
			starttime = current_time;
			frames = 0;
		}
	}

	inline float GetFps()
	{
		return fps;
	}

};