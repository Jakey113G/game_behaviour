#pragma once
#include <unordered_map>
#include <string>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Font.hpp>

//This class is purely dedicated to storing and retrieval of assets in memory for usage within this framework.
//Enums are hard coded for the corresponding assets as it is easier than using strings.

//Game elements are stored within a world dictionary - not this class

enum TextureKeyEnum
{
	NullTexture,
	PlatformIndustrial_048,
	PlatformIndustrial_057,
	PlatformIndustrial_003,
	AlienBlue_Suit,
	Barnacle,
	BluePortal,
	Red_Bird,
	VerticalSpike,
	HorizontalSpike,
	GreenButton,
	GreenWallBlock,
	GoldCoin
};

enum FontKeyEnum
{
	AARDV
};

class AssetLibrary
{
	AssetLibrary();

public:
	inline void AddFont(FontKeyEnum key, sf::Font font)
	{
		font_collection[key] = font;
	}

	inline sf::Font& GetFont(FontKeyEnum key)
	{
		return font_collection[key];
	}

	inline void AddTexture(TextureKeyEnum key, sf::Texture texture)
	{
		texture_collection[key] = texture;
	}

	inline sf::Texture& GetTexture(TextureKeyEnum key)
	{
		return texture_collection[key];
	}

	static AssetLibrary* GetInstance();
private:
	static AssetLibrary* s_AssetLibrarySingleton;
	std::unordered_map<TextureKeyEnum, sf::Texture> texture_collection;
	std::unordered_map<FontKeyEnum, sf::Font> font_collection;
};