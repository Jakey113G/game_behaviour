#pragma once
#include <iostream>
#include <fstream>

class ErrorLog
{
private:
	static ErrorLog* s_ErrorLog;
	std::ofstream error_stream;
	ErrorLog();
	
	void WriteStringToStream(std::string& s);
public:
	static void Write(std::string s);
	
	static ErrorLog* GetInstance();
 };

