#pragma once
#include "GameEntity.h"

namespace Physics { class SpringConstraint;  }

class SpringPad : public GameEntity
{
public:
	SpringPad(float);
	virtual ~SpringPad();

	virtual void BeginPlay() override;
	virtual void Update(float delta_time) override;

	virtual void InitComponents() override;

	virtual void SetPosition(Vector2D& newPosition) override;

	void SetSpringStiffness(float value);
private:
	Physics::SpringConstraint* spring_constraint;
	Physics::RigidBody2D* static_spring_base;
	//sf::Sprite* spring_base_sprite;
};