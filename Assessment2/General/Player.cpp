#include "Player.h"
#include "AssetLibrary.h"
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Joystick.hpp>
#include "CameraManager.h"

//Physics & AI stuff
#include "PhysicsComponent.h"
#include <Physics/RigidBody2D.h>
#include <Physics/AABB.h>
#include "GameOperator.h" //<-- derived from AI class featured in game

#include "World.h"
#include "GameMode.h"
#include "TransformComponent.h"
#include "AsyncTimer.h"
#include "Portal.h"


Player::Player()
{
	speed = 350;
	can_telport = true;
}

Player::Player(TransformComponent* t, RenderComponent* r, PhysicsComponent* p) : Character(t,r,p)
{
	speed = 350;
	can_telport = true;
}

Player::~Player()
{
	GetWorld()->GetCamera()->SetTarget(nullptr);

#ifdef _DEBUG
	std::cout << "Player being deleted!" << std::endl;
#endif // _DEBUG	
	m_world->AIWorld().RemoveOperatorFromAIWorld(ai_operator);
	delete ai_operator;

	GetWorld()->GetGameMode().SetGameLost();
}

/*
Player::Player(sf::Sprite& sprite, Physics::RigidBody2D& physics) : Character()
{
	//m_drawable = &sprite;
	//m_transform = &sprite;
	//m_rigidbody = &physics;
}
*/

void Player::Update(float delta_time)
{
	GameEntity::Update(delta_time);

#ifdef _DEBUG
	//some debug stuff
	if (!GetWorld()->PhysicsWorld().HitRigidBodies(&GetPhysicsComponent()->GetRigidBody()).empty())
	{
		//std::cout << "HIT EVENT" << std::endl;
	}
	if (!GetWorld()->PhysicsWorld().OverlappingRigidBodies(&GetPhysicsComponent()->GetRigidBody()).empty())
	{
		//std::cout << "Overlap EVENT" << std::endl;
	}
	//std::cout << "Position x: " << this->GetTransformComponent()->GetLocation().x << "Position Y: " << this->GetTransformComponent()->GetLocation().y << std::endl;
#endif // _DEBUG

	InputPolling(delta_time);
}

void Player::BeginPlay()
{
		
}

void Player::InitComponents()
{
	Character::InitComponents();

	ai_operator = new GameOperator(*this);
	m_world->AIWorld().AddOperatorToWorld(ai_operator);
}

void Player::InputPolling(float delta_time)
{
	bool input_detected = false;
	//keep existing velo allow horizontal instantaneous changes 
	Vector2D overriding_velocity = GetPhysicsComponent()->GetRigidBody().GetLinearVelocity();

	//Poll based movement
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		//movement hack
		input_detected = true;
		overriding_velocity.x = speed;
	}
	//Poll based movement
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		//movement hack
		input_detected = true;
		overriding_velocity.x = -speed;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		const std::vector<Physics::HitDataResult>& contacts = this->GetWorld()->PhysicsWorld().HitRigidBodies(&this->GetPhysicsComponent()->GetRigidBody());
		if (!contacts.empty())
		{
			//check if any of the contacts are beneath us for the player to jump off.
			bool can_jump = false;
			for (const Physics::HitDataResult& hit : contacts)
			{
				if (hit.contact_normal.y < 0)
				{
					can_jump = true;
					break;
				}
			}

			//check to see if we are on top of anything we have collided with
			if (can_jump)
			{
				input_detected = true;
				this->GetPhysicsComponent()->GetRigidBody().AddForce(Vector2D(0, -1000.0f));
				overriding_velocity.y = -300.0f;
			}
		}
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
	{
		Vector2D mouse_position; 
		mouse_position.x = (float)sf::Mouse::getPosition(GetWorld()->GetWindow()).x;
		mouse_position.y = (float)sf::Mouse::getPosition(GetWorld()->GetWindow()).y;

		//Window position uses top-left coordinate system. Everything I have been doing uses center
		//Use window size to calculate the center point, so I have the mouse position relative to center.
		auto window_size = GetWorld()->GetWindow().getSize();
		Vector2D window_center = Vector2D((float)window_size.x, (float)window_size.y) * 0.5f;
		mouse_position = mouse_position - window_center;

		float point_length = mouse_position.Length() + 150.0f;
		if (point_length > 350.0f) //cap teleport distance
		{
			point_length = 350.0f;
		}

		//should be done better
		TeleportShift(mouse_position.Normal() * point_length); 		//use direction for teleport
	}

	if (input_detected)
	{
		this->GetPhysicsComponent()->GetRigidBody().SetLinearVelocity(overriding_velocity);
	}
}

void Player::TeleportShift(Vector2D param1)
{
	if (can_telport)
	{
		can_telport = false;

#ifdef _DEBUG
		//std::cout << "Performed teleport" << std::endl;
#endif // _DEBUG
		
		//spawn portal A
		Portal* a_portal = new Portal();
		m_world->AddToSpawnQueue(a_portal);
		a_portal->SetPosition(Vector2D(GetTransformComponent()->GetLocation()));

		//spawn portal b
		Portal* b_portal = new Portal();
		m_world->AddToSpawnQueue(b_portal);
		b_portal->SetPosition(Vector2D(GetTransformComponent()->GetLocation() + param1));

		//link them
		a_portal->Link(b_portal);
		b_portal->Link(a_portal);

		//AsyncTimer(2000, true, &GameEntity::Destroy, a_portal); //Milliseconds, Async, STD::Bind arguments
		//AsyncTimer(2000, true, &GameEntity::Destroy, b_portal); //Milliseconds, Async, STD::Bind arguments

		//set async timer to re-enable can_telport
		AsyncTimer(4500, true, &Player::ResetTeleport, this); //Milliseconds, Async, STD::Bind arguments
	}
}

void Player::ResetTeleport()
{
	can_telport = true;
#ifdef _DEBUG
	//std::cout << "Reset teleport" << std::endl;
#endif // _DEBUG
}
