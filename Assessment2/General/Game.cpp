#include "Game.h"
#include <SFML/Graphics.hpp>

#include "Player.h"
#include <Physics/RigidBody2D.h>

Game::Game(sf::RenderWindow& renderWindow) : world(this), window(renderWindow), cam(window), game_mode(this)
{
	world.Load();
	
#ifdef _DEBUG
	debug_render.SetWindow(renderWindow);
#endif // _DEBUG

}

void Game::Render()
{
	world.DrawWorld(window);

#ifdef _DEBUG
	debug_render.DrawAllPhysics(world.PhysicsWorld().PhysicsEntities());
	debug_render.DrawAIRepresentation(world.ai_representation);
	debug_render.DrawAllAgentPaths(world.AIWorld().GetAgents());
#endif // _DEBUG

	cam.DrawView(window);
}

void Game::BeginPlay()
{
	world.BeginPlay();
	world.AIWorld().Update(0.0f);

	for (GameEntity* entity : world.world_entities)
	{
		if (Player* p = dynamic_cast<Player*>(entity))
		{
			cam.SetTarget(p); //should come up with a more generic way of setting a camera focus
		}
	}
}

void Game::update(float frameTimeDuration)
{
	game_mode.GameModeUpdate(frameTimeDuration);
	if (game_mode.GameOver())
	{
		return;
	}

#ifdef _DEBUG
	debug_render.Refresh();
#endif
	//process spawn & destroy lists
	world.ProcessLifetimeLists();
	//world.ProccessSpawnList();

	//Logic of world game objects
	world.update(frameTimeDuration);

	//update ai decisions based on world changes.
	//Recalculate world representation to provide to the AI world. - preferably on a timer.
	world.UpdateWorldRepresentation();
	world.AIWorld().Update(frameTimeDuration);

	//Physics Update -- collision detection, Collision Resolution
	world.PhysicsWorld().Update(frameTimeDuration);
	world.PostPhysicsUpdate(frameTimeDuration);

	cam.Update(frameTimeDuration);
}

GameMode& Game::GetGameMode()
{
	return game_mode;
}
