#pragma once
#include "GameEntity.h"
#include "Physics/Vector2D.h"
#include "SyncroTimer.h"

class MovingPlatform : public GameEntity
{
	
public:
	MovingPlatform(TransformComponent* transform, RenderComponent* draw, PhysicsComponent* body);
	virtual void BeginPlay() override;
	virtual void Update(float delta_time) override;

	void SetMovement(Vector2D displacement_to_use);
	
	//The amount of time needed to pass before reversing the movement
	void SetMotionTimer(float seconds);

	//void SetMaxDisplacement(float value);
private:
	void MoveBodiesInContact(Vector2D delta_position);
	Vector2D movement_velocity;
	SyncroTimer timer_for_motion;
	
	//float max_displacement;
	//Vector2D restart_position;
};