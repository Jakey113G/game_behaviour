#pragma once
#include "GameEntity.h"
#include <vector>
#include <Physics/Vector2D.h>

#include "SyncroTimer.h"

//Portal class is a type of game entity.
//It has no blocking physics.
//it has an on overlap function that is to be bound to the physics event.

namespace Physics { class RigidBody2D; }
class Portal : public GameEntity
{
public:
	Portal();
	virtual ~Portal();
	void OnOverlapEnter(Physics::RigidBody2D*);

	virtual void BeginPlay() override;


	virtual void Update(float delta_time) override;

	void Link(Portal* b_portal);

	std::vector<GameEntity*>& TeleportedBodies();
	bool EntityTeleported(GameEntity* entity);
	void RemoveFromTeleportList(GameEntity* entity);
	void AddToRecentTeleportList(GameEntity* entity);

private:
	void Teleport_Entity(GameEntity*);

	SyncroTimer deletion_timer;
	Portal* tethered_portal;
	Vector2D teleport_location;
	std::vector<GameEntity*> entities_teleported; //a shared list of teleported entities
	void CleanUpTeleportList();
};