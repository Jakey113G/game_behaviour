#include "PlatformCrawlingEnemy.h"
#include <AI/AIWorld.h>
#include <Physics/Vector2D.h>

#include <AI/AIWorld.h>
#include <AI/WaypointWorld.h>

#include "AssetLibrary.h"
#include "EntityFactory.h"

#include "World.h"
#include "TransformComponent.h"
#include "PhysicsComponent.h"
#include "RenderComponent.h"

#include "GameOperator.h"
#include "Player.h"

PlatformCrawlingEntity::PlatformCrawlingEntity() : BaseAgent(), Enemy()
{
	transform_component = new TransformComponent();
	physics_component = new PhysicsComponent();

	sf::Sprite sprite = Factory::GetInstance()->MakeSprite(AssetLibrary::GetInstance()->GetTexture(TextureKeyEnum::Barnacle), 51, 57);
	sprite.setOrigin(51 * 0.5f, 57 * 0.5f);
	this->render_component = new RenderComponent(sprite);

	Physics::RigidBody2D& body = physics_component->GetRigidBody();
	body.SetShapeCollider(new Physics::AABB(52.0f, 57.0f));
	body.SetCollisionType(Physics::BLOCKING_COLLISION);
	//body.SetRestituition(0.6f);

	ai_decision_update_rate.SetTimer(0.5f); //every third of a second

	//ai_behaviour_check_rate.SetTimer(2.0f); //every 5 seconds evaluate if we are stuck
	
	//need_to_recalculate_path = false;
	speed = 50;
	state = EnumPlatformCrawlingState::ReturnToPatrol;
}

void PlatformCrawlingEntity::UpdateAgent(AI::AIWorld* world, float time_elapse)
{
	ai_decision_update_rate.Update(time_elapse);
	if (!ai_decision_update_rate.HasTimeElapsed())
	{
		return;
	}
	ai_decision_update_rate.Restart();

	//////////////////////////////////////////////////////////////////////////
	///Detect enemy event State
	bool attack_detected = false;
	for (AI::Operator* e : world->GetOperators())
	{
		if (GameOperator* game_operator = dynamic_cast<GameOperator*>(e))
		{
			Vector2D displacement_to_player = game_operator->GetLocation() - this->GetTransformComponent()->GetLocation();
			if (displacement_to_player.Length() < 450.0f)
			{
				AI::Waypoint* way_point_nearest_operator = world->GetWaypointClosestToPoint(game_operator->GetLocation());

				//Prevent detecting enemies behind walls. 
				//This still isn't guaranteed to be 100% accurate but in theory when doing this cast there should only be two collision, that of the GameOperator and the AI
				//conflicted whether this should check from waypoint to enemy or AI to enemy
				if (GetWorld()->PhysicsWorld().RayCastWorld(this->GetTransformComponent()->GetLocation(), game_operator->GetLocation()).size() == 2)
				{
					point_to_attack = way_point_nearest_operator;
					player_direction_from_point = displacement_to_player;
					attack_detected = true;
				}
			}
		}
	}

	//Update state with enemy
	if (attack_detected)
	{
		state_machine.UpdateState(EnumPlatformCrawlingvents::EnemySighted, state);
	}
	else
	{
		state_machine.UpdateState(EnumPlatformCrawlingvents::LostEnemy, state);
	}

	//state check for at guard
	Vector2D displacement = Vector2D(this->GetTransformComponent()->GetLocation() - point_to_protect->GetLocation());
	if (displacement.Length() < 50.0f)
	{
		state_machine.UpdateState(EnumPlatformCrawlingvents::AtGuard, state);
		patrol_direction = displacement;
	}
	//state check for too far from guard
	else if (displacement.Length() > 700.0f)
	{
		state_machine.UpdateState(EnumPlatformCrawlingvents::FarFromGuard, state);
	}

	//////////////////////////////////////////////////////////////////////////
	//Process attack path
	//////////////////////////////////////////////////////////////////////////
	if (state == EnumPlatformCrawlingState::Attack)
	{
		//Calculate path handling for attack
		CalculatePathOfAttack(time_elapse);
		speed = 280;
	}

	//////////////////////////////////////////////////////////////////////////
	//Resume returning to patrol
	//////////////////////////////////////////////////////////////////////////
	if (state == EnumPlatformCrawlingState::ReturnToPatrol)
	{

		//else calculate path to return to patrol point
		CalculatePathOfPatrol(time_elapse);
		speed = 75;
	}

#ifdef _DEBUG
	static EnumPlatformCrawlingState statecache;

	if (statecache != state)
	{

		if(EnumPlatformCrawlingState::Attack == state)
		{
			std::cout << "Attack" << std::endl;
		}
		if (EnumPlatformCrawlingState::Patrol == state)
		{
			std::cout << "Patrol" << std::endl;
		}
		if (EnumPlatformCrawlingState::ReturnToPatrol == state)
		{
			std::cout << "ReturnToPatrol" << std::endl;
		}

	}
	statecache = state;
#endif // _DEBUG
}

void PlatformCrawlingEntity::Update(float delta_time)
{
	for (const Physics::HitDataResult& hit_data : GetWorld()->PhysicsWorld().HitRigidBodies(&physics_component->GetRigidBody()))
	{
		if (Player* p = dynamic_cast<Player*>(GetWorld()->FindGameEntityInWorld(hit_data.body)))
		{
			p->Destroy();
			return;
		}
	}

#ifdef _DEBUG
	//std::cout << "Node: " << path_to_goal.GetCurrentNode()->ID() << std::endl;
#endif // _DEBUG

	if (state == EnumPlatformCrawlingState::Attack)
	{
		if ( previously_visited_waypoint != next_waypoint)
		{
			ProcessMovementAlongPath(delta_time);
			if (next_waypoint->InAcceptableRangeOfWaypoint(Vector2D(this->transform_component->GetLocation()), 5.0f))
			{
				previously_visited_waypoint = current_waypoint;
				current_waypoint = next_waypoint;
				next_waypoint = path_to_goal.Advance();
			}
		}
		else
		{
			//move towards player vector if this route can not be advanced down anymore
		
			Physics::RigidBody2D& body = GetPhysicsComponent()->GetRigidBody();

			Vector2D velocity = body.GetLinearVelocity();
			velocity.x = player_direction_from_point.Normal().x * speed;
			body.SetLinearVelocity(velocity);
		}
	}
	else if (state == EnumPlatformCrawlingState::ReturnToPatrol)
	{

		ProcessMovementAlongPath(delta_time);
		if (next_waypoint->InAcceptableRangeOfWaypoint(Vector2D(this->transform_component->GetLocation()), 5.0f))
		{
			previously_visited_waypoint = current_waypoint;
			current_waypoint = next_waypoint;
			next_waypoint = path_to_goal.Advance();
		}
	}
	else if (state == EnumPlatformCrawlingState::Patrol)
	{
		ProcessMovementPatrolling(delta_time);
	}
}

void PlatformCrawlingEntity::BeginPlay()
{
	SetGuardLocation(this->GetTransformComponent()->GetLocation());

	current_waypoint = path_to_goal.GetCurrentNode();
	next_waypoint = current_waypoint;
}

void PlatformCrawlingEntity::InitComponents()
{
	Enemy::InitComponents();

	//Register this in the ai world as an ai operator.
	m_world->AIWorld().AddAgent(this);
}

void PlatformCrawlingEntity::ProcessMovementPatrolling(float)
{
	//Vector2D difference_in_movement = Vector2D(previously_visited_waypoint->GetLocation() - this->GetTransformComponent()->GetLocation());

	//direction to patrol in.
	//Vector2D direction_to_travel = difference_in_movement.Normal();

	Vector2D direction_to_travel = patrol_direction.Normal();

	Physics::RigidBody2D* body = &this->GetPhysicsComponent()->GetRigidBody();
	Vector2D movement = body->GetLinearVelocity();

	const std::vector<Physics::HitDataResult>& collisions = GetWorld()->PhysicsWorld().HitRigidBodies(body);
	if (!collisions.empty())
	{
		bool obstacle = false;
		bool can_jump = false;
		for (const Physics::HitDataResult& collision : collisions)
		{
			if (collision.contact_normal.y == -1.0f)
			{
				can_jump = true;
			}
			if (collision.contact_normal.x != 0) //some kind of horizontal collision detected.
			{
				obstacle = true;
			}
		}

		//Try jumping over the obstacle
		if (obstacle && can_jump)
		{
			movement.y += -250.0f;
		}

		movement.x = (direction_to_travel * speed).x;
		body->SetLinearVelocity(movement);
	}
}

bool PlatformCrawlingEntity::EvaluateIfPossibleToReachPointFromNode()
{
	return true;
}

bool PlatformCrawlingEntity::PathForAIStillValid(AI::AStarPath& path)
{
	std::vector<AI::Waypoint*>& the_way = path.GetRoute();

	for (unsigned int i = path.IndexOfNode(path.GetCurrentNode()); i < the_way.size() - 1; ++i)
	{
		AI::EdgeRelation* edge = the_way[i]->GetEdgeToWayPoint(*the_way[i + 1]);
		if (edge == nullptr)
		{
			return false;
		}

		//Changed we no longer disable the path we just make it costly to use it.
		//if (edge->GetState() != AI::EnumEdgeRelationState::PASSABLE)
		//{
		//	return false;
		//}
	}
	return true;
}

void PlatformCrawlingEntity::SetGuardLocation(Vector2D location)
{
	if (point_to_protect != nullptr)
	{
		return;
	}

	//This should create the node at the location and have the ai attempt to go there.
	AI::AIWorld& ai_world = GetWorld()->AIWorld();

	//Due to a lack of collision filters I have to disable this body when I do my collision testing node generation
	GetPhysicsComponent()->GetRigidBody().SetCollisionType(Physics::IGNORE_COLLISION);

	point_to_protect = &GetWorld()->AIWorld().GetRepresentation()->CreateWayPoint(location);
	GetWorld()->AIWorld().GenerateRoutesByNodeDistance(*point_to_protect, 400.0f);
	GetPhysicsComponent()->GetRigidBody().SetCollisionType(Physics::BLOCKING_COLLISION);

	path_to_goal = ai_world.GetPathRoute(ai_world.GetWaypointClosestToPoint(this->GetTransformComponent()->GetLocation()), point_to_protect);

#ifdef _DEBUG
	/*
	std::cout << "Path To Protect Node: " << point_to_protect->ID() << std::endl;

	std::cout << "Nodes that reach the target" << std::endl;
	for (AI::EdgeRelation& edge : point_to_protect->GetEdgeRoutes())
	{
		std::cout << edge.GetDestination()->ID() << std::endl;
	}

	std::cout << "Nodes on route: " << std::endl;
	for (AI::Waypoint* way : path_to_goal.GetRoute())
	{
		static int i;
		std::cout << i++ << " : " << way->ID() << std::endl;
	}
	*/
#endif // _DEBUG
}

/*
//Ensure we aren't stuck on the existing point
bool PlatformCrawlingEntity::CheckBehaviour(float delta)
{
	
	ai_behaviour_check_rate.Update(delta);
	if (ai_behaviour_check_rate.HasTimeElapsed())
	{
		if (current_waypoint == previously_visited_waypoint)
		{
			need_to_recalculate_path = true;
		}
	}

	return need_to_recalculate_path;
	
}
*/

void PlatformCrawlingEntity::CalculatePathOfAttack(float time_elapse)
{
	AI::AIWorld& world = GetWorld()->AIWorld();
	AI::Waypoint* closest_point_to_location = GetWorld()->AIWorld().GetWaypointClosestToPoint(this->GetTransformComponent()->GetLocation());
	path_to_goal = world.GetPathRoute(closest_point_to_location, point_to_attack);

	//Move the route forward if point reached
	//Move the route forward if point reached
	if (next_waypoint->InAcceptableRangeOfWaypoint(Vector2D(this->transform_component->GetLocation()), 5.0f))
	{
		previously_visited_waypoint = current_waypoint;
		current_waypoint = closest_point_to_location;
		next_waypoint = path_to_goal.Advance();
	}
}

void PlatformCrawlingEntity::CalculatePathOfPatrol(float time_elapse)
{
	//////////////////////////////////////////////////////////////////////////
	//AI Route planning if Returning to defense point
	//Or attacking player
	//////////////////////////////////////////////////////////////////////////
	//if (point_to_protect->IsInaccesible()) //A* hangs like crazy if it cant find the route
	//{
	//	state = EnumPlatformCrawlingState::Patrol;
	//	return;
	//}


	AI::AIWorld& world = GetWorld()->AIWorld();
	AI::Waypoint* closest_point_to_location = GetWorld()->AIWorld().GetWaypointClosestToPoint(this->GetTransformComponent()->GetLocation());
	path_to_goal = world.GetPathRoute(closest_point_to_location, point_to_protect);
	
	//Move the route forward if point reached
	if (next_waypoint->InAcceptableRangeOfWaypoint(Vector2D(this->transform_component->GetLocation()), 35.0f))
	{
		Physics::RigidBody2D* body = &this->GetPhysicsComponent()->GetRigidBody();
		const std::vector<Physics::HitDataResult>& hits = GetWorld()->PhysicsWorld().HitRigidBodies(body);
		if (hits.size() != 0)
		{
			previously_visited_waypoint = current_waypoint;
			current_waypoint = closest_point_to_location;
			next_waypoint = path_to_goal.Advance();
		}
	}
}

void PlatformCrawlingEntity::ProcessMovementAlongPath(float delta_time)
{
	//follow route
	Vector2D point = next_waypoint->GetLocation();
	Vector2D distance_to_point = point - this->GetTransformComponent()->GetLocation();

	Physics::RigidBody2D* body = &this->GetPhysicsComponent()->GetRigidBody();
	Vector2D movement = body->GetLinearVelocity();

	if (point.x < this->GetTransformComponent()->GetLocation().x)
	{
		movement.x = -speed;
	}
	else
	{
		movement.x = speed;
	}

	//if should jump
	//if can jump
	//else get a different route

	if (std::abs(distance_to_point.y) > 50.0f)
	{
		const std::vector<Physics::HitDataResult>& hits = GetWorld()->PhysicsWorld().HitRigidBodies(body);
		//find any pointing up normals for jump test
		for (const Physics::HitDataResult& hit : hits)
		{
			if (hit.contact_normal.y == -1.0f)
			{
				movement.y = -(std::abs(distance_to_point.y) + 100.0f);
				if (movement.y < -360.0F)
				{
					movement.y = -360.0f;
				}

				//this->GetPhysicsComponent()->GetRigidBody().AddForce(Vector2D(0, -30000.0f));
			}
		}
	}
	this->GetPhysicsComponent()->GetRigidBody().SetLinearVelocity(movement);
}

//////////////////////////////////////////////////////////////////////////
//STATIC FINITE STATE MACHINE
//////////////////////////////////////////////////////////////////////////

PlatformCrawlingStateMachine PlatformCrawlingEntity::state_machine;

void PlatformCrawlingStateMachine::UpdateState(EnumPlatformCrawlingvents event_value, EnumPlatformCrawlingState& existing_state)
{
	if (event_value == EnumPlatformCrawlingvents::EnemySighted)
	{
		existing_state = EnumPlatformCrawlingState::Attack;
		return;
	}

	if (event_value == EnumPlatformCrawlingvents::LostEnemy)
	{
		if (existing_state == EnumPlatformCrawlingState::Attack)
		{
			existing_state = EnumPlatformCrawlingState::ReturnToPatrol;
			return;
		}
	}

	if (event_value == EnumPlatformCrawlingvents::FarFromGuard)
	{
		if (existing_state == EnumPlatformCrawlingState::Patrol)
		{
			existing_state = EnumPlatformCrawlingState::ReturnToPatrol;
			return;
		}
	}

	if (event_value == EnumPlatformCrawlingvents::AtGuard)
	{
		if (existing_state == EnumPlatformCrawlingState::ReturnToPatrol)
		{
			existing_state = EnumPlatformCrawlingState::Patrol;
			return;
		}

	}

}