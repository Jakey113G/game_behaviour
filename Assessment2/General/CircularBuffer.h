#pragma once
#include <vector>

//Template cyclic buffer class.
//The purpose for this is to create a buffer for storing a lot of information continuously

template <typename T> 
class CircularBuffer
{
public:
	CircularBuffer()
	{
		//Set initial size to 1 for usability
		m_circularBuffer.resize(1);
	}

	CircularBuffer(size_t initial_buffer_size) : m_circularBuffer(initial_buffer_size)
	{
	}

	~CircularBuffer()
	{
		m_circularBuffer.clear(); //Probably not needed but oh well.
	}

	inline void Push(T object)
	{
		m_circularBuffer[current_index++] = object;
		if (current_index >= m_circularBuffer.size()) { current_index = 0; }
	}

	inline std::vector<T>& Buffer()
	{
		return m_circularBuffer;
	}
private:
	std::vector<T> m_circularBuffer;
	unsigned int current_index;
};

//Partially specialized template class for handling pointer types T
template <typename T> class CircularBuffer<T*>
{
public:
	CircularBuffer()
	{
		//Set initial size to 1 for usability
		m_circularBuffer.resize(1);
	}

	CircularBuffer(size_t initial_buffer_size) : m_circularBuffer(initial_buffer_size)
	{
	}

	~CircularBuffer()
	{
		for (unsigned int i = 0; i < m_circularBuffer.size(); ++i)
		{
			delete m_circularBuffer[i];
		}
		m_circularBuffer.clear();
	}

	inline void Push(T* object)
	{
		//The object at index we are replacing needs deallocation.
		if (m_circularBuffer[current_index] != nullptr)
			delete m_circularBuffer[current_index];

		m_circularBuffer[current_index++] = object;
		if (current_index >= m_circularBuffer.size()) { current_index = 0; }
	}

	inline std::vector<T*>& Buffer()
	{
		return m_circularBuffer;
	}
private:
	std::vector<T*> m_circularBuffer;
	unsigned int current_index;
};
