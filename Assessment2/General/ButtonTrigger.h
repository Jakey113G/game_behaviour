#pragma once
#include "GameEntity.h"
#include "SyncroTimer.h"
#include <functional>
#include <vector>

//an interactive button that can be used to call functions.
class ButtonTrigger : public GameEntity
{

public:
	ButtonTrigger();


	virtual void BeginPlay() override;


	virtual void InitComponents() override;


	virtual void Update(float delta_time) override;
	
	void AddFunctionToTrigger(std::function<void()>);

	void ToggleButton();

private:
	bool active;
	SyncroTimer reenable_button_timer;
	std::vector<std::function<void()>> button_function_trigger;
};