#pragma once
#include "Particle.h"
#include "RenderComponent.h"

//Sprite effect has no physics 
//todo make work

class PulsingEmotion : public Particle
{
public:
	PulsingEmotion(sf::Sprite& sprite)
	{
		this->transform_component = new TransformComponent();
		this->render_component = new RenderComponent(sprite);
	}

	virtual void BeginPlay() override
	{
	}


	virtual void Update(float delta_time) override
	{

	}

private:
	sf::Sprite sprite;
};