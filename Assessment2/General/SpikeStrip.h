#pragma once
#include "GameEntity.h"


class SpikeStrip : public GameEntity
{
public:
	enum SpikeDirection
	{
		UP,
		DOWN,
		LEFT,
		RIGHT
	};

public:
	SpikeStrip(SpikeDirection sd, float length_x);
	
	virtual void Update(float delta_time) override;

	virtual void PostPhysicsUpdate(float delta_time) override;

};