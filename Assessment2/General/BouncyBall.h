#pragma once
#include <Physics/Circle.h>

class BouncyBall : public GameEntity
{
public:
	BouncyBall()
	{
		transform_component = new TransformComponent();
		render_component = new RenderComponent();

		sf::Sprite sprite = Factory::GetInstance()->MakeSprite(AssetLibrary::GetInstance()->GetTexture(BluePortal), 90, 90);
		sprite.setOrigin(90 * 0.5f, 90 * 0.5f);
		render_component = new RenderComponent(sprite);

		physics_component = new PhysicsComponent();
		Physics::RigidBody2D& body = physics_component->GetRigidBody();
		body.SetRestituition(0.9f);


		body.SetShapeCollider(new Physics::Circle(Vector2D(45,45), 45.0f));
		body.SetCollisionType(Physics::BLOCKING_COLLISION);
		body.SetBodyMotionType(Physics::BodyMotionType::DYNAMIC_MOTION);
	}


	virtual void BeginPlay() override
	{

	}


	virtual void Update(float delta_time) override
	{

	}
};