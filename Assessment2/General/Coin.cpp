#include "Coin.h"
#include "PhysicsComponent.h"
#include "RenderComponent.h"
#include "TransformComponent.h"

#include "World.h"
#include "GameMode.h"
#include "Player.h"

#include "AssetLibrary.h"
#include "EntityFactory.h"

Coin::Coin()
{
	transform_component = new TransformComponent();

	sf::Sprite coin_sprite = Factory::GetInstance()->MakeSprite(AssetLibrary::GetInstance()->GetTexture(TextureKeyEnum::GoldCoin), 70, 70);
	coin_sprite.setOrigin(70 * 0.5f, 70 * 0.5f);
	render_component = new RenderComponent(coin_sprite);

	physics_component = new PhysicsComponent();
	Physics::RigidBody2D& body = physics_component->GetRigidBody();
	body.SetShapeCollider(new Physics::AABB(45, 45));
	body.SetCollisionType(Physics::OVERLAP_COLLISION);
	body.SetBodyMotionType(Physics::STATIC_MOTION);
}

Coin::~Coin()
{
	GetWorld()->GetGameMode().IncrementScore();
}

void Coin::BeginPlay()
{
}

void Coin::InitComponents()
{
	GameEntity::InitComponents();

	GameMode& gamemode = GetWorld()->GetGameMode();
	gamemode.SetWinConditionScore(gamemode.GetWinConditionScore() + 1);
}

void Coin::Update(float delta_time)
{
	const std::vector<Physics::HitDataResult>& collisions = GetWorld()->PhysicsWorld().OverlappingRigidBodies(&physics_component->GetRigidBody());
	if (!collisions.empty())
	{
		for (const Physics::HitDataResult& hit_data : collisions)
		{
			GameEntity* world_entity = GetWorld()->FindGameEntityInWorld(hit_data.body);
			if (world_entity == nullptr)
			{
				continue;
			}

			if (dynamic_cast<Player*>(world_entity) != nullptr)
			{
				this->Destroy();
				return;
			}
		}
	}
}
