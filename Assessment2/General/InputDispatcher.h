#pragma once
#include <stack>

class InputComponent;

//Singleton Class that Input events are passed into.
class InputDispatcher
{
public:
	static InputDispatcher* GetInstance()
	{
		if (s_InputDispatcher == nullptr)
		{
			s_InputDispatcher = new InputDispatcher();
		}
		return s_InputDispatcher;
	}

	void PushInput(InputComponent* input_component);

private:
	InputDispatcher();
	InputComponent* GetCurrentInputReciever();

	static InputDispatcher* s_InputDispatcher;
	std::stack<InputComponent*> input_stack;
};