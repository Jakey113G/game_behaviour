#pragma once
#include "BaseComponent.h"

class InputComponent : public BaseComponent
{
	InputComponent();
	virtual ~InputComponent();
public:
	virtual void WKey() = 0;
	virtual void AKey() = 0;
	virtual void SKey() = 0;
	virtual void DKey() = 0;

	virtual void SpaceBar() = 0;

	//still pure virtual
	//virtual void Begin(float delta) override;
	//virtual void Update(float delta) override;

private:
	void InputDispatchRegister();
};