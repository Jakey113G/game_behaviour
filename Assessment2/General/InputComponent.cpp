#include "InputComponent.h"
#include "InputDispatcher.h"


InputComponent::InputComponent()
{

}

InputComponent::~InputComponent()
{

}

void InputComponent::WKey()
{

}

void InputComponent::AKey()
{

}

void InputComponent::SKey()
{

}

void InputComponent::DKey()
{

}

void InputComponent::SpaceBar()
{

}

void InputComponent::InputDispatchRegister()
{
	InputDispatcher::GetInstance()->PushInput(this);
}

//Virtual pure base component functionality
/*
void InputComponent::Begin(float delta)
{
	InputDispatchRegister();
}

void InputComponent::Update(float delta)
{
}
*/