#include "AssetLibrary.h"

AssetLibrary* AssetLibrary::s_AssetLibrarySingleton;

AssetLibrary::AssetLibrary() : texture_collection(), font_collection()
{

}

AssetLibrary* AssetLibrary::GetInstance()
{
	if (s_AssetLibrarySingleton == nullptr)
	{
		s_AssetLibrarySingleton = new AssetLibrary();
	}
	return s_AssetLibrarySingleton;
}