#pragma once
#include "GameEntity.h"

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <Physics/RigidBody2D.h>
#include <Physics/AABB.h>

class Player;

class Factory
{
public:
	static Factory* GetInstance();

	Factory();

	sf::Sprite MakeSprite(const sf::Texture& texture, int size_x, int size_y);
	Player* MakePlayer(const sf::Texture& texture, float x, float y);

	template <typename Type>
	Type* MakeEntityAtLocation(Vector2D location)
	{
		Type* t = new Type();
		t->SetPosition(location);
		return t;
	}

	template <typename Type>
	Type* MakeGeneric(sf::Texture& tex, float x, float y)
	{
		TransformComponent* trans = new TransformComponent();
		sf::Sprite sprite(tex, sf::IntRect(0, 0, (int)x, (int)y));
		sprite.setOrigin(x * 0.5f, y * 0.5f);
		RenderComponent* render = new RenderComponent(sprite);

		Physics::BodyData bodydata;
		bodydata.position = trans->GetLocation();
		bodydata.mass = 1;
		bodydata.motion_type = Physics::BodyMotionType::STATIC_MOTION;
		bodydata.collision_type = Physics::BodyCollisionType::BLOCKING_COLLISION;
		PhysicsComponent* phys = new PhysicsComponent(new Physics::RigidBody2D(bodydata, new Physics::AABB(x, y)));
		
		Type* entity = new Type(trans, render, phys);
		return entity;
	}
	
private:
	static Factory* s_factory;
};