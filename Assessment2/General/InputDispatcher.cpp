#include "InputDispatcher.h"
#include "InputComponent.h"
#include <assert.h>

InputDispatcher* InputDispatcher::s_InputDispatcher;

void InputDispatcher::PushInput(InputComponent* current_input_receiver)
{
	input_stack.push(current_input_receiver);
}

InputDispatcher::InputDispatcher()
{

}

InputComponent* InputDispatcher::GetCurrentInputReciever()
{
	assert(!input_stack.empty());

	return input_stack.top();
}
