#include "World.h"
#include "Game.h"
#include "AssetLibrary.h"
#include <iostream>
#include "EntityFactory.h"
#include "PhysicsComponent.h"
#include "CameraManager.h"

#include "AsyncTimer.h"
#include <functional>

#include <Physics/RigidBody2D.h>
#include <Physics/AABB.h>
#include <Physics/Circle.h>
#include <AI/WaypointWorld.h>
#include <AI/Waypoint.h>
#include "GameOperator.h"

//todo have the template factory handle these
#include "Player.h"
#include "PulsingEmoticon.h"
#include "Portal.h"
#include "FlyingEnemy.h"
#include "PlatformCrawlingEnemy.h"
#include "MovingPlatform.h"
#include "BouncyBall.h"
#include "SpikeStrip.h"
#include "SpringPad.h"
#include "MovableCrate.h"
#include "Coin.h"
#include "ButtonTrigger.h"
#include "TriggerWall.h"

#ifdef _DEBUG
#include "ErrorLog.h"
#include "Utility.h"
#endif // _DEBUG

World::World(Game* game) : m_game(*game)
{
	world_state = sf::RenderStates::Default;
	ai_representation = new AI::WayPointWorld();
	ai_world.SetRepresentation(ai_representation);
	ai_world.SetPhysicsQuery(&physical_world);

	//This was going to be used to scale my rendering so I can use more stable physics values and relatable units
	//Create a transform that will scale the position by the required units
	//world_state.transform = sf::Transform(PIXEL_UNITS_TO_METRE, 0, 0, 0, PIXEL_UNITS_TO_METRE, 0, 0, 0, 1);
	//world_state.transform = sf::Transform();
	//world_state.transform.translate(PIXEL_UNITS_TO_METRE, PIXEL_UNITS_TO_METRE);		
}

void World::Load()
{
	SpawnStaticEnvironment();

	//Create way point graph from static geometry
	CalculateStaticWorldRepresentation();

	SpawnDynamicEnvironment();

	return;
}

void World::BeginPlay()
{
	for (GameEntity* entity : world_entities)
	{
		entity->BeginPlay();
	}

	UpdateWorldRepresentation();
}

void World::update(float frameTimeDuration)
{
	for (GameEntity* entity : world_entities)
	{
		entity->Update(frameTimeDuration);
	}

#ifdef _DEBUG
	//std::cout << frameTimeDuration << std::endl;
	//ErrorLog::Write(Utility::ConvertFloatToString(frameTimeDuration));
#endif // _DEBUG
}

void World::PostPhysicsUpdate(float frame_delta)
{
	for (GameEntity* enity : world_entities)
	{
		enity->PostPhysicsUpdate(frame_delta);
	}
}

void World::AddToDestroyQueue(GameEntity* entity)
{
	m_destroyedList.push_back(entity);
}

void World::AddToSpawnQueue(GameEntity* entity)
{
	m_spawnList.push_back(entity);
}

void World::ProcessLifetimeLists()
{
	ProcessDestroyList();
	ProccessSpawnList();
}

void World::ProccessSpawnList()
{
	for (auto it = m_spawnList.begin(), end_it = m_spawnList.end(); it != end_it; ++it)
	{
		SpawnGameEntity(*it);
	}
	m_spawnList.clear();
}

void World::ProcessDestroyList()
{
	for (auto it = m_destroyedList.begin(), end_it = m_destroyedList.end(); it != end_it; ++it)
	{
		RemoveGameEntity(*it);
	}
	m_destroyedList.clear();
}

void World::RemoveGameEntity(GameEntity* entity)
{
	if (AI::BaseAgent* ai = dynamic_cast<AI::BaseAgent*>(entity))
	{
		this->ai_world.RemoveAgent(ai);
	}

	//remove the components from lists 
	if (entity->GetPhysicsComponent())
	{
		//Remove the pointer from vector - the physics component manages the lifetime we just remove it from physics world 
		PhysicsWorld().RemoveWorldEntity(&entity->GetPhysicsComponent()->GetRigidBody());
	}
	if (entity->GetRenderComponent())
	{
		//Remove the pointer from vector - the game entity class manages its lifetime we just remove it from a render look up container 
		RemoveFromDraw(entity->GetRenderComponent());
	}

	//Remove the entity from world vector
	auto it = std::find(world_entities.begin(), world_entities.end(), entity);
	if (it != world_entities.end())
		world_entities.erase(it);

	delete entity; //delete the entity for good destroying all components with destructor
}

void World::SpawnGameEntity(GameEntity* entity)
{
	Register(entity);
	entity->BeginPlay();
}

void World::Register(GameEntity* entity)
{
	entity->SetWorldOwnership(this);

	entity->InitComponents();

	world_entities.push_back(entity);

	if (entity->GetRenderComponent())
		AddToDraw(entity->GetRenderComponent());
	if (entity->GetPhysicsComponent())
		physical_world.AddPhysicsEntity(&entity->GetPhysicsComponent()->GetRigidBody());

}

CameraManager* World::GetCamera()
{
	return m_game.GetCameraManager();
}


std::vector<AI::Waypoint> World::CreateSmoothedPoint(Physics::CollisionPair& contact, AI::Waypoint& start_point, AI::Waypoint& end_point)
{
	//extend from collision point in two directions 
	std::vector<AI::Waypoint>smooth_P;

	//x axis

	Vector2D location_point = contact.contact_point + (contact.contact_normal * -50.0f);
	smooth_P.push_back(AI::Waypoint(location_point));

	//y axis

	//test both points are valid

	//return smoothed route
	return smooth_P;
}

std::vector<AI::Waypoint> World::CreateSmoothingOfPoints(Physics::Shape& shape, Physics::CollisionPair& contact, AI::Waypoint& start_point, AI::Waypoint& end_point)
{
	//start of point smoothing
	std::vector<AI::Waypoint> smoothed_points;
	Vector2D shape_extends = shape.GetSize() * 0.5f;

	Vector2D direction_from_block = contact.bodyB->GetPosition() - shape.GetCenter();
	direction_from_block = direction_from_block.Normal();

	Vector2D offset_position = (direction_from_block * shape_extends.Length());
	Vector2D start_smoothed_position = shape.GetCenter() + offset_position; //original position but off set by its extends
	smoothed_points.push_back(start_point);

	shape.SetPosition(start_smoothed_position);

	while (shape.Overlap(*contact.bodyB->GetColliderShape()))
	{
		//iterate the points 
		start_smoothed_position += offset_position;
		shape.UpdatePosition(offset_position);

		smoothed_points.push_back(AI::Waypoint(start_smoothed_position));
	}

	for (unsigned int i = 0; i < smoothed_points.size() - 1; ++i)
	{
		smoothed_points[i].CreateEdgeToWaypoint(smoothed_points[i + 1]);
	}

	return smoothed_points;
}

//return game entity in world from rigid body.
//Brute force iteration through game entities checking physics components for a matching rigid body
//TODO: Consider creating a component list for easy quick look up matching of entities to components.
GameEntity* World::FindGameEntityInWorld(Physics::RigidBody2D* body)
{
	for (GameEntity* entity : world_entities)
	{
		PhysicsComponent* physics_component = entity->GetPhysicsComponent();
		if (physics_component)
		{
			if (body == &physics_component->GetRigidBody())
			{
				return entity;
			}
		}
	}
	return nullptr;
}

const std::vector<GameEntity*>& World::GetGameEntitiesInWorld()
{
	return this->world_entities;
}

//Utility function find location of game entity
Vector2D World::GetWorldLocation(GameEntity* entity)
{
	for (GameEntity* world_entity : world_entities)
	{
		if (world_entity == entity)
		{
			return world_entity->GetTransformComponent()->GetLocation();
		}
	}

	throw new std::exception("Entity does not exist in world to have a Vector2D location");
}

void World::SpawnStaticEnvironment()
{
	Factory factory;

	//Create object instances
	sf::Texture& industrialtexture = AssetLibrary::GetInstance()->GetTexture(TextureKeyEnum::PlatformIndustrial_057);
	sf::Texture& industrial_floor_texture = AssetLibrary::GetInstance()->GetTexture(TextureKeyEnum::PlatformIndustrial_003);

	GameEntity* wallLeft = factory.MakeGeneric<GameEntity>(industrial_floor_texture, 70, 3000);
	GameEntity* wallright = factory.MakeGeneric<GameEntity>(industrial_floor_texture, 70, 3000);
	GameEntity* ceiling = factory.MakeGeneric<GameEntity>(industrial_floor_texture, 3700, 70);
	GameEntity* floor = factory.MakeGeneric<GameEntity>(industrial_floor_texture, 3000, 70);

	GameEntity* SpawnPlatform = factory.MakeGeneric<GameEntity>(industrial_floor_texture, 400, 70);
	GameEntity* platform_LongA = factory.MakeGeneric<GameEntity>(industrial_floor_texture, 1300, 70);
	GameEntity* platform_LongB = factory.MakeGeneric<GameEntity>(industrial_floor_texture, 600, 70);
	GameEntity* platform_LongC = factory.MakeGeneric<GameEntity>(industrial_floor_texture, 2300, 70);
	GameEntity* platform_LongC_spikestrip = new SpikeStrip(SpikeStrip::UP, 2290);

	GameEntity* stationaryPlatform = factory.MakeGeneric<GameEntity>(industrial_floor_texture, 750, 70);

	GameEntity* jump_route_a = factory.MakeGeneric<GameEntity>(industrialtexture, 210, 70);
	GameEntity* jump_route_b = factory.MakeGeneric<GameEntity>(industrialtexture, 210, 70);
	GameEntity* jump_route_c = factory.MakeGeneric<GameEntity>(industrialtexture, 210, 70);
	GameEntity* jump_route_d = factory.MakeGeneric<GameEntity>(industrialtexture, 210, 70);
	GameEntity* jump_route_e = factory.MakeGeneric<GameEntity>(industrialtexture, 210, 70);

	GameEntity* large_block = factory.MakeGeneric<GameEntity>(industrialtexture, 1400, 700);

	GameEntity* jump_route_a_opposing = factory.MakeGeneric<GameEntity>(industrialtexture, 210, 70);
	GameEntity* jump_route_b_opposing = factory.MakeGeneric<GameEntity>(industrialtexture, 210, 70);
	GameEntity* jump_route_c_opposing = factory.MakeGeneric<GameEntity>(industrialtexture, 210, 70);
	GameEntity* jump_route_d_opposing = factory.MakeGeneric<GameEntity>(industrialtexture, 210, 70);
	GameEntity* jump_route_e_opposing = factory.MakeGeneric<GameEntity>(industrialtexture, 210, 70);

	GameEntity* spring_pad_left_block = factory.MakeGeneric<GameEntity>(industrial_floor_texture, 70, 140);
	GameEntity* spring_pad_left_wall = factory.MakeGeneric<GameEntity>(industrial_floor_texture, 70, 1400);
	GameEntity* spring_pad_strong = new SpringPad(0);
	GameEntity* spring_site_1 = new SpringPad(0);
	GameEntity* spring_site_2 = new SpringPad(0);

	TriggerWall* trigger_wall = new TriggerWall(370, 70);
	ButtonTrigger* trigger_button = new ButtonTrigger();
	trigger_button->AddFunctionToTrigger(std::bind(&TriggerWall::ToggleWall, trigger_wall));

	stationaryPlatform->SetPosition(Vector2D(140, 890));
	SpawnPlatform->SetPosition(Vector2D(-1150, 600));
	wallLeft->SetPosition(Vector2D(-1500, 0));
	wallright->SetPosition(Vector2D(2150, 0));
	floor->SetPosition(Vector2D(0, 1535));
	ceiling->SetPosition(Vector2D(300, -1535));
	platform_LongA->SetPosition(Vector2D(-817.5f, 2.5f));
	platform_LongB->SetPosition(Vector2D(750, 2.5f));
	platform_LongC->SetPosition(Vector2D(-100, -202.5f));
	platform_LongC_spikestrip->SetPosition(Vector2D(-100, -150.0f));
	jump_route_a->SetPosition(Vector2D(-1000, -400));
	jump_route_b->SetPosition(Vector2D(-1362.5f, -600));
	jump_route_c->SetPosition(Vector2D(-1000, -800));
	jump_route_d->SetPosition(Vector2D(-1362.5f, -1000));
	jump_route_e->SetPosition(Vector2D(-1000.0f, -1130));

	jump_route_a_opposing->SetPosition(Vector2D(1000, -400));
	jump_route_b_opposing->SetPosition(Vector2D(1362.5f, -600));
	jump_route_c_opposing->SetPosition(Vector2D(1000, -800));
	jump_route_d_opposing->SetPosition(Vector2D(1362.5f, -1000));
	jump_route_e_opposing->SetPosition(Vector2D(1000.0f, -1130));

	large_block->SetPosition(Vector2D(-195, -885.0f));
	spring_site_1->SetPosition(Vector2D(-650, 1300));
	spring_site_2->SetPosition(Vector2D(850, 1300));
	spring_pad_strong->SetPosition(Vector2D(1810, 1400));
	spring_pad_left_block->SetPosition(Vector2D(1465, 1430));
	spring_pad_left_wall->SetPosition(Vector2D(1465, 450));
	trigger_button->SetPosition(Vector2D(1465, -285));
	trigger_wall->SetPosition(Vector2D(1240, -202.5f));

	Register(SpawnPlatform);
	Register(wallLeft);
	Register(wallright);
	Register(ceiling);
	Register(floor);
	Register(platform_LongA);
	Register(platform_LongB);
	Register(platform_LongC);
	Register(platform_LongC_spikestrip);
	Register(jump_route_a);
	Register(jump_route_b);
	Register(jump_route_c);
	Register(jump_route_d);
	Register(jump_route_e);
	Register(jump_route_a_opposing);
	Register(jump_route_b_opposing);
	Register(jump_route_c_opposing);
	Register(jump_route_d_opposing);
	Register(jump_route_e_opposing);
	Register(large_block);
	Register(spring_site_1);
	Register(spring_site_2);
	Register(spring_pad_strong);
	Register(spring_pad_left_block);
	Register(spring_pad_left_wall);
	Register(trigger_button);
	Register(trigger_wall);
	Register(stationaryPlatform);
}

void World::SpawnDynamicEnvironment()
{
	Factory factory;

	//Create object instances
	sf::Texture& industrialtexture = AssetLibrary::GetInstance()->GetTexture(TextureKeyEnum::PlatformIndustrial_057);
	sf::Texture& industrial_floor_texture = AssetLibrary::GetInstance()->GetTexture(TextureKeyEnum::PlatformIndustrial_003);
	sf::Texture& alien_blue_suit = AssetLibrary::GetInstance()->GetTexture(TextureKeyEnum::AlienBlue_Suit);

	//GameEntity* particle = new PulsingEmotion(factory.MakeSprite(industrialtexture, 70, 70));
	GameEntity* crate = new MovableCrate();
	crate->GetPhysicsComponent()->GetRigidBody().SetRestituition(0.2f);
	GameEntity* crate1 = new MovableCrate();
	crate1->GetPhysicsComponent()->GetRigidBody().SetRestituition(0.2f);
	GameEntity* crate2 = new MovableCrate();
	crate2->GetPhysicsComponent()->GetRigidBody().SetRestituition(0.2f);

	GameEntity* coin1 = new Coin();
	GameEntity* coin2 = new Coin();
	GameEntity* coin3 = new Coin();
	GameEntity* coin4 = new Coin();
	GameEntity* coin5 = new Coin();
	GameEntity* coin6 = new Coin();
	GameEntity* coin7 = new Coin();
	GameEntity* coin8 = new Coin();
	GameEntity* coin9 = new Coin();
	GameEntity* coin10 = new Coin();
	GameEntity* coin11 = new Coin();
	GameEntity* coin12 = new Coin();
	GameEntity* coin13 = new Coin();
	GameEntity* coin14 = new Coin();
	GameEntity* coin15 = new Coin();

	Player* player = factory.MakePlayer(alien_blue_suit, 70, 70);

	MovingPlatform* moving_platform = factory.MakeGeneric<MovingPlatform>(industrial_floor_texture, 610, 70);
	moving_platform->SetMovement(Vector2D(0, -70));
	moving_platform->SetMotionTimer(3.0f); //will be in either direction

	coin1->SetPosition(Vector2D(-600, -80));
	coin2->SetPosition(Vector2D(600, -80));

	coin3->SetPosition(Vector2D(-900, -300));
	coin4->SetPosition(Vector2D(-600, -300));
	coin5->SetPosition(Vector2D(-300, -300));
	coin6->SetPosition(Vector2D(0, -300));
	coin7->SetPosition(Vector2D(300, -300));
	coin8->SetPosition(Vector2D(600, -300));

	coin9->SetPosition(Vector2D(-900, -1300));
	coin10->SetPosition(Vector2D(-600, -1300));
	coin11->SetPosition(Vector2D(-300, -1300));
	coin12->SetPosition(Vector2D(0, -1300));

	coin13->SetPosition(Vector2D(-500, 1200));
	coin14->SetPosition(Vector2D(1800, 1200));
	coin15->SetPosition(Vector2D(800, 1200));

	crate->SetPosition(Vector2D(0, -1500));
	crate1->SetPosition(Vector2D(100, -1500));
	crate2->SetPosition(Vector2D(450, -1500));

	player->SetPosition(Vector2D(-1150, 350));
	moving_platform->SetPosition(Vector2D(140, 0));

	Register(crate);
	Register(crate1);
	Register(crate2);
	Register(moving_platform);

	Register(coin1);
	Register(coin2);
	Register(coin3);
	Register(coin4);
	Register(coin5);
	Register(coin6);
	Register(coin7);
	Register(coin8);
	Register(coin9);
	Register(coin10);
	Register(coin11);
	Register(coin12);
	Register(coin13);
	Register(coin14);
	Register(coin15);

	Register(player);

	//GameEntity* bouncy = new BouncyBall();
	//bouncy->SetPosition(Vector2D(800, -500));
	//Register(bouncy);
}

void World::CalculateStaticWorldRepresentation()
{
	//Create all of our nodes and use the ai world and phsyics query to calculate edge links.
	//It is intensive but the logic behind this was that in runtime if a node needs adding it can be placed and
	//automatically connect to nodes within the entry size. Smoothing would be in ai behavior

	//NOTE Vector iteration in debug is ridiculously slow. 
	//consider disabling debug vectors as a compiler argument in the project solution:  -D "_HAS_ITERATOR_DEBUGGING=0" 

	/*
	//spawn row
	ai_representation->CreateWayPoint(Vector2D(250, -300)).SetEntrySize(260.0f);
	ai_representation->CreateWayPoint(Vector2D(500, -300)).SetEntrySize(260.0f);
	ai_representation->CreateWayPoint(Vector2D(750, -300)).SetEntrySize(260.0f);
	ai_representation->CreateWayPoint(Vector2D(1000, -300)).SetEntrySize(260.0f);
	ai_representation->CreateWayPoint(Vector2D(0, -300)).SetEntrySize(260.0f);
	ai_representation->CreateWayPoint(Vector2D(-250, -300)).SetEntrySize(260.0f);
	ai_representation->CreateWayPoint(Vector2D(-500, -300)).SetEntrySize(260.0f);

	//spawn row below
	ai_representation->CreateWayPoint(Vector2D(-1300, -80)).SetEntrySize(601.0f);
	ai_representation->CreateWayPoint(Vector2D(-750, -80)).SetEntrySize(600.0f);
	ai_representation->CreateWayPoint(Vector2D(-250, -80)).SetEntrySize(600.0f);
	ai_representation->CreateWayPoint(Vector2D(150, -80)).SetEntrySize(600.0f);
	ai_representation->CreateWayPoint(Vector2D(500, -80)).SetEntrySize(600.0f);
	ai_representation->CreateWayPoint(Vector2D(1000, -80)).SetEntrySize(600.0f);
	
	//Staggered points left case
	ai_representation->CreateWayPoint(Vector2D(-1250, -300)).SetEntrySize(266.0f);
	ai_representation->CreateWayPoint(Vector2D(-750, -300)).SetEntrySize(266.0f);
	ai_representation->CreateWayPoint(Vector2D(-1000, -300)).SetEntrySize(266.0f);
	ai_representation->CreateWayPoint(Vector2D(-850, -480)).SetEntrySize(300.0f);  //smooth
	ai_representation->CreateWayPoint(Vector2D(-1100, -480)).SetEntrySize(300.0f); //smooth
	ai_representation->CreateWayPoint(Vector2D(-1300, -680)).SetEntrySize(300.0f); //smooth
	ai_representation->CreateWayPoint(Vector2D(-1100, -870)).SetEntrySize(400.0f); //smooth
	ai_representation->CreateWayPoint(Vector2D(-1250, -1070)).SetEntrySize(266.0f); //smooth
	ai_representation->CreateWayPoint(Vector2D(-1075, -1190)).SetEntrySize(266.0f); //smooth

	//staggered points right case
	ai_representation->CreateWayPoint(Vector2D(900, -460)).SetEntrySize(300.0f); //base floor
	ai_representation->CreateWayPoint(Vector2D(1110, -460)).SetEntrySize(300.0f); //base floor
	ai_representation->CreateWayPoint(Vector2D(1250, -650)).SetEntrySize(300.0f); //base floor
	ai_representation->CreateWayPoint(Vector2D(1450, -650)).SetEntrySize(300.0f); //base floor
	ai_representation->CreateWayPoint(Vector2D(1100, -850)).SetEntrySize(500.0f);
	ai_representation->CreateWayPoint(Vector2D(1400, -400)).SetEntrySize(300.0f); 
	ai_representation->CreateWayPoint(Vector2D(1250, -1050)).SetEntrySize(300.0f); 
	ai_representation->CreateWayPoint(Vector2D(900, -1200)).SetEntrySize(300.0f);
	ai_representation->CreateWayPoint(Vector2D(1100, -1200)).SetEntrySize(300.0f);

	ai_representation->CreateWayPoint(Vector2D(750, -1350)).SetEntrySize(500.0f);

	//down below spawn platform
	ai_representation->CreateWayPoint(Vector2D(-650, 700)).SetEntrySize(1300.0f); //spring pad left aerial
	ai_representation->CreateWayPoint(Vector2D(-950, 1250)).SetEntrySize(300.0f); //spring pad left left
	ai_representation->CreateWayPoint(Vector2D(-350, 1250)).SetEntrySize(300.0f); //spring pad left right

	ai_representation->CreateWayPoint(Vector2D(900, 700)).SetEntrySize(1300.0f); //spring pad right aerial
	ai_representation->CreateWayPoint(Vector2D(1150, 1250)).SetEntrySize(300.0f); //spring pad right right
	ai_representation->CreateWayPoint(Vector2D(570, 1250)).SetEntrySize(300.0f); //spring pad right left

	//ceiling row
	ai_representation->CreateWayPoint(Vector2D(250, -1300)).SetEntrySize(300.0f);
	ai_representation->CreateWayPoint(Vector2D(500, -1300)).SetEntrySize(300.0f);
	ai_representation->CreateWayPoint(Vector2D(0, -1300)).SetEntrySize(300.0f);
	ai_representation->CreateWayPoint(Vector2D(-250, -1300)).SetEntrySize(300.0f);
	ai_representation->CreateWayPoint(Vector2D(-500, -1300)).SetEntrySize(300.0f);
	ai_representation->CreateWayPoint(Vector2D(-800, -1300)).SetEntrySize(300.0f);

	//down below springs
	ai_representation->CreateWayPoint(Vector2D(1810, 1350)).SetEntrySize(600.0f); 
	ai_representation->CreateWayPoint(Vector2D(1470, 1300)).SetEntrySize(400.0f); //spring pad tunnel connector

	ai_representation->CreateWayPoint(Vector2D(1810, 800)).SetEntrySize(950.0f); //spring pad right tunnel
	ai_representation->CreateWayPoint(Vector2D(1810, -100)).SetEntrySize(800.0f); //spring pad right tunnel
	ai_representation->CreateWayPoint(Vector2D(1810, -900)).SetEntrySize(600.0f); //spring pad right tunnel

	ai_representation->CreateWayPoint(Vector2D(0, 1400)).SetEntrySize(600.0f); //base floor
	ai_representation->CreateWayPoint(Vector2D(1240, -202.0f)).SetEntrySize(400.0f);

	ai_representation->CreateWayPoint(Vector2D(-1000, 500)).SetEntrySize(250.0f);

	for (AI::Waypoint& waypoint : ai_representation->world_waypoints)
	{
		ai_world.GenerateRoutesByNodeDistance(waypoint, waypoint.GetEntrySize());
	}
	*/

	/*
	//connection up nodes dynamically becomes harder.
	for (unsigned int i = 0; i < ai_representation->world_waypoints.size() - 1; ++i)
	{
		//GenerateRoutesByNodeDistance(ai_representation->world_waypoints[i] , 500);

		if (physical_world.RayCastWorld(
			ai_representation->world_waypoints[i].GetLocation(),
			ai_representation->world_waypoints[i + 1].GetLocation()).empty())
		{
			ai_representation->world_waypoints[i].CreateEdgeToWaypoint(ai_representation->world_waypoints[i + 1]);
		}
		else
		{
			GenerateSmoothing(Hitdata, start , end):
		}
	}
	*/
	return;
}

void World::UpdateWorldRepresentation()
{
	//check AI edges in the physics world
	for (AI::Waypoint& point : ai_representation->world_waypoints)
	{
		for (AI::EdgeRelation& edge : point.GetEdgeRoutes())
		{
			const std::vector<Physics::RigidBody2D*>& obstructions = physical_world.RayCastWorld(edge.GetOrigin()->GetLocation(), edge.GetDestination()->GetLocation());
			if (obstructions.empty())
			{
				edge.SetState(AI::EnumEdgeRelationState::PASSABLE);
			}
			else
			{
				//due to lack of collision filtering I have to cast game objects found by rigid bodies. Optimization would be to have filtering on my collision world query				
				bool valid_block = false;
				for (Physics::RigidBody2D* body : obstructions)
				{
					//object is not a character it is another type of obstruction
					if ((dynamic_cast<Character*>(FindGameEntityInWorld(body)) == nullptr))
					{
						valid_block = true;
						break;
					}
				}

				if (valid_block)
				{
					edge.SetState(AI::EnumEdgeRelationState::BLOCKED);
				}
				else //it is a character in the route
				{
					edge.SetState(AI::EnumEdgeRelationState::PASSABLE);
				}
			}
		}
	}
}

sf::RenderWindow& World::GetWindow()
{
	return m_game.Window();
}

//Query for objects that may hold invalid references (this is why I should use smart pointers or some other smart method to remove invalid pointers)
bool World::IsInWorld(GameEntity* entity)
{
	for (GameEntity* world_objs : world_entities)
	{
		if (entity == world_objs)
			return true;
	}
	return false;
}

void World::RemoveFromDraw(sf::Drawable* render_component)
{
	auto it = std::find(world_drawing.begin(), world_drawing.end(), render_component);
	if (it != world_drawing.end())
	{
		world_drawing.erase(it);
	}
}

void World::AddToDraw(sf::Drawable* render_component)
{
	world_drawing.push_back(render_component);
}

Game& World::GetGame()
{
	return m_game;
}

GameMode& World::GetGameMode()
{
	return m_game.GetGameMode();
}
