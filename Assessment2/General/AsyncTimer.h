#pragma once
#include <functional>
#include <iostream>
#include <chrono>
#include <cstdio>
#include <future>
#include <thread>

//think about this some more
//template <class T>
class AsyncTimer
{
public:
	//I found this online example which is very simple and does what I need it to do. If I had time I would implement my own timer management classes.
	//aka so it can be continuously polling looping - Dangerous to rely on this incase it removes an object while it is being used.
	//Basically this takes a functor and processes it on another thread.
	template <class callable, class... arguments>
	AsyncTimer(int after, bool async, callable&& f, arguments&&... args)
	{
		std::function<typename std::result_of<callable(arguments...)>::type()> task(std::bind(std::forward<callable>(f), std::forward<arguments>(args)...));

		if (async)
		{
			std::thread([after, task]() {
				std::this_thread::sleep_for(std::chrono::milliseconds(after));
				task();
			}).detach();
		}
		else
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(after));
			task();
		}
	}
};