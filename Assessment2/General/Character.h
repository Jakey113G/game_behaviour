#pragma once
#include "GameEntity.h"

class Character : public GameEntity
{
public:
	Character();
	Character(TransformComponent*, RenderComponent*, PhysicsComponent*);
	virtual ~Character();
	virtual void Update(float delta_time);

protected:
	void MovementRight();
};