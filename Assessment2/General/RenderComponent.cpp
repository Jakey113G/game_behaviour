#include "RenderComponent.h"
#include "AssetLibrary.h"
#include <SFML/Graphics/Sprite.hpp>

//RenderComponent::RenderComponent(sf::Transform& transform) : sf::Drawable(), transform_ref(transform)
//{
//	sf::Texture& tex = AssetLibrary::GetInstance()->GetTexture(TextureKeyEnum::PlatformIndustrial_057);
//	sprite = sf::Sprite(tex, sf::IntRect(0, 0, 70, 70));
//}

RenderComponent::~RenderComponent()
{

}

void RenderComponent::Begin(float delta)
{
}

void RenderComponent::Update(float delta)
{

}

void RenderComponent::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	sf::Transform combinedTransform = this->GetTransform() * states.transform;
	
	//Scale the positional coordinate system of our transform + world transform.
	//sf::Transform combinedTransform = ScaleTransformPosition(transform_ref * states.transform, GetOwner()->GetWorld().PhysicalUnits(), GetOwner().GetWorld().PhysicalUnits());

	//apply local offset to rendering
	//combinedTransform *= local_transform.getTransform();

	onDraw(target, combinedTransform);
}

RenderComponent::RenderComponent()
{

}

RenderComponent::RenderComponent(sf::Sprite draw_sprite)
{
	sprite = draw_sprite;
}
