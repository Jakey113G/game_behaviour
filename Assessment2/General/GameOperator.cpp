#include "GameOperator.h"
#include "GameEntity.h"
#include "TransformComponent.h"
#include <AI/Hueristic.h>

GameOperator::GameOperator(GameEntity& obj, int value) : entity(obj)
{
	value_of_this_operator.SetHeristicValue(value);
}

GameOperator::GameOperator(GameEntity& obj) : entity(obj)
{

}

void GameOperator::SetValue(int value)
{
	value_of_this_operator.SetHeristicValue(value);
}

Vector2D GameOperator::GetLocation()
{
	return entity.GetTransformComponent()->GetLocation();
}

GameEntity* GameOperator::WorldEntity()
{
	return &entity;
}
