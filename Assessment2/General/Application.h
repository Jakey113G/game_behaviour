#pragma once
#include "Game.h"
#include <SFML/Graphics.hpp>
#include <chrono>

//Holds the highest most logic for handling the game events.
class Application
{
public:
			Application();

	void	InstantiateOtherSystems();
	void	LoadResource();

	void	StartRunTime();
	int		RunTime();

	float	GetFrameRate();
private:
	void	DefaultFrameRate();

	sf::RenderWindow window;
	Game game;

	std::chrono::duration<float>			m_deltaFrameFime;
};

