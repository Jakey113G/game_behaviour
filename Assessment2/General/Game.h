#pragma once
#include "World.h"
#include "CameraManager.h"
#include "GameMode.h"

#ifdef _DEBUG
#include "DebugRendering.h"
#endif // _DEBUG

//Another level of abstraction - Entry-> Application -> Game -> World 
class Game
{
	friend class Application;
private:
	Game(sf::RenderWindow& renderWindow);

	sf::RenderWindow& window;
	CameraManager cam;

	GameMode game_mode;
	World world;
#ifdef _DEBUG
	DebugRender debug_render;
#endif // _DEBUG

public:
	void Render();
	void BeginPlay();
	void update(float frameTimeDuration);

	CameraManager* const GetCameraManager()
	{
		return &cam;
	}

	sf::RenderWindow& Window()
	{
		return window;
	}

	GameMode& GetGameMode();
};