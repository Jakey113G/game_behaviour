#include "BaseComponent.h"

BaseComponent::~BaseComponent()
{

}

void BaseComponent::SetOwner(GameEntity* entity)
{
	owner = entity;
}

BaseComponent::BaseComponent()
{

}

BaseComponent::BaseComponent(GameEntity* entity) : owner(entity)
{

}
